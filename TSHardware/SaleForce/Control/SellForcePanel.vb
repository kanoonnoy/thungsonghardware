﻿Public Class SellForcePanel

#Region "Variable"
    Private table As TableEntity = New TableEntity()
    Private ReadOnly _TransactionType = "ขายเงินสด"
    Private ReadOnly _PrefixDocument = "TS"
    Private _CurrentTransactionID = -1
#End Region

#Region "Reload and Startup"

    Public Sub Reload()
        Try
            Me.Enabled = False


            Me.TransactionSellTableAdapter.Fill(RealTimeDataSet.TransactionSell)
            Me.TransactionSellItemTableAdapter.Fill(RealTimeDataSet.TransactionSellItem)

            Me.TransactionSellBindingSource.MoveLast()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
        End Try
    End Sub
    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()
    End Sub

#End Region

#Region "UpdateAll"
    Private Sub UpdateAll()
        Try

            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor


            Me.Validate()
            Me.TransactionSellItemBindingSource.EndEdit()
            Me.TransactionSellBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try
    End Sub
    Private Sub TransactionSellItemBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles TransactionSellItemBindingNavigatorSaveItem.Click
        UpdateAll()
    End Sub
#End Region

#Region "Add Product"

    Private Sub AddProduct()
        Try
            Me.btAddProduct.Enabled = False
            Dim productCode = Me.TextBox1.Text
            Dim rowadd As DataRow
            Dim str = String.Format("SELECT * FROM ProductMaster WHERE ProductCode = '{0}'", productCode)
            If (table.SQLQuery(str)) Then
                If (table.Records.Rows.Count > 0) Then
                    rowadd = table.Records.Rows(0)
                Else
                    Return
                End If
            End If

            Dim rowview As DataRowView = Me.TransactionSellItemBindingSource.AddNew()

            rowview("TransactionID") = _CurrentTransactionID
            rowview("ProductCode") = productCode
            rowview("ProductName") = rowadd("ProductName")

            rowview("Qty") = 1
            rowview("Price") = rowadd("SellPrice1")
            rowview("Discount") = 0
            rowview("UpdateUser") = GlobalV.CurrentUsername
            rowview("UpdateWhen") = DateTime.Now

            Me.TransactionSellItemBindingSource.MoveLast()
            Me.TransactionSellItemBindingSource.EndEdit()

            CalculateTotal()

            UpdateAll()


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.btAddProduct.Enabled = True
        End Try
    End Sub

    Private Sub btAddProduct_Click(sender As Object, e As EventArgs) Handles btAddProduct.Click
        AddProduct()
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If (e.KeyData = Keys.Enter) Then
            AddProduct()
        End If
    End Sub

    Private Sub CalculateTotal()
        Try

            Dim Totalsum As Decimal = 0
            Dim TotalPrice As Decimal = 0
            Dim Totaldiscount As Decimal = 0
            Dim rows = Me.RealTimeDataSet.TransactionSellItem.Select(String.Format("TransactionID = {0}", _CurrentTransactionID))
            If (rows.Length > 0) Then
                For Each row As DataRow In rows
                    Dim price As Decimal = 0
                    Dim discount As Decimal = 0

                    If (row("Price") IsNot Nothing) Then price = row("Price")
                    If (row("Discount") IsNot Nothing) Then discount = row("Discount")
                    Dim tempSum = price - discount
                    Totalsum += tempSum
                    Totaldiscount += discount
                    TotalPrice += price

                Next
            End If

            Dim rowCurrent As DataRowView = Me.TransactionSellBindingSource.Current

            rowCurrent("Total") = TotalPrice
            rowCurrent("Discount") = Totaldiscount
            rowCurrent("Amount") = Totalsum

            Me.TransactionSellBindingSource.EndEdit()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally


        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CalculateTotal()
    End Sub
#End Region

#Region "Relink"
    Private Sub Relink()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Dim rowview As DataRowView = TransactionSellBindingSource.Current
            _CurrentTransactionID = rowview("TransactionID")

            Me.TransactionSellItemBindingSource.Filter = String.Format("TransactionID = {0}", _CurrentTransactionID)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub TransactionSellBindingSource_PositionChanged(sender As Object, e As EventArgs) Handles TransactionSellBindingSource.PositionChanged
        Relink()
    End Sub
#End Region

#Region "Add Transaction"
    Private Sub AddTracnsaction()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Dim lastNumber = 0

            Try
                Dim str = String.Format("SELECT DocumentNumber FROM TransactionSell WHERE TransactionType = '{0}' AND LEFT(DocumentNumber,4) = '{1}' ORDER BY DocumentNumber DESC", _TransactionType, _PrefixDocument + DateTime.Now.ToString("yy"))
                If (table.SQLQuery(str)) Then
                    If (table.Records.Rows.Count > 0) Then
                        Dim temp = table.Records.Rows(0)("DocumentNumber").ToString().Replace(_PrefixDocument + DateTime.Now.ToString("yy"), "")
                        lastNumber = Convert.ToInt16(temp)
                    End If
                End If
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try


            lastNumber = lastNumber + 1

            Dim rowview As DataRowView = Me.TransactionSellBindingSource.AddNew()

            rowview("TransactionType") = _TransactionType
            rowview("TrasactionWhen") = DateTime.Now

            rowview("DocumentNumber") = String.Format("{0}{1}{2:0000}", _PrefixDocument, DateTime.Now.ToString("yy"), lastNumber)
            rowview("DocumentDate") = DateTime.Now.Date

            rowview("Status") = "0"
            rowview("CreateUser") = GlobalV.CurrentUsername
            rowview("CreateWhen") = DateTime.Now

            rowview("UpdateUser") = GlobalV.CurrentUsername
            rowview("UpdateWhen") = DateTime.Now

            Me.TransactionSellBindingSource.MoveLast()
            Me.TransactionSellBindingSource.EndEdit()

            UpdateAll()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub
    Private Sub BindingNavigatorAddNewItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorAddNewItem.Click
        AddTracnsaction()
    End Sub
#End Region

#Region "SearchCustomer"
    Private Sub SearchCustomer()
        Try

            Dim customercode = Me.CustomerCodeTextBox.Text
            Dim str = String.Format("SELECT * FROM Customer WHERE CustomerCode = '{0}' ", customercode)
            If (table.SQLQuery(str)) Then
                If (table.Records.Rows.Count > 0) Then

                    Me.CustomerNameTextBox.Text = table.Records.Rows(0)("CustomerName")
                    Me.TelephoneNumberTextBox.Text = table.Records.Rows(0)("TelephoneNumber")
                    Me.AddressNumberTextBox.Text = table.Records.Rows(0)("AddressNumber")
                    Me.AddressRoadTextBox.Text = table.Records.Rows(0)("AddressRoad")
                    Me.AddressTownTextBox.Text = table.Records.Rows(0)("AddressTown")
                    Me.AddressCityTextBox.Text = table.Records.Rows(0)("AddressCity")
                    Me.ProvinceTextBox.Text = table.Records.Rows(0)("Province")
                    Me.PostalCodeTextBox.Text = table.Records.Rows(0)("PostalCode")
                End If
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btSearch_Click(sender As Object, e As EventArgs) Handles btSearch.Click
        SearchCustomer()
    End Sub
    Private Sub CustomerCodeTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles CustomerCodeTextBox.KeyDown
        If (e.KeyData = Keys.Enter) Then
            SearchCustomer()
        End If
    End Sub
#End Region

#Region "Print"
    Private Sub PrintReport()
        Try

            Dim roview As DataRowView = Me.TransactionSellBindingSource.Current
            If (roview Is Nothing) Then Return
            Dim DocumentNumber = roview("DocumentNumber").ToString()

            Dim dlg As DynamicReportForm = New DynamicReportForm()
            dlg.ReportNumber = "RL0002"
            dlg.Parameter = String.Format("DocumentNumber=""{0}""", DocumentNumber)
            dlg.WindowState = FormWindowState.Maximized
            dlg.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub
    Private Sub btPrint_Click(sender As Object, e As EventArgs) Handles btPrint.Click
        PrintReport()
    End Sub
#End Region

End Class
