﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SellForcePanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TransactionTypeLabel As System.Windows.Forms.Label
        Dim DocumentNumberLabel As System.Windows.Forms.Label
        Dim DocumentDateLabel As System.Windows.Forms.Label
        Dim CustomerCodeLabel As System.Windows.Forms.Label
        Dim CustomerNameLabel As System.Windows.Forms.Label
        Dim AddressNumberLabel As System.Windows.Forms.Label
        Dim AddressRoadLabel As System.Windows.Forms.Label
        Dim AddressCityLabel As System.Windows.Forms.Label
        Dim ProvinceLabel As System.Windows.Forms.Label
        Dim PostalCodeLabel As System.Windows.Forms.Label
        Dim TelephoneNumberLabel As System.Windows.Forms.Label
        Dim TotalLabel As System.Windows.Forms.Label
        Dim DiscountLabel As System.Windows.Forms.Label
        Dim AmountLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim CreateUserLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SellForcePanel))
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.TransactionSellItemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TransactionSellBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TransactionSellItemTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.TransactionSellItemTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.TransactionSellTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.TransactionSellTableAdapter()
        Me.TransactionSellItemBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btReload = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.TransactionSellItemBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.TransactionSellItemDataGridView = New System.Windows.Forms.DataGridView()
        Me.ProductCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateUserDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateWhenDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btAddProduct = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btPrint = New System.Windows.Forms.Button()
        Me.TotalTextBox = New System.Windows.Forms.TextBox()
        Me.DiscountTextBox = New System.Windows.Forms.TextBox()
        Me.AmountTextBox = New System.Windows.Forms.TextBox()
        Me.TransactionTypeTextBox = New System.Windows.Forms.TextBox()
        Me.DocumentNumberTextBox = New System.Windows.Forms.TextBox()
        Me.DocumentDateMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.CustomerCodeTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerNameTextBox = New System.Windows.Forms.TextBox()
        Me.AddressNumberTextBox = New System.Windows.Forms.TextBox()
        Me.AddressRoadTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTownTextBox = New System.Windows.Forms.TextBox()
        Me.AddressCityTextBox = New System.Windows.Forms.TextBox()
        Me.ProvinceTextBox = New System.Windows.Forms.TextBox()
        Me.PostalCodeTextBox = New System.Windows.Forms.TextBox()
        Me.TelephoneNumberTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.CreateUserTextBox = New System.Windows.Forms.TextBox()
        Me.CreateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btSearch = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TransactionIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrasactionWhenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocumentNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocumentDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressRoadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressTownDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressCityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProvinceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PostalCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TelephoneNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiscountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CreateUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CreateWhenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateWhenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        TransactionTypeLabel = New System.Windows.Forms.Label()
        DocumentNumberLabel = New System.Windows.Forms.Label()
        DocumentDateLabel = New System.Windows.Forms.Label()
        CustomerCodeLabel = New System.Windows.Forms.Label()
        CustomerNameLabel = New System.Windows.Forms.Label()
        AddressNumberLabel = New System.Windows.Forms.Label()
        AddressRoadLabel = New System.Windows.Forms.Label()
        AddressCityLabel = New System.Windows.Forms.Label()
        ProvinceLabel = New System.Windows.Forms.Label()
        PostalCodeLabel = New System.Windows.Forms.Label()
        TelephoneNumberLabel = New System.Windows.Forms.Label()
        TotalLabel = New System.Windows.Forms.Label()
        DiscountLabel = New System.Windows.Forms.Label()
        AmountLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        CreateUserLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionSellItemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionSellBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionSellItemBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TransactionSellItemBindingNavigator.SuspendLayout()
        CType(Me.TransactionSellItemDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TransactionTypeLabel
        '
        TransactionTypeLabel.AutoSize = True
        TransactionTypeLabel.Location = New System.Drawing.Point(45, 70)
        TransactionTypeLabel.Name = "TransactionTypeLabel"
        TransactionTypeLabel.Size = New System.Drawing.Size(66, 24)
        TransactionTypeLabel.TabIndex = 2
        TransactionTypeLabel.Text = "ประเภท"
        '
        'DocumentNumberLabel
        '
        DocumentNumberLabel.AutoSize = True
        DocumentNumberLabel.Location = New System.Drawing.Point(16, 34)
        DocumentNumberLabel.Name = "DocumentNumberLabel"
        DocumentNumberLabel.Size = New System.Drawing.Size(95, 24)
        DocumentNumberLabel.TabIndex = 6
        DocumentNumberLabel.Text = "เลขที่เอกสาร"
        '
        'DocumentDateLabel
        '
        DocumentDateLabel.AutoSize = True
        DocumentDateLabel.Location = New System.Drawing.Point(223, 37)
        DocumentDateLabel.Name = "DocumentDateLabel"
        DocumentDateLabel.Size = New System.Drawing.Size(92, 24)
        DocumentDateLabel.TabIndex = 8
        DocumentDateLabel.Text = "วันที่เอกสาร"
        '
        'CustomerCodeLabel
        '
        CustomerCodeLabel.AutoSize = True
        CustomerCodeLabel.Location = New System.Drawing.Point(8, 30)
        CustomerCodeLabel.Name = "CustomerCodeLabel"
        CustomerCodeLabel.Size = New System.Drawing.Size(77, 24)
        CustomerCodeLabel.TabIndex = 10
        CustomerCodeLabel.Text = "รหัสลูกค้า"
        '
        'CustomerNameLabel
        '
        CustomerNameLabel.AutoSize = True
        CustomerNameLabel.Location = New System.Drawing.Point(17, 63)
        CustomerNameLabel.Name = "CustomerNameLabel"
        CustomerNameLabel.Size = New System.Drawing.Size(68, 24)
        CustomerNameLabel.TabIndex = 12
        CustomerNameLabel.Text = "ชื่อลูกค้า"
        '
        'AddressNumberLabel
        '
        AddressNumberLabel.AutoSize = True
        AddressNumberLabel.Location = New System.Drawing.Point(44, 97)
        AddressNumberLabel.Name = "AddressNumberLabel"
        AddressNumberLabel.Size = New System.Drawing.Size(41, 24)
        AddressNumberLabel.TabIndex = 14
        AddressNumberLabel.Text = "ที่อยู่"
        '
        'AddressRoadLabel
        '
        AddressRoadLabel.AutoSize = True
        AddressRoadLabel.Location = New System.Drawing.Point(145, 98)
        AddressRoadLabel.Name = "AddressRoadLabel"
        AddressRoadLabel.Size = New System.Drawing.Size(82, 24)
        AddressRoadLabel.TabIndex = 16
        AddressRoadLabel.Text = "ซอย/ถนน"
        '
        'AddressCityLabel
        '
        AddressCityLabel.AutoSize = True
        AddressCityLabel.Location = New System.Drawing.Point(36, 131)
        AddressCityLabel.Name = "AddressCityLabel"
        AddressCityLabel.Size = New System.Drawing.Size(49, 24)
        AddressCityLabel.TabIndex = 20
        AddressCityLabel.Text = "ตำบล"
        '
        'ProvinceLabel
        '
        ProvinceLabel.AutoSize = True
        ProvinceLabel.Location = New System.Drawing.Point(28, 166)
        ProvinceLabel.Name = "ProvinceLabel"
        ProvinceLabel.Size = New System.Drawing.Size(57, 24)
        ProvinceLabel.TabIndex = 22
        ProvinceLabel.Text = "จังหวัด"
        '
        'PostalCodeLabel
        '
        PostalCodeLabel.AutoSize = True
        PostalCodeLabel.Location = New System.Drawing.Point(8, 200)
        PostalCodeLabel.Name = "PostalCodeLabel"
        PostalCodeLabel.Size = New System.Drawing.Size(104, 24)
        PostalCodeLabel.TabIndex = 24
        PostalCodeLabel.Text = "รหัสไปรษณีย์"
        '
        'TelephoneNumberLabel
        '
        TelephoneNumberLabel.AutoSize = True
        TelephoneNumberLabel.Location = New System.Drawing.Point(194, 201)
        TelephoneNumberLabel.Name = "TelephoneNumberLabel"
        TelephoneNumberLabel.Size = New System.Drawing.Size(37, 24)
        TelephoneNumberLabel.TabIndex = 26
        TelephoneNumberLabel.Text = "โทร"
        '
        'TotalLabel
        '
        TotalLabel.AutoSize = True
        TotalLabel.Location = New System.Drawing.Point(563, 30)
        TotalLabel.Name = "TotalLabel"
        TotalLabel.Size = New System.Drawing.Size(113, 24)
        TotalLabel.TabIndex = 28
        TotalLabel.Text = "จำนวนเงินรวม"
        TotalLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'DiscountLabel
        '
        DiscountLabel.AutoSize = True
        DiscountLabel.Location = New System.Drawing.Point(615, 64)
        DiscountLabel.Name = "DiscountLabel"
        DiscountLabel.Size = New System.Drawing.Size(60, 24)
        DiscountLabel.TabIndex = 30
        DiscountLabel.Text = "ส่วนลด"
        DiscountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'AmountLabel
        '
        AmountLabel.AutoSize = True
        AmountLabel.Location = New System.Drawing.Point(563, 98)
        AmountLabel.Name = "AmountLabel"
        AmountLabel.Size = New System.Drawing.Size(117, 24)
        AmountLabel.TabIndex = 32
        AmountLabel.Text = "จำนวนเงินสุทธิ"
        AmountLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(256, 74)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(59, 24)
        StatusLabel.TabIndex = 34
        StatusLabel.Text = "สถานะ"
        '
        'CreateUserLabel
        '
        CreateUserLabel.AutoSize = True
        CreateUserLabel.Location = New System.Drawing.Point(41, 104)
        CreateUserLabel.Name = "CreateUserLabel"
        CreateUserLabel.Size = New System.Drawing.Size(70, 24)
        CreateUserLabel.TabIndex = 36
        CreateUserLabel.Text = "สร้างโดย"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(41, 138)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(72, 24)
        UpdateUserLabel.TabIndex = 40
        UpdateUserLabel.Text = "ล่าสุดเมื่อ"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(217, 132)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(52, 24)
        Label1.TabIndex = 44
        Label1.Text = "อำเภอ"
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TransactionSellItemBindingSource
        '
        Me.TransactionSellItemBindingSource.DataMember = "TransactionSellItem"
        Me.TransactionSellItemBindingSource.DataSource = Me.RealTimeDataSet
        '
        'TransactionSellBindingSource
        '
        Me.TransactionSellBindingSource.DataMember = "TransactionSell"
        Me.TransactionSellBindingSource.DataSource = Me.RealTimeDataSet
        '
        'TransactionSellItemTableAdapter
        '
        Me.TransactionSellItemTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.EmployeeMasterTableAdapter = Nothing
        Me.TableAdapterManager.OrderItemTableAdapter = Nothing
        Me.TableAdapterManager.OrderMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.TransactionSellItemTableAdapter = Me.TransactionSellItemTableAdapter
        Me.TableAdapterManager.TransactionSellTableAdapter = Me.TransactionSellTableAdapter
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TransactionSellTableAdapter
        '
        Me.TransactionSellTableAdapter.ClearBeforeFill = True
        '
        'TransactionSellItemBindingNavigator
        '
        Me.TransactionSellItemBindingNavigator.AddNewItem = Nothing
        Me.TransactionSellItemBindingNavigator.BindingSource = Me.TransactionSellBindingSource
        Me.TransactionSellItemBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.TransactionSellItemBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.TransactionSellItemBindingNavigator.DeleteItem = Nothing
        Me.TransactionSellItemBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.TransactionSellItemBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btReload, Me.BindingNavigatorAddNewItem, Me.TransactionSellItemBindingNavigatorSaveItem})
        Me.TransactionSellItemBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.TransactionSellItemBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.TransactionSellItemBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.TransactionSellItemBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.TransactionSellItemBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.TransactionSellItemBindingNavigator.Name = "TransactionSellItemBindingNavigator"
        Me.TransactionSellItemBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.TransactionSellItemBindingNavigator.Size = New System.Drawing.Size(922, 27)
        Me.TransactionSellItemBindingNavigator.TabIndex = 0
        Me.TransactionSellItemBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(72, 24)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'btReload
        '
        Me.btReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(24, 24)
        Me.btReload.Text = "โหลดข้อมูลใหม่"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'TransactionSellItemBindingNavigatorSaveItem
        '
        Me.TransactionSellItemBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TransactionSellItemBindingNavigatorSaveItem.Image = CType(resources.GetObject("TransactionSellItemBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.TransactionSellItemBindingNavigatorSaveItem.Name = "TransactionSellItemBindingNavigatorSaveItem"
        Me.TransactionSellItemBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.TransactionSellItemBindingNavigatorSaveItem.Text = "Save Data"
        '
        'TransactionSellItemDataGridView
        '
        Me.TransactionSellItemDataGridView.AllowUserToAddRows = False
        Me.TransactionSellItemDataGridView.AutoGenerateColumns = False
        Me.TransactionSellItemDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.TransactionSellItemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TransactionSellItemDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ProductCodeDataGridViewTextBoxColumn, Me.ProductNameDataGridViewTextBoxColumn, Me.QtyDataGridViewTextBoxColumn, Me.PriceDataGridViewTextBoxColumn, Me.DiscountDataGridViewTextBoxColumn1, Me.UpdateUserDataGridViewTextBoxColumn1, Me.UpdateWhenDataGridViewTextBoxColumn1})
        Me.TransactionSellItemDataGridView.DataSource = Me.TransactionSellItemBindingSource
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TransactionSellItemDataGridView.DefaultCellStyle = DataGridViewCellStyle10
        Me.TransactionSellItemDataGridView.Location = New System.Drawing.Point(3, 63)
        Me.TransactionSellItemDataGridView.Name = "TransactionSellItemDataGridView"
        Me.TransactionSellItemDataGridView.ReadOnly = True
        Me.TransactionSellItemDataGridView.RowTemplate.Height = 24
        Me.TransactionSellItemDataGridView.Size = New System.Drawing.Size(916, 226)
        Me.TransactionSellItemDataGridView.TabIndex = 1
        '
        'ProductCodeDataGridViewTextBoxColumn
        '
        Me.ProductCodeDataGridViewTextBoxColumn.DataPropertyName = "ProductCode"
        Me.ProductCodeDataGridViewTextBoxColumn.HeaderText = "รหัสสินค้า"
        Me.ProductCodeDataGridViewTextBoxColumn.Name = "ProductCodeDataGridViewTextBoxColumn"
        Me.ProductCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductCodeDataGridViewTextBoxColumn.Width = 120
        '
        'ProductNameDataGridViewTextBoxColumn
        '
        Me.ProductNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName"
        Me.ProductNameDataGridViewTextBoxColumn.HeaderText = "ชื่อสินค้า"
        Me.ProductNameDataGridViewTextBoxColumn.Name = "ProductNameDataGridViewTextBoxColumn"
        Me.ProductNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductNameDataGridViewTextBoxColumn.Width = 110
        '
        'QtyDataGridViewTextBoxColumn
        '
        Me.QtyDataGridViewTextBoxColumn.DataPropertyName = "Qty"
        DataGridViewCellStyle6.Format = "N0"
        Me.QtyDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.QtyDataGridViewTextBoxColumn.HeaderText = "จำนวน"
        Me.QtyDataGridViewTextBoxColumn.Name = "QtyDataGridViewTextBoxColumn"
        Me.QtyDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PriceDataGridViewTextBoxColumn
        '
        Me.PriceDataGridViewTextBoxColumn.DataPropertyName = "Price"
        DataGridViewCellStyle7.Format = "N0"
        Me.PriceDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.PriceDataGridViewTextBoxColumn.HeaderText = "ราคา"
        Me.PriceDataGridViewTextBoxColumn.Name = "PriceDataGridViewTextBoxColumn"
        Me.PriceDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DiscountDataGridViewTextBoxColumn1
        '
        Me.DiscountDataGridViewTextBoxColumn1.DataPropertyName = "Discount"
        DataGridViewCellStyle8.Format = "N0"
        Me.DiscountDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle8
        Me.DiscountDataGridViewTextBoxColumn1.HeaderText = "ส่วนลด"
        Me.DiscountDataGridViewTextBoxColumn1.Name = "DiscountDataGridViewTextBoxColumn1"
        Me.DiscountDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'UpdateUserDataGridViewTextBoxColumn1
        '
        Me.UpdateUserDataGridViewTextBoxColumn1.DataPropertyName = "UpdateUser"
        Me.UpdateUserDataGridViewTextBoxColumn1.HeaderText = "โดย"
        Me.UpdateUserDataGridViewTextBoxColumn1.Name = "UpdateUserDataGridViewTextBoxColumn1"
        Me.UpdateUserDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'UpdateWhenDataGridViewTextBoxColumn1
        '
        Me.UpdateWhenDataGridViewTextBoxColumn1.DataPropertyName = "UpdateWhen"
        DataGridViewCellStyle9.Format = "dd/MM/yy HH:mm"
        Me.UpdateWhenDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle9
        Me.UpdateWhenDataGridViewTextBoxColumn1.HeaderText = "เมื่อ"
        Me.UpdateWhenDataGridViewTextBoxColumn1.Name = "UpdateWhenDataGridViewTextBoxColumn1"
        Me.UpdateWhenDataGridViewTextBoxColumn1.ReadOnly = True
        Me.UpdateWhenDataGridViewTextBoxColumn1.Width = 130
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btAddProduct)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TransactionSellItemDataGridView)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 262)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(922, 295)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "รายการขาย"
        '
        'btAddProduct
        '
        Me.btAddProduct.Location = New System.Drawing.Point(338, 25)
        Me.btAddProduct.Name = "btAddProduct"
        Me.btAddProduct.Size = New System.Drawing.Size(110, 32)
        Me.btAddProduct.TabIndex = 46
        Me.btAddProduct.Text = "เพิ่มสินค้า"
        Me.btAddProduct.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TextBox1.Location = New System.Drawing.Point(91, 27)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(241, 28)
        Me.TextBox1.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "รหัสสินค้า"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.btPrint)
        Me.GroupBox2.Controls.Add(TotalLabel)
        Me.GroupBox2.Controls.Add(Me.TotalTextBox)
        Me.GroupBox2.Controls.Add(DiscountLabel)
        Me.GroupBox2.Controls.Add(Me.DiscountTextBox)
        Me.GroupBox2.Controls.Add(AmountLabel)
        Me.GroupBox2.Controls.Add(Me.AmountTextBox)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox2.Location = New System.Drawing.Point(0, 557)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(922, 136)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "สรุปผลรวม"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(788, 25)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(110, 32)
        Me.Button1.TabIndex = 48
        Me.Button1.Text = "คำนวณใหม่"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btPrint
        '
        Me.btPrint.Location = New System.Drawing.Point(788, 93)
        Me.btPrint.Name = "btPrint"
        Me.btPrint.Size = New System.Drawing.Size(110, 32)
        Me.btPrint.TabIndex = 47
        Me.btPrint.Text = "พิมพ์ใบเสร็จ"
        Me.btPrint.UseVisualStyleBackColor = True
        '
        'TotalTextBox
        '
        Me.TotalTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "Total", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.TotalTextBox.Location = New System.Drawing.Point(682, 27)
        Me.TotalTextBox.Name = "TotalTextBox"
        Me.TotalTextBox.Size = New System.Drawing.Size(100, 28)
        Me.TotalTextBox.TabIndex = 29
        Me.TotalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DiscountTextBox
        '
        Me.DiscountTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DiscountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "Discount", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.DiscountTextBox.Location = New System.Drawing.Point(682, 61)
        Me.DiscountTextBox.Name = "DiscountTextBox"
        Me.DiscountTextBox.Size = New System.Drawing.Size(100, 28)
        Me.DiscountTextBox.TabIndex = 31
        Me.DiscountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AmountTextBox
        '
        Me.AmountTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AmountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "Amount", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.AmountTextBox.Location = New System.Drawing.Point(682, 95)
        Me.AmountTextBox.Name = "AmountTextBox"
        Me.AmountTextBox.Size = New System.Drawing.Size(100, 28)
        Me.AmountTextBox.TabIndex = 33
        Me.AmountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TransactionTypeTextBox
        '
        Me.TransactionTypeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TransactionTypeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "TransactionType", True))
        Me.TransactionTypeTextBox.Location = New System.Drawing.Point(117, 67)
        Me.TransactionTypeTextBox.Name = "TransactionTypeTextBox"
        Me.TransactionTypeTextBox.Size = New System.Drawing.Size(100, 28)
        Me.TransactionTypeTextBox.TabIndex = 3
        '
        'DocumentNumberTextBox
        '
        Me.DocumentNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DocumentNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "DocumentNumber", True))
        Me.DocumentNumberTextBox.Location = New System.Drawing.Point(117, 33)
        Me.DocumentNumberTextBox.Name = "DocumentNumberTextBox"
        Me.DocumentNumberTextBox.Size = New System.Drawing.Size(100, 28)
        Me.DocumentNumberTextBox.TabIndex = 7
        '
        'DocumentDateMaskedTextBox
        '
        Me.DocumentDateMaskedTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DocumentDateMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "DocumentDate", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy"))
        Me.DocumentDateMaskedTextBox.Location = New System.Drawing.Point(321, 34)
        Me.DocumentDateMaskedTextBox.Mask = "00/00/0000"
        Me.DocumentDateMaskedTextBox.Name = "DocumentDateMaskedTextBox"
        Me.DocumentDateMaskedTextBox.Size = New System.Drawing.Size(100, 28)
        Me.DocumentDateMaskedTextBox.TabIndex = 9
        Me.DocumentDateMaskedTextBox.ValidatingType = GetType(Date)
        '
        'CustomerCodeTextBox
        '
        Me.CustomerCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "CustomerCode", True))
        Me.CustomerCodeTextBox.Location = New System.Drawing.Point(91, 27)
        Me.CustomerCodeTextBox.Name = "CustomerCodeTextBox"
        Me.CustomerCodeTextBox.Size = New System.Drawing.Size(298, 28)
        Me.CustomerCodeTextBox.TabIndex = 11
        '
        'CustomerNameTextBox
        '
        Me.CustomerNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "CustomerName", True))
        Me.CustomerNameTextBox.Location = New System.Drawing.Point(91, 61)
        Me.CustomerNameTextBox.Name = "CustomerNameTextBox"
        Me.CustomerNameTextBox.Size = New System.Drawing.Size(298, 28)
        Me.CustomerNameTextBox.TabIndex = 13
        '
        'AddressNumberTextBox
        '
        Me.AddressNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "AddressNumber", True))
        Me.AddressNumberTextBox.Location = New System.Drawing.Point(91, 95)
        Me.AddressNumberTextBox.Name = "AddressNumberTextBox"
        Me.AddressNumberTextBox.Size = New System.Drawing.Size(48, 28)
        Me.AddressNumberTextBox.TabIndex = 15
        '
        'AddressRoadTextBox
        '
        Me.AddressRoadTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressRoadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "AddressRoad", True))
        Me.AddressRoadTextBox.Location = New System.Drawing.Point(232, 95)
        Me.AddressRoadTextBox.Name = "AddressRoadTextBox"
        Me.AddressRoadTextBox.Size = New System.Drawing.Size(157, 28)
        Me.AddressRoadTextBox.TabIndex = 17
        '
        'AddressTownTextBox
        '
        Me.AddressTownTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressTownTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "AddressTown", True))
        Me.AddressTownTextBox.Location = New System.Drawing.Point(91, 129)
        Me.AddressTownTextBox.Name = "AddressTownTextBox"
        Me.AddressTownTextBox.Size = New System.Drawing.Size(120, 28)
        Me.AddressTownTextBox.TabIndex = 19
        '
        'AddressCityTextBox
        '
        Me.AddressCityTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "AddressCity", True))
        Me.AddressCityTextBox.Location = New System.Drawing.Point(272, 128)
        Me.AddressCityTextBox.Name = "AddressCityTextBox"
        Me.AddressCityTextBox.Size = New System.Drawing.Size(117, 28)
        Me.AddressCityTextBox.TabIndex = 21
        '
        'ProvinceTextBox
        '
        Me.ProvinceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProvinceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "Province", True))
        Me.ProvinceTextBox.Location = New System.Drawing.Point(91, 163)
        Me.ProvinceTextBox.Name = "ProvinceTextBox"
        Me.ProvinceTextBox.Size = New System.Drawing.Size(298, 28)
        Me.ProvinceTextBox.TabIndex = 23
        '
        'PostalCodeTextBox
        '
        Me.PostalCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PostalCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "PostalCode", True))
        Me.PostalCodeTextBox.Location = New System.Drawing.Point(115, 197)
        Me.PostalCodeTextBox.Name = "PostalCodeTextBox"
        Me.PostalCodeTextBox.Size = New System.Drawing.Size(73, 28)
        Me.PostalCodeTextBox.TabIndex = 25
        '
        'TelephoneNumberTextBox
        '
        Me.TelephoneNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TelephoneNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "TelephoneNumber", True))
        Me.TelephoneNumberTextBox.Location = New System.Drawing.Point(237, 198)
        Me.TelephoneNumberTextBox.Name = "TelephoneNumberTextBox"
        Me.TelephoneNumberTextBox.Size = New System.Drawing.Size(152, 28)
        Me.TelephoneNumberTextBox.TabIndex = 27
        '
        'StatusTextBox
        '
        Me.StatusTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(321, 71)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(100, 28)
        Me.StatusTextBox.TabIndex = 35
        '
        'CreateUserTextBox
        '
        Me.CreateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "CreateUser", True))
        Me.CreateUserTextBox.Location = New System.Drawing.Point(117, 101)
        Me.CreateUserTextBox.Name = "CreateUserTextBox"
        Me.CreateUserTextBox.ReadOnly = True
        Me.CreateUserTextBox.Size = New System.Drawing.Size(55, 28)
        Me.CreateUserTextBox.TabIndex = 37
        '
        'CreateWhenTextBox
        '
        Me.CreateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "CreateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm"))
        Me.CreateWhenTextBox.Location = New System.Drawing.Point(178, 101)
        Me.CreateWhenTextBox.Name = "CreateWhenTextBox"
        Me.CreateWhenTextBox.ReadOnly = True
        Me.CreateWhenTextBox.Size = New System.Drawing.Size(121, 28)
        Me.CreateWhenTextBox.TabIndex = 39
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(117, 135)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(55, 28)
        Me.UpdateUserTextBox.TabIndex = 41
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TransactionSellBindingSource, "UpdateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm"))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(178, 135)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(121, 28)
        Me.UpdateWhenTextBox.TabIndex = 43
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btSearch)
        Me.GroupBox3.Controls.Add(TelephoneNumberLabel)
        Me.GroupBox3.Controls.Add(Label1)
        Me.GroupBox3.Controls.Add(Me.TelephoneNumberTextBox)
        Me.GroupBox3.Controls.Add(Me.CustomerCodeTextBox)
        Me.GroupBox3.Controls.Add(Me.PostalCodeTextBox)
        Me.GroupBox3.Controls.Add(PostalCodeLabel)
        Me.GroupBox3.Controls.Add(Me.ProvinceTextBox)
        Me.GroupBox3.Controls.Add(ProvinceLabel)
        Me.GroupBox3.Controls.Add(Me.AddressCityTextBox)
        Me.GroupBox3.Controls.Add(AddressCityLabel)
        Me.GroupBox3.Controls.Add(Me.AddressTownTextBox)
        Me.GroupBox3.Controls.Add(Me.AddressRoadTextBox)
        Me.GroupBox3.Controls.Add(CustomerCodeLabel)
        Me.GroupBox3.Controls.Add(AddressRoadLabel)
        Me.GroupBox3.Controls.Add(Me.AddressNumberTextBox)
        Me.GroupBox3.Controls.Add(CustomerNameLabel)
        Me.GroupBox3.Controls.Add(AddressNumberLabel)
        Me.GroupBox3.Controls.Add(Me.CustomerNameTextBox)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(448, 235)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ข้อมูลลูกค้า"
        '
        'btSearch
        '
        Me.btSearch.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0007
        Me.btSearch.Location = New System.Drawing.Point(398, 25)
        Me.btSearch.Name = "btSearch"
        Me.btSearch.Size = New System.Drawing.Size(32, 32)
        Me.btSearch.TabIndex = 45
        Me.btSearch.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(TransactionTypeLabel)
        Me.GroupBox4.Controls.Add(Me.DocumentDateMaskedTextBox)
        Me.GroupBox4.Controls.Add(DocumentDateLabel)
        Me.GroupBox4.Controls.Add(Me.TransactionTypeTextBox)
        Me.GroupBox4.Controls.Add(StatusLabel)
        Me.GroupBox4.Controls.Add(Me.DocumentNumberTextBox)
        Me.GroupBox4.Controls.Add(Me.StatusTextBox)
        Me.GroupBox4.Controls.Add(CreateUserLabel)
        Me.GroupBox4.Controls.Add(DocumentNumberLabel)
        Me.GroupBox4.Controls.Add(Me.CreateUserTextBox)
        Me.GroupBox4.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox4.Controls.Add(Me.CreateWhenTextBox)
        Me.GroupBox4.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox4.Controls.Add(UpdateUserLabel)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox4.Location = New System.Drawing.Point(448, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(468, 235)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "ข้อมูลบิล"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(922, 235)
        Me.Panel1.TabIndex = 6
        '
        'TransactionIDDataGridViewTextBoxColumn
        '
        Me.TransactionIDDataGridViewTextBoxColumn.DataPropertyName = "TransactionID"
        Me.TransactionIDDataGridViewTextBoxColumn.HeaderText = "TransactionID"
        Me.TransactionIDDataGridViewTextBoxColumn.Name = "TransactionIDDataGridViewTextBoxColumn"
        '
        'TransactionTypeDataGridViewTextBoxColumn
        '
        Me.TransactionTypeDataGridViewTextBoxColumn.DataPropertyName = "TransactionType"
        Me.TransactionTypeDataGridViewTextBoxColumn.HeaderText = "TransactionType"
        Me.TransactionTypeDataGridViewTextBoxColumn.Name = "TransactionTypeDataGridViewTextBoxColumn"
        '
        'TrasactionWhenDataGridViewTextBoxColumn
        '
        Me.TrasactionWhenDataGridViewTextBoxColumn.DataPropertyName = "TrasactionWhen"
        Me.TrasactionWhenDataGridViewTextBoxColumn.HeaderText = "TrasactionWhen"
        Me.TrasactionWhenDataGridViewTextBoxColumn.Name = "TrasactionWhenDataGridViewTextBoxColumn"
        '
        'DocumentNumberDataGridViewTextBoxColumn
        '
        Me.DocumentNumberDataGridViewTextBoxColumn.DataPropertyName = "DocumentNumber"
        Me.DocumentNumberDataGridViewTextBoxColumn.HeaderText = "DocumentNumber"
        Me.DocumentNumberDataGridViewTextBoxColumn.Name = "DocumentNumberDataGridViewTextBoxColumn"
        '
        'DocumentDateDataGridViewTextBoxColumn
        '
        Me.DocumentDateDataGridViewTextBoxColumn.DataPropertyName = "DocumentDate"
        Me.DocumentDateDataGridViewTextBoxColumn.HeaderText = "DocumentDate"
        Me.DocumentDateDataGridViewTextBoxColumn.Name = "DocumentDateDataGridViewTextBoxColumn"
        '
        'CustomerCodeDataGridViewTextBoxColumn
        '
        Me.CustomerCodeDataGridViewTextBoxColumn.DataPropertyName = "CustomerCode"
        Me.CustomerCodeDataGridViewTextBoxColumn.HeaderText = "CustomerCode"
        Me.CustomerCodeDataGridViewTextBoxColumn.Name = "CustomerCodeDataGridViewTextBoxColumn"
        '
        'CustomerNameDataGridViewTextBoxColumn
        '
        Me.CustomerNameDataGridViewTextBoxColumn.DataPropertyName = "CustomerName"
        Me.CustomerNameDataGridViewTextBoxColumn.HeaderText = "CustomerName"
        Me.CustomerNameDataGridViewTextBoxColumn.Name = "CustomerNameDataGridViewTextBoxColumn"
        '
        'AddressNumberDataGridViewTextBoxColumn
        '
        Me.AddressNumberDataGridViewTextBoxColumn.DataPropertyName = "AddressNumber"
        Me.AddressNumberDataGridViewTextBoxColumn.HeaderText = "AddressNumber"
        Me.AddressNumberDataGridViewTextBoxColumn.Name = "AddressNumberDataGridViewTextBoxColumn"
        '
        'AddressRoadDataGridViewTextBoxColumn
        '
        Me.AddressRoadDataGridViewTextBoxColumn.DataPropertyName = "AddressRoad"
        Me.AddressRoadDataGridViewTextBoxColumn.HeaderText = "AddressRoad"
        Me.AddressRoadDataGridViewTextBoxColumn.Name = "AddressRoadDataGridViewTextBoxColumn"
        '
        'AddressTownDataGridViewTextBoxColumn
        '
        Me.AddressTownDataGridViewTextBoxColumn.DataPropertyName = "AddressTown"
        Me.AddressTownDataGridViewTextBoxColumn.HeaderText = "AddressTown"
        Me.AddressTownDataGridViewTextBoxColumn.Name = "AddressTownDataGridViewTextBoxColumn"
        '
        'AddressCityDataGridViewTextBoxColumn
        '
        Me.AddressCityDataGridViewTextBoxColumn.DataPropertyName = "AddressCity"
        Me.AddressCityDataGridViewTextBoxColumn.HeaderText = "AddressCity"
        Me.AddressCityDataGridViewTextBoxColumn.Name = "AddressCityDataGridViewTextBoxColumn"
        '
        'ProvinceDataGridViewTextBoxColumn
        '
        Me.ProvinceDataGridViewTextBoxColumn.DataPropertyName = "Province"
        Me.ProvinceDataGridViewTextBoxColumn.HeaderText = "Province"
        Me.ProvinceDataGridViewTextBoxColumn.Name = "ProvinceDataGridViewTextBoxColumn"
        '
        'PostalCodeDataGridViewTextBoxColumn
        '
        Me.PostalCodeDataGridViewTextBoxColumn.DataPropertyName = "PostalCode"
        Me.PostalCodeDataGridViewTextBoxColumn.HeaderText = "PostalCode"
        Me.PostalCodeDataGridViewTextBoxColumn.Name = "PostalCodeDataGridViewTextBoxColumn"
        '
        'TelephoneNumberDataGridViewTextBoxColumn
        '
        Me.TelephoneNumberDataGridViewTextBoxColumn.DataPropertyName = "TelephoneNumber"
        Me.TelephoneNumberDataGridViewTextBoxColumn.HeaderText = "TelephoneNumber"
        Me.TelephoneNumberDataGridViewTextBoxColumn.Name = "TelephoneNumberDataGridViewTextBoxColumn"
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        '
        'DiscountDataGridViewTextBoxColumn
        '
        Me.DiscountDataGridViewTextBoxColumn.DataPropertyName = "Discount"
        Me.DiscountDataGridViewTextBoxColumn.HeaderText = "Discount"
        Me.DiscountDataGridViewTextBoxColumn.Name = "DiscountDataGridViewTextBoxColumn"
        '
        'AmountDataGridViewTextBoxColumn
        '
        Me.AmountDataGridViewTextBoxColumn.DataPropertyName = "Amount"
        Me.AmountDataGridViewTextBoxColumn.HeaderText = "Amount"
        Me.AmountDataGridViewTextBoxColumn.Name = "AmountDataGridViewTextBoxColumn"
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        '
        'CreateUserDataGridViewTextBoxColumn
        '
        Me.CreateUserDataGridViewTextBoxColumn.DataPropertyName = "CreateUser"
        Me.CreateUserDataGridViewTextBoxColumn.HeaderText = "CreateUser"
        Me.CreateUserDataGridViewTextBoxColumn.Name = "CreateUserDataGridViewTextBoxColumn"
        '
        'CreateWhenDataGridViewTextBoxColumn
        '
        Me.CreateWhenDataGridViewTextBoxColumn.DataPropertyName = "CreateWhen"
        Me.CreateWhenDataGridViewTextBoxColumn.HeaderText = "CreateWhen"
        Me.CreateWhenDataGridViewTextBoxColumn.Name = "CreateWhenDataGridViewTextBoxColumn"
        '
        'UpdateUserDataGridViewTextBoxColumn
        '
        Me.UpdateUserDataGridViewTextBoxColumn.DataPropertyName = "UpdateUser"
        Me.UpdateUserDataGridViewTextBoxColumn.HeaderText = "UpdateUser"
        Me.UpdateUserDataGridViewTextBoxColumn.Name = "UpdateUserDataGridViewTextBoxColumn"
        '
        'UpdateWhenDataGridViewTextBoxColumn
        '
        Me.UpdateWhenDataGridViewTextBoxColumn.DataPropertyName = "UpdateWhen"
        Me.UpdateWhenDataGridViewTextBoxColumn.HeaderText = "UpdateWhen"
        Me.UpdateWhenDataGridViewTextBoxColumn.Name = "UpdateWhenDataGridViewTextBoxColumn"
        '
        'SellForcePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TransactionSellItemBindingNavigator)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "SellForcePanel"
        Me.Size = New System.Drawing.Size(922, 693)
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionSellItemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionSellBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionSellItemBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TransactionSellItemBindingNavigator.ResumeLayout(False)
        Me.TransactionSellItemBindingNavigator.PerformLayout()
        CType(Me.TransactionSellItemDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents TransactionSellItemBindingSource As BindingSource
    Friend WithEvents TransactionSellItemTableAdapter As RealTimeDataSetTableAdapters.TransactionSellItemTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TransactionSellItemBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents TransactionSellItemBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents TransactionSellTableAdapter As RealTimeDataSetTableAdapters.TransactionSellTableAdapter
    Friend WithEvents TransactionSellBindingSource As BindingSource
    Friend WithEvents TransactionSellItemDataGridView As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TotalTextBox As TextBox
    Friend WithEvents DiscountTextBox As TextBox
    Friend WithEvents AmountTextBox As TextBox
    Friend WithEvents TransactionTypeTextBox As TextBox
    Friend WithEvents DocumentNumberTextBox As TextBox
    Friend WithEvents DocumentDateMaskedTextBox As MaskedTextBox
    Friend WithEvents CustomerCodeTextBox As TextBox
    Friend WithEvents CustomerNameTextBox As TextBox
    Friend WithEvents AddressNumberTextBox As TextBox
    Friend WithEvents AddressRoadTextBox As TextBox
    Friend WithEvents AddressTownTextBox As TextBox
    Friend WithEvents AddressCityTextBox As TextBox
    Friend WithEvents ProvinceTextBox As TextBox
    Friend WithEvents PostalCodeTextBox As TextBox
    Friend WithEvents TelephoneNumberTextBox As TextBox
    Friend WithEvents StatusTextBox As TextBox
    Friend WithEvents CreateUserTextBox As TextBox
    Friend WithEvents CreateWhenTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btSearch As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btAddProduct As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TransactionIDDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransactionTypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TrasactionWhenDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DocumentNumberDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DocumentDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AddressNumberDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AddressRoadDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AddressTownDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AddressCityDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProvinceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PostalCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TelephoneNumberDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AmountDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CreateUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CreateWhenDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UpdateUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UpdateWhenDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents btReload As ToolStripButton
    Friend WithEvents btPrint As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents ProductCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents QtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DiscountDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents UpdateUserDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents UpdateWhenDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
End Class
