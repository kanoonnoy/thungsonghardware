﻿Public Class CustomerPanel

#Region "Variable"
    Private _IsStartup = True
    Private _IsLoadding = False
    Private _CurrentCustomerCode As String = String.Empty

#End Region

#Region "Reload and Startup"
    Public Sub Reload()

        Try
            _IsLoadding = True
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            If _IsStartup Then

                _IsStartup = False
                Dim dtSource = New DataTable()
                dtSource.Columns.Add("Value")
                dtSource.Rows.Add("")
                dtSource.Rows.Add("ลูกค้าทั่วไป")
                dtSource.Rows.Add("ลูกค้าโครงการ")
                dtSource.Rows.Add("หน่วยงานราชการ")
                dtSource.Rows.Add("หน่วยงานเอกชน/องค์กร")

                Me.CutomerTypeComboBox.ValueMember = "Value"
                Me.CutomerTypeComboBox.DisplayMember = "Value"
                Me.CutomerTypeComboBox.DataSource = dtSource

            End If
            Me.CustomerTableAdapter.Fill(Me.RealTimeDataSet.Customer)
            Me.CustomerBindingSource.MoveLast()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
            _IsLoadding = False
        End Try

    End Sub
    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()
    End Sub
#End Region

#Region "Relink"
    Private Sub Relink()
        _CurrentCustomerCode = String.Empty
        If _IsLoadding Then
            Return
        End If
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Dim rowview As DataRowView = Me.CustomerBindingSource.Current
            _CurrentCustomerCode = rowview("CustomerCode").ToString()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub CustomerBindingSource_PositionChanged(sender As Object, e As EventArgs) Handles CustomerBindingSource.PositionChanged
        Relink()

    End Sub
#End Region

#Region "Update All"
    Private Sub UpdateAll()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            Me.Validate()

            Me.CustomerBindingSource.EndEdit()
            Dim dtUpdate = Me.RealTimeDataSet.Customer.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductGroupID = row("CustomerID").ToString()
                    Dim position = Me.CustomerBindingSource.Find("CustomerID", ProductGroupID)
                    If position >= 0 Then
                        Dim rowView = Me.CustomerBindingSource(position)
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If
                    End If
                Next
                Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub CustomerBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CustomerBindingNavigatorSaveItem.Click
        UpdateAll()
    End Sub

    Private Sub btSearch_Click(sender As Object, e As EventArgs) Handles btSearch.Click
        Try
            Dim SearchText = Me.ToolStripTextBox1.Text
            If SearchText.Length = 0 Then
                Return
            End If

            Dim position As Integer = Me.CustomerBindingSource.Find("CustomerCode", SearchText)
            If Not position - 1 Then
                Me.CustomerBindingSource.Position = position
            Else
                MessageBox.Show(String.Format("ขออภัยไม่พบรหัสลูกค้า '{0}' นี้ค่ะ", SearchText), "ค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.ToolStripTextBox1.SelectAll()
                Me.ToolStripTextBox1.Focus()

            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
#End Region

End Class
