﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CustomerPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CustomerCodeLabel As System.Windows.Forms.Label
        Dim CustomerNameLabel As System.Windows.Forms.Label
        Dim TelephoneNumberLabel As System.Windows.Forms.Label
        Dim SMSNumberLabel As System.Windows.Forms.Label
        Dim FaxNumberLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim Email2Label As System.Windows.Forms.Label
        Dim IDCardLabel As System.Windows.Forms.Label
        Dim AddressNumberLabel As System.Windows.Forms.Label
        Dim AddressRoadLabel As System.Windows.Forms.Label
        Dim AddressTownLabel As System.Windows.Forms.Label
        Dim AddressCityLabel As System.Windows.Forms.Label
        Dim ProvinceLabel As System.Windows.Forms.Label
        Dim PostalCodeLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim CutomerTypeLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CustomerPanel))
        Me.CustomerBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.CustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btReload = New System.Windows.Forms.ToolStripButton()
        Me.CustomerBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.btSearch = New System.Windows.Forms.ToolStripButton()
        Me.CustomerCodeTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerNameTextBox = New System.Windows.Forms.TextBox()
        Me.TelephoneNumberTextBox = New System.Windows.Forms.TextBox()
        Me.SMSNumberTextBox = New System.Windows.Forms.TextBox()
        Me.FaxNumberTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.Email2TextBox = New System.Windows.Forms.TextBox()
        Me.IDCardTextBox = New System.Windows.Forms.TextBox()
        Me.AddressNumberTextBox = New System.Windows.Forms.TextBox()
        Me.AddressRoadTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTownTextBox = New System.Windows.Forms.TextBox()
        Me.AddressCityTextBox = New System.Windows.Forms.TextBox()
        Me.ProvinceTextBox = New System.Windows.Forms.TextBox()
        Me.PostalCodeTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.CustomerTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.CutomerTypeComboBox = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CustomerCodeLabel = New System.Windows.Forms.Label()
        CustomerNameLabel = New System.Windows.Forms.Label()
        TelephoneNumberLabel = New System.Windows.Forms.Label()
        SMSNumberLabel = New System.Windows.Forms.Label()
        FaxNumberLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        Email2Label = New System.Windows.Forms.Label()
        IDCardLabel = New System.Windows.Forms.Label()
        AddressNumberLabel = New System.Windows.Forms.Label()
        AddressRoadLabel = New System.Windows.Forms.Label()
        AddressTownLabel = New System.Windows.Forms.Label()
        AddressCityLabel = New System.Windows.Forms.Label()
        ProvinceLabel = New System.Windows.Forms.Label()
        PostalCodeLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        CutomerTypeLabel = New System.Windows.Forms.Label()
        CType(Me.CustomerBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CustomerBindingNavigator.SuspendLayout()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CustomerCodeLabel
        '
        CustomerCodeLabel.AutoSize = True
        CustomerCodeLabel.Location = New System.Drawing.Point(299, 38)
        CustomerCodeLabel.Name = "CustomerCodeLabel"
        CustomerCodeLabel.Size = New System.Drawing.Size(77, 24)
        CustomerCodeLabel.TabIndex = 5
        CustomerCodeLabel.Text = "รหัสลูกค้า"
        '
        'CustomerNameLabel
        '
        CustomerNameLabel.AutoSize = True
        CustomerNameLabel.Location = New System.Drawing.Point(12, 75)
        CustomerNameLabel.Name = "CustomerNameLabel"
        CustomerNameLabel.Size = New System.Drawing.Size(68, 24)
        CustomerNameLabel.TabIndex = 7
        CustomerNameLabel.Text = "ชื่อลูกค้า"
        '
        'TelephoneNumberLabel
        '
        TelephoneNumberLabel.AutoSize = True
        TelephoneNumberLabel.Location = New System.Drawing.Point(12, 109)
        TelephoneNumberLabel.Name = "TelephoneNumberLabel"
        TelephoneNumberLabel.Size = New System.Drawing.Size(105, 24)
        TelephoneNumberLabel.TabIndex = 9
        TelephoneNumberLabel.Text = "เบอร์โทรศัพท์"
        '
        'SMSNumberLabel
        '
        SMSNumberLabel.AutoSize = True
        SMSNumberLabel.Location = New System.Drawing.Point(233, 109)
        SMSNumberLabel.Name = "SMSNumberLabel"
        SMSNumberLabel.Size = New System.Drawing.Size(82, 24)
        SMSNumberLabel.TabIndex = 11
        SMSNumberLabel.Text = "เบอรมือถือ"
        '
        'FaxNumberLabel
        '
        FaxNumberLabel.AutoSize = True
        FaxNumberLabel.Location = New System.Drawing.Point(12, 144)
        FaxNumberLabel.Name = "FaxNumberLabel"
        FaxNumberLabel.Size = New System.Drawing.Size(62, 24)
        FaxNumberLabel.TabIndex = 13
        FaxNumberLabel.Text = "โทรสาร"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Location = New System.Drawing.Point(12, 178)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(44, 24)
        EmailLabel.TabIndex = 15
        EmailLabel.Text = "อีเมล์"
        '
        'Email2Label
        '
        Email2Label.AutoSize = True
        Email2Label.Location = New System.Drawing.Point(272, 177)
        Email2Label.Name = "Email2Label"
        Email2Label.Size = New System.Drawing.Size(59, 24)
        Email2Label.TabIndex = 17
        Email2Label.Text = "อีเมล์ 2"
        '
        'IDCardLabel
        '
        IDCardLabel.AutoSize = True
        IDCardLabel.Location = New System.Drawing.Point(233, 143)
        IDCardLabel.Name = "IDCardLabel"
        IDCardLabel.Size = New System.Drawing.Size(164, 24)
        IDCardLabel.TabIndex = 19
        IDCardLabel.Text = "เลขประจำตัวผู้เสียภาษี"
        '
        'AddressNumberLabel
        '
        AddressNumberLabel.AutoSize = True
        AddressNumberLabel.Location = New System.Drawing.Point(12, 211)
        AddressNumberLabel.Name = "AddressNumberLabel"
        AddressNumberLabel.Size = New System.Drawing.Size(46, 24)
        AddressNumberLabel.TabIndex = 21
        AddressNumberLabel.Text = "เลขที่"
        '
        'AddressRoadLabel
        '
        AddressRoadLabel.AutoSize = True
        AddressRoadLabel.Location = New System.Drawing.Point(183, 211)
        AddressRoadLabel.Name = "AddressRoadLabel"
        AddressRoadLabel.Size = New System.Drawing.Size(82, 24)
        AddressRoadLabel.TabIndex = 23
        AddressRoadLabel.Text = "ซอย/ถนน"
        '
        'AddressTownLabel
        '
        AddressTownLabel.AutoSize = True
        AddressTownLabel.Location = New System.Drawing.Point(12, 245)
        AddressTownLabel.Name = "AddressTownLabel"
        AddressTownLabel.Size = New System.Drawing.Size(49, 24)
        AddressTownLabel.TabIndex = 25
        AddressTownLabel.Text = "ตำบล"
        '
        'AddressCityLabel
        '
        AddressCityLabel.AutoSize = True
        AddressCityLabel.Location = New System.Drawing.Point(12, 279)
        AddressCityLabel.Name = "AddressCityLabel"
        AddressCityLabel.Size = New System.Drawing.Size(52, 24)
        AddressCityLabel.TabIndex = 27
        AddressCityLabel.Text = "อำเภอ"
        '
        'ProvinceLabel
        '
        ProvinceLabel.AutoSize = True
        ProvinceLabel.Location = New System.Drawing.Point(12, 313)
        ProvinceLabel.Name = "ProvinceLabel"
        ProvinceLabel.Size = New System.Drawing.Size(57, 24)
        ProvinceLabel.TabIndex = 29
        ProvinceLabel.Text = "จังหวัด"
        '
        'PostalCodeLabel
        '
        PostalCodeLabel.AutoSize = True
        PostalCodeLabel.Location = New System.Drawing.Point(327, 313)
        PostalCodeLabel.Name = "PostalCodeLabel"
        PostalCodeLabel.Size = New System.Drawing.Size(104, 24)
        PostalCodeLabel.TabIndex = 31
        PostalCodeLabel.Text = "รหัสไปรษณีย์"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(1, 347)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(114, 24)
        UpdateUserLabel.TabIndex = 33
        UpdateUserLabel.Text = "แก้ไขล่าสุดโดย"
        '
        'CutomerTypeLabel
        '
        CutomerTypeLabel.AutoSize = True
        CutomerTypeLabel.Location = New System.Drawing.Point(12, 39)
        CutomerTypeLabel.Name = "CutomerTypeLabel"
        CutomerTypeLabel.Size = New System.Drawing.Size(104, 24)
        CutomerTypeLabel.TabIndex = 36
        CutomerTypeLabel.Text = "ประเภทลูกค้า"
        '
        'CustomerBindingNavigator
        '
        Me.CustomerBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.CustomerBindingNavigator.BindingSource = Me.CustomerBindingSource
        Me.CustomerBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.CustomerBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.CustomerBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CustomerBindingNavigator.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CustomerBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.CustomerBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btReload, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.CustomerBindingNavigatorSaveItem, Me.ToolStripSeparator1, Me.ToolStripLabel2, Me.ToolStripTextBox1, Me.btSearch})
        Me.CustomerBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CustomerBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.CustomerBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.CustomerBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.CustomerBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.CustomerBindingNavigator.Name = "CustomerBindingNavigator"
        Me.CustomerBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.CustomerBindingNavigator.Size = New System.Drawing.Size(816, 31)
        Me.CustomerBindingNavigator.TabIndex = 0
        Me.CustomerBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'CustomerBindingSource
        '
        Me.CustomerBindingSource.DataMember = "Customer"
        Me.CustomerBindingSource.DataSource = Me.RealTimeDataSet
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(90, 28)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(108, 28)
        Me.ToolStripLabel1.Text = "ทะเบียนลูกค้า"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 31)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 31)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 28)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 31)
        '
        'btReload
        '
        Me.btReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(24, 28)
        Me.btReload.Text = "ดึงข้อมูลใหม่"
        '
        'CustomerBindingNavigatorSaveItem
        '
        Me.CustomerBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CustomerBindingNavigatorSaveItem.Image = CType(resources.GetObject("CustomerBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CustomerBindingNavigatorSaveItem.Name = "CustomerBindingNavigatorSaveItem"
        Me.CustomerBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 28)
        Me.CustomerBindingNavigatorSaveItem.Text = "Save Data"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(92, 28)
        Me.ToolStripLabel2.Text = "ค้นหาลูกค้า"
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(100, 31)
        '
        'btSearch
        '
        Me.btSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btSearch.Image = CType(resources.GetObject("btSearch.Image"), System.Drawing.Image)
        Me.btSearch.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btSearch.Name = "btSearch"
        Me.btSearch.Size = New System.Drawing.Size(58, 28)
        Me.btSearch.Text = "ค้นหา"
        '
        'CustomerCodeTextBox
        '
        Me.CustomerCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CustomerCode", True))
        Me.CustomerCodeTextBox.Location = New System.Drawing.Point(382, 35)
        Me.CustomerCodeTextBox.Name = "CustomerCodeTextBox"
        Me.CustomerCodeTextBox.Size = New System.Drawing.Size(175, 28)
        Me.CustomerCodeTextBox.TabIndex = 6
        '
        'CustomerNameTextBox
        '
        Me.CustomerNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "CustomerName", True))
        Me.CustomerNameTextBox.Location = New System.Drawing.Point(121, 72)
        Me.CustomerNameTextBox.Name = "CustomerNameTextBox"
        Me.CustomerNameTextBox.Size = New System.Drawing.Size(350, 28)
        Me.CustomerNameTextBox.TabIndex = 8
        '
        'TelephoneNumberTextBox
        '
        Me.TelephoneNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TelephoneNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "TelephoneNumber", True))
        Me.TelephoneNumberTextBox.Location = New System.Drawing.Point(121, 106)
        Me.TelephoneNumberTextBox.Name = "TelephoneNumberTextBox"
        Me.TelephoneNumberTextBox.Size = New System.Drawing.Size(106, 28)
        Me.TelephoneNumberTextBox.TabIndex = 10
        '
        'SMSNumberTextBox
        '
        Me.SMSNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SMSNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "SMSNumber", True))
        Me.SMSNumberTextBox.Location = New System.Drawing.Point(327, 105)
        Me.SMSNumberTextBox.Name = "SMSNumberTextBox"
        Me.SMSNumberTextBox.Size = New System.Drawing.Size(144, 28)
        Me.SMSNumberTextBox.TabIndex = 12
        '
        'FaxNumberTextBox
        '
        Me.FaxNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FaxNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "FaxNumber", True))
        Me.FaxNumberTextBox.Location = New System.Drawing.Point(121, 140)
        Me.FaxNumberTextBox.Name = "FaxNumberTextBox"
        Me.FaxNumberTextBox.Size = New System.Drawing.Size(106, 28)
        Me.FaxNumberTextBox.TabIndex = 14
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "Email", True))
        Me.EmailTextBox.Location = New System.Drawing.Point(121, 174)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(145, 28)
        Me.EmailTextBox.TabIndex = 16
        '
        'Email2TextBox
        '
        Me.Email2TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Email2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "Email2", True))
        Me.Email2TextBox.Location = New System.Drawing.Point(337, 173)
        Me.Email2TextBox.Name = "Email2TextBox"
        Me.Email2TextBox.Size = New System.Drawing.Size(134, 28)
        Me.Email2TextBox.TabIndex = 18
        '
        'IDCardTextBox
        '
        Me.IDCardTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.IDCardTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "IDCard", True))
        Me.IDCardTextBox.Location = New System.Drawing.Point(403, 140)
        Me.IDCardTextBox.Name = "IDCardTextBox"
        Me.IDCardTextBox.Size = New System.Drawing.Size(154, 28)
        Me.IDCardTextBox.TabIndex = 20
        '
        'AddressNumberTextBox
        '
        Me.AddressNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "AddressNumber", True))
        Me.AddressNumberTextBox.Location = New System.Drawing.Point(121, 208)
        Me.AddressNumberTextBox.Name = "AddressNumberTextBox"
        Me.AddressNumberTextBox.Size = New System.Drawing.Size(56, 28)
        Me.AddressNumberTextBox.TabIndex = 22
        '
        'AddressRoadTextBox
        '
        Me.AddressRoadTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressRoadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "AddressRoad", True))
        Me.AddressRoadTextBox.Location = New System.Drawing.Point(271, 208)
        Me.AddressRoadTextBox.Name = "AddressRoadTextBox"
        Me.AddressRoadTextBox.Size = New System.Drawing.Size(200, 28)
        Me.AddressRoadTextBox.TabIndex = 24
        '
        'AddressTownTextBox
        '
        Me.AddressTownTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressTownTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "AddressTown", True))
        Me.AddressTownTextBox.Location = New System.Drawing.Point(121, 242)
        Me.AddressTownTextBox.Name = "AddressTownTextBox"
        Me.AddressTownTextBox.Size = New System.Drawing.Size(194, 28)
        Me.AddressTownTextBox.TabIndex = 26
        '
        'AddressCityTextBox
        '
        Me.AddressCityTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "AddressCity", True))
        Me.AddressCityTextBox.Location = New System.Drawing.Point(121, 276)
        Me.AddressCityTextBox.Name = "AddressCityTextBox"
        Me.AddressCityTextBox.Size = New System.Drawing.Size(194, 28)
        Me.AddressCityTextBox.TabIndex = 28
        '
        'ProvinceTextBox
        '
        Me.ProvinceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProvinceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "Province", True))
        Me.ProvinceTextBox.Location = New System.Drawing.Point(121, 310)
        Me.ProvinceTextBox.Name = "ProvinceTextBox"
        Me.ProvinceTextBox.Size = New System.Drawing.Size(194, 28)
        Me.ProvinceTextBox.TabIndex = 30
        '
        'PostalCodeTextBox
        '
        Me.PostalCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PostalCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "PostalCode", True))
        Me.PostalCodeTextBox.Location = New System.Drawing.Point(437, 310)
        Me.PostalCodeTextBox.MaxLength = 5
        Me.PostalCodeTextBox.Name = "PostalCodeTextBox"
        Me.PostalCodeTextBox.Size = New System.Drawing.Size(80, 28)
        Me.PostalCodeTextBox.TabIndex = 32
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(121, 344)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(96, 28)
        Me.UpdateUserTextBox.TabIndex = 34
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CustomerBindingSource, "UpdateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm"))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(223, 343)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(180, 28)
        Me.UpdateWhenTextBox.TabIndex = 36
        '
        'CustomerTableAdapter
        '
        Me.CustomerTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CustomerTableAdapter = Me.CustomerTableAdapter
        Me.TableAdapterManager.EmployeeMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CutomerTypeComboBox
        '
        Me.CutomerTypeComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CustomerBindingSource, "CutomerType", True))
        Me.CutomerTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CutomerTypeComboBox.FormattingEnabled = True
        Me.CutomerTypeComboBox.Location = New System.Drawing.Point(121, 36)
        Me.CutomerTypeComboBox.Name = "CutomerTypeComboBox"
        Me.CutomerTypeComboBox.Size = New System.Drawing.Size(177, 30)
        Me.CutomerTypeComboBox.TabIndex = 37
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(CutomerTypeLabel)
        Me.GroupBox1.Controls.Add(Me.CutomerTypeComboBox)
        Me.GroupBox1.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox1.Controls.Add(CustomerCodeLabel)
        Me.GroupBox1.Controls.Add(AddressTownLabel)
        Me.GroupBox1.Controls.Add(Me.AddressRoadTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressTownTextBox)
        Me.GroupBox1.Controls.Add(AddressCityLabel)
        Me.GroupBox1.Controls.Add(Me.CustomerCodeTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressCityTextBox)
        Me.GroupBox1.Controls.Add(AddressRoadLabel)
        Me.GroupBox1.Controls.Add(ProvinceLabel)
        Me.GroupBox1.Controls.Add(CustomerNameLabel)
        Me.GroupBox1.Controls.Add(Me.ProvinceTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressNumberTextBox)
        Me.GroupBox1.Controls.Add(PostalCodeLabel)
        Me.GroupBox1.Controls.Add(Me.CustomerNameTextBox)
        Me.GroupBox1.Controls.Add(Me.PostalCodeTextBox)
        Me.GroupBox1.Controls.Add(AddressNumberLabel)
        Me.GroupBox1.Controls.Add(UpdateUserLabel)
        Me.GroupBox1.Controls.Add(TelephoneNumberLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox1.Controls.Add(Me.IDCardTextBox)
        Me.GroupBox1.Controls.Add(Me.TelephoneNumberTextBox)
        Me.GroupBox1.Controls.Add(IDCardLabel)
        Me.GroupBox1.Controls.Add(SMSNumberLabel)
        Me.GroupBox1.Controls.Add(Me.Email2TextBox)
        Me.GroupBox1.Controls.Add(Me.SMSNumberTextBox)
        Me.GroupBox1.Controls.Add(Email2Label)
        Me.GroupBox1.Controls.Add(FaxNumberLabel)
        Me.GroupBox1.Controls.Add(Me.EmailTextBox)
        Me.GroupBox1.Controls.Add(Me.FaxNumberTextBox)
        Me.GroupBox1.Controls.Add(EmailLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 30)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(568, 390)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ข้อมูลพื้นฐาน"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.PictureBox1)
        Me.GroupBox2.Location = New System.Drawing.Point(577, 30)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(214, 246)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "รูปประจำตัว"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(6, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(200, 200)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'CustomerPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CustomerBindingNavigator)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "CustomerPanel"
        Me.Size = New System.Drawing.Size(816, 483)
        CType(Me.CustomerBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CustomerBindingNavigator.ResumeLayout(False)
        Me.CustomerBindingNavigator.PerformLayout()
        CType(Me.CustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents CustomerBindingSource As BindingSource
    Friend WithEvents CustomerTableAdapter As RealTimeDataSetTableAdapters.CustomerTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents CustomerBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents CustomerBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents btReload As ToolStripButton
    Friend WithEvents CustomerCodeTextBox As TextBox
    Friend WithEvents CustomerNameTextBox As TextBox
    Friend WithEvents TelephoneNumberTextBox As TextBox
    Friend WithEvents SMSNumberTextBox As TextBox
    Friend WithEvents FaxNumberTextBox As TextBox
    Friend WithEvents EmailTextBox As TextBox
    Friend WithEvents Email2TextBox As TextBox
    Friend WithEvents IDCardTextBox As TextBox
    Friend WithEvents AddressNumberTextBox As TextBox
    Friend WithEvents AddressRoadTextBox As TextBox
    Friend WithEvents AddressTownTextBox As TextBox
    Friend WithEvents AddressCityTextBox As TextBox
    Friend WithEvents ProvinceTextBox As TextBox
    Friend WithEvents PostalCodeTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents CutomerTypeComboBox As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ToolStripLabel2 As ToolStripLabel
    Friend WithEvents ToolStripTextBox1 As ToolStripTextBox
    Friend WithEvents btSearch As ToolStripButton
End Class
