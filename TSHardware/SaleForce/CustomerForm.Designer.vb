﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CustomerForm
    Inherits ChildForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CustomerPanel1 = New TSHardware.CustomerPanel()
        Me.SuspendLayout()
        '
        'CustomerPanel1
        '
        Me.CustomerPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CustomerPanel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CustomerPanel1.Location = New System.Drawing.Point(0, 0)
        Me.CustomerPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.CustomerPanel1.Name = "CustomerPanel1"
        Me.CustomerPanel1.Size = New System.Drawing.Size(762, 530)
        Me.CustomerPanel1.TabIndex = 0
        '
        'CustomerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(762, 530)
        Me.Controls.Add(Me.CustomerPanel1)
        Me.Name = "CustomerForm"
        Me.Text = "CustomerForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents CustomerPanel1 As CustomerPanel
End Class
