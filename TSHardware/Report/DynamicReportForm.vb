﻿Public Class DynamicReportForm

    Private _Parameter As String
    Public Property Parameter() As String
        Get
            Return _Parameter
        End Get
        Set(ByVal value As String)
            _Parameter = value
        End Set
    End Property
    Private _reportnumber As String
    Public Property ReportNumber() As String
        Get
            Return _reportnumber
        End Get
        Set(ByVal value As String)
            _reportnumber = value
        End Set
    End Property

    Public Sub Reload()

        Me.ReportViwerPanel1.ReportNumber = _reportnumber
        Me.ReportViwerPanel1.ParameterText = _Parameter
        Me.ReportViwerPanel1.Reload()
        Me.ReportViwerPanel1.Visible = True

    End Sub

    Private Sub DynamicReportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load


    End Sub

    Private Sub DynamicReportForm_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Reload()
    End Sub
End Class