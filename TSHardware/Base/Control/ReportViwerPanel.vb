﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports System.Globalization
Imports System.Diagnostics

Public Class ReportViwerPanel
    Private crystalDocument As ReportDocument = New ReportDocument()

    Private _DefaultDBServer = String.Empty
    Private _DefaultPath = String.Empty
    Private _ParameterFildText As String = String.Empty
    Public Property ParameterText() As String
        Get
            Return _ParameterFildText
        End Get
        Set(ByVal value As String)
            _ParameterFildText = value
        End Set
    End Property
    Private _ReportNumber As String
    Public Property ReportNumber() As String
        Get
            Return _ReportNumber
        End Get
        Set(ByVal value As String)
            _ReportNumber = value
        End Set
    End Property

    Private Sub initialtoolbar()
        Me.crystalViewer.ShowPrintButton = False
        Me.crystalViewer.ShowRefreshButton = False
        Me.crystalViewer.ShowExportButton = False
        Me.crystalViewer.ShowCloseButton = False
        Me.crystalViewer.ShowLogo = False
        Me.crystalViewer.ShowCopyButton = False
        Me.crystalViewer.ShowParameterPanelButton = False
        Me.crystalViewer.ShowGroupTreeButton = False
    End Sub

    Public Sub Reload()

        Try

            _DefaultPath = GlobalV.ERPPath + "\\Report"
            _DefaultDBServer = GlobalV.DataBaseConnectionString


            Me.crystalViewer.Visible = False
            Dim filefullPath = String.Format("{0}\\{1}.rpt", _DefaultPath, _ReportNumber)

            If Not File.Exists(filefullPath) Then Return
            Dim crtableLogoninfos = New TableLogOnInfos()
            Dim crtableLogoninfo = New TableLogOnInfo()
            Dim crConnectionInfo = New ConnectionInfo()
            Dim CrTables As Tables
            crystalDocument = New ReportDocument()
            crystalDocument.Load(filefullPath)
            crConnectionInfo.ServerName = _DefaultDBServer
            CrTables = crystalDocument.Database.Tables

            For Each CrTable As CrystalDecisions.CrystalReports.Engine.Table In CrTables
                crtableLogoninfo = CrTable.LogOnInfo
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                CrTable.ApplyLogOnInfo(crtableLogoninfo)
            Next


            Me.crystalDocument.SetDatabaseLogon("", "", _DefaultDBServer, "")
            Me.crystalViewer.ShowGroupTreeButton = False

            If Not _ParameterFildText = Nothing Then
                If _ParameterFildText.Length > 0 Then
                    PassReportParameters(_ParameterFildText)
                End If
            End If



            Me.crystalViewer.ReportSource = crystalDocument
            Me.crystalViewer.ShowGroupTree()
            Me.crystalViewer.Refresh()
            Me.crystalViewer.Visible = True
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

    End Sub

    Private Sub PassReportParameters()
        Dim parameterFieldDefinition As ParameterFieldDefinition
        Dim parameterFields As ParameterFields
        Dim parameterField As ParameterField
        Dim parameterValues As ParameterValues
        Dim parameterDiscreteValue As ParameterDiscreteValue
        Dim provider As CultureInfo = CultureInfo.InvariantCulture
        Dim dateParameter As DateTime
        Dim stringParameter As String
        Dim intParameter As Integer
        Dim paramFieldName, paramFieldValues As String
        Dim paramFieldValue As String()
        If crystalDocument.ParameterFields.Count > 0 Then
            If _ParameterFildText IsNot Nothing Then
                crystalViewer.ReuseParameterValuesOnRefresh = True
                parameterFields = crystalViewer.ParameterFieldInfo
                For Each parameter As String In _ParameterFildText.Split(";"c)
                    Try
                        Dim parameterfieldtemp = parameter.Split("="c)
                        If parameterfieldtemp.Length = 2 Then
                            paramFieldName = parameterfieldtemp(0)
                            paramFieldValues = parameterfieldtemp(1)
                            parameterFieldDefinition = crystalDocument.DataDefinition.ParameterFields(paramFieldName)
                            If parameterFieldDefinition.DiscreteOrRangeKind = DiscreteOrRangeKind.DiscreteValue Then
                                parameterField = parameterFields(paramFieldName)
                                parameterValues = parameterField.CurrentValues
                                parameterValues.Clear()
                                paramFieldValue = paramFieldValues.Split(","c)
                                For Each paramValue As String In paramFieldValue
                                    parameterDiscreteValue = New ParameterDiscreteValue()
                                    If paramValue.Length = 12 AndAlso paramValue.Substring(0, 1) = "#" AndAlso paramValue.Substring(5, 1) = "-" AndAlso paramValue.Substring(8, 1) = "-" AndAlso paramValue.Substring(11, 1) = "#" Then
                                        dateParameter = New DateTime()
                                        dateParameter = DateTime.ParseExact(paramValue.Substring(1, 10), "yyyy-MM-dd", provider)
                                        parameterDiscreteValue.Value = dateParameter
                                    ElseIf (paramValue.Substring(0, 1) = """" AndAlso paramValue.Substring(paramValue.Length - 1, 1) = """") OrElse (paramValue.Substring(0, 1) = "'" AndAlso paramValue.Substring(paramValue.Length - 1, 1) = "'") Then
                                        stringParameter = paramValue.Substring(1, paramValue.Length - 2)
                                        parameterDiscreteValue.Value = stringParameter
                                    Else
                                        intParameter = Integer.Parse(paramValue)
                                        parameterDiscreteValue.Value = intParameter
                                    End If

                                    parameterValues.Add(parameterDiscreteValue)
                                Next
                            End If
                        End If
                    Catch ex As Exception
                        Debug.Print("Error Passing Parameter: " & ex.Message)
                    End Try
                Next
            End If
        End If
    End Sub

    Private Sub PassReportParametersDefault()
        Dim parameterValues As ParameterValues
        Dim parameterDiscreteValue As ParameterDiscreteValue
        Dim provider As CultureInfo = CultureInfo.InvariantCulture
        Dim stringParameter As String
        If crystalViewer.ParameterFieldInfo.Count > 0 Then
            Try
                For Each parameterField As ParameterField In crystalViewer.ParameterFieldInfo
                    If parameterField.ParameterValueType = ParameterValueKind.StringParameter Then
                        parameterValues = parameterField.CurrentValues
                        parameterDiscreteValue = New ParameterDiscreteValue()
                        stringParameter = "*"
                        parameterDiscreteValue.Value = stringParameter
                        parameterValues.Clear()
                        parameterValues.Add(parameterDiscreteValue)
                    End If
                Next

                crystalViewer.ReuseParameterValuesOnRefresh = True
            Catch ex As Exception
                Debug.Print("Error Passing Parameter: " & ex.Message)
            End Try
        End If
    End Sub

    Private Sub PassReportParameters(ByVal ParameterFieldsText As String)
        Dim parameterField As CrystalDecisions.[Shared].ParameterField
        Dim parameterValues As ParameterValues
        Dim parameterDiscreteValue As ParameterDiscreteValue
        Dim provider As CultureInfo = CultureInfo.InvariantCulture
        Dim dateParameter As DateTime
        Dim stringParameter As String
        Dim intParameter As Integer
        Dim paramFieldName, paramFieldValues As String
        Dim paramFieldValue As String()
        For Each parameter As String In ParameterFieldsText.Split(";"c)
            Try
                Dim parameterfieldtemp = parameter.Split("="c)
                If parameterfieldtemp.Length = 2 Then
                    paramFieldName = parameterfieldtemp(0)
                    paramFieldValues = parameterfieldtemp(1)
                    parameterField = Me.crystalDocument.ParameterFields(paramFieldName)
                    If parameterField.DiscreteOrRangeKind = DiscreteOrRangeKind.DiscreteValue Then
                        parameterValues = parameterField.CurrentValues
                        parameterValues.Clear()
                        paramFieldValue = paramFieldValues.Split(","c)
                        For Each paramValue As String In paramFieldValue
                            parameterDiscreteValue = New ParameterDiscreteValue()
                            If paramValue.Length = 12 AndAlso paramValue.Substring(0, 1) = "#" AndAlso paramValue.Substring(5, 1) = "-" AndAlso paramValue.Substring(8, 1) = "-" AndAlso paramValue.Substring(11, 1) = "#" Then
                                dateParameter = New DateTime()
                                dateParameter = DateTime.ParseExact(paramValue.Substring(1, 10), "yyyy-MM-dd", provider)
                                parameterDiscreteValue.Value = dateParameter
                            ElseIf paramValue.Substring(0, 1) = """" AndAlso paramValue.Substring(paramValue.Length - 1, 1) = """" Then
                                stringParameter = paramValue.Substring(1, paramValue.Length - 2)
                                parameterDiscreteValue.Value = stringParameter
                            Else
                                intParameter = Integer.Parse(paramValue)
                                parameterDiscreteValue.Value = intParameter
                            End If

                            parameterValues.Add(parameterDiscreteValue)
                        Next
                    End If
                End If
            Catch ex As Exception
                Debug.Print("Error Passing Parameter: " & ex.Message)
            End Try
        Next
    End Sub

    Private Sub RelinkReport()

    End Sub
End Class
