﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class TableEntity
    Public Records As DataTable = New DataTable()
    Public _DataBasePath As String = String.Empty


    Public Function SQLQuery(ByVal sql As String) As Boolean
        Dim isSuccess As Boolean = False
        Dim table As DataTable = New DataTable()

        If String.IsNullOrEmpty(_DataBasePath) Then
            'DataBasePath = ConfigurationManager.ConnectionStrings("TSHardware.My.MySettings.RealTimeConnectionString").ConnectionString
            _DataBasePath = My.Settings.Item("RealTimeConnectionString").ToString()
        End If
        Try

            Using cnn As New OleDb.OleDbConnection(_DataBasePath)
                cnn.Open()
                Using dad As New OleDb.OleDbDataAdapter(sql, cnn)
                    dad.Fill(table)
                    isSuccess = True
                End Using
                cnn.Close()
            End Using

            Records = table
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        Return isSuccess
    End Function
End Class
