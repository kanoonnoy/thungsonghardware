﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmployeeMasterPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim EmployeeCodeLabel As System.Windows.Forms.Label
        Dim EmployeeNameLabel As System.Windows.Forms.Label
        Dim FullNameThaiLabel As System.Windows.Forms.Label
        Dim NickNameLabel As System.Windows.Forms.Label
        Dim TelephoneNumberLabel As System.Windows.Forms.Label
        Dim SMSNumberLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim IDCardLabel As System.Windows.Forms.Label
        Dim AddressNumberLabel As System.Windows.Forms.Label
        Dim AddressRoadLabel As System.Windows.Forms.Label
        Dim AddressTownLabel As System.Windows.Forms.Label
        Dim AddressCityLabel As System.Windows.Forms.Label
        Dim ProvinceLabel As System.Windows.Forms.Label
        Dim PostalCodeLabel As System.Windows.Forms.Label
        Dim BrthdayLabel As System.Windows.Forms.Label
        Dim StartDateLabel As System.Windows.Forms.Label
        Dim EndDateLabel As System.Windows.Forms.Label
        Dim UserNameLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Dim CreateWhenLabel As System.Windows.Forms.Label
        Dim UpdateWhenLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EmployeeMasterPanel))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.EmployeeCodeTextBox = New System.Windows.Forms.TextBox()
        Me.EmployeeMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.EmployeeNameTextBox = New System.Windows.Forms.TextBox()
        Me.FullNameThaiTextBox = New System.Windows.Forms.TextBox()
        Me.NickNameTextBox = New System.Windows.Forms.TextBox()
        Me.TelephoneNumberTextBox = New System.Windows.Forms.TextBox()
        Me.SMSNumberTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.IDCardTextBox = New System.Windows.Forms.TextBox()
        Me.AddressNumberTextBox = New System.Windows.Forms.TextBox()
        Me.AddressRoadTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTownTextBox = New System.Windows.Forms.TextBox()
        Me.AddressCityTextBox = New System.Windows.Forms.TextBox()
        Me.ProvinceTextBox = New System.Windows.Forms.TextBox()
        Me.PostalCodeTextBox = New System.Windows.Forms.TextBox()
        Me.BrthdayMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.StartDateMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.EndDateMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
        Me.UserNameTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.CreateUserTextBox = New System.Windows.Forms.TextBox()
        Me.CreateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.EmployeeMasterTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.EmployeeMasterTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.EmployeeMasterBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.EmployeeMasterBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        EmployeeCodeLabel = New System.Windows.Forms.Label()
        EmployeeNameLabel = New System.Windows.Forms.Label()
        FullNameThaiLabel = New System.Windows.Forms.Label()
        NickNameLabel = New System.Windows.Forms.Label()
        TelephoneNumberLabel = New System.Windows.Forms.Label()
        SMSNumberLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        IDCardLabel = New System.Windows.Forms.Label()
        AddressNumberLabel = New System.Windows.Forms.Label()
        AddressRoadLabel = New System.Windows.Forms.Label()
        AddressTownLabel = New System.Windows.Forms.Label()
        AddressCityLabel = New System.Windows.Forms.Label()
        ProvinceLabel = New System.Windows.Forms.Label()
        PostalCodeLabel = New System.Windows.Forms.Label()
        BrthdayLabel = New System.Windows.Forms.Label()
        StartDateLabel = New System.Windows.Forms.Label()
        EndDateLabel = New System.Windows.Forms.Label()
        UserNameLabel = New System.Windows.Forms.Label()
        PasswordLabel = New System.Windows.Forms.Label()
        CreateWhenLabel = New System.Windows.Forms.Label()
        UpdateWhenLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.EmployeeMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployeeMasterBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EmployeeMasterBindingNavigator.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'EmployeeCodeLabel
        '
        EmployeeCodeLabel.AutoSize = True
        EmployeeCodeLabel.Location = New System.Drawing.Point(6, 37)
        EmployeeCodeLabel.Name = "EmployeeCodeLabel"
        EmployeeCodeLabel.Size = New System.Drawing.Size(102, 24)
        EmployeeCodeLabel.TabIndex = 2
        EmployeeCodeLabel.Text = "รหัสพนักงาน"
        '
        'EmployeeNameLabel
        '
        EmployeeNameLabel.AutoSize = True
        EmployeeNameLabel.Location = New System.Drawing.Point(6, 71)
        EmployeeNameLabel.Name = "EmployeeNameLabel"
        EmployeeNameLabel.Size = New System.Drawing.Size(117, 24)
        EmployeeNameLabel.TabIndex = 4
        EmployeeNameLabel.Text = "ชื่อภาษาอังกฤษ"
        '
        'FullNameThaiLabel
        '
        FullNameThaiLabel.AutoSize = True
        FullNameThaiLabel.Location = New System.Drawing.Point(214, 36)
        FullNameThaiLabel.Name = "FullNameThaiLabel"
        FullNameThaiLabel.Size = New System.Drawing.Size(100, 24)
        FullNameThaiLabel.TabIndex = 6
        FullNameThaiLabel.Text = "ชื่อภาษาไทย"
        '
        'NickNameLabel
        '
        NickNameLabel.AutoSize = True
        NickNameLabel.Location = New System.Drawing.Point(528, 35)
        NickNameLabel.Name = "NickNameLabel"
        NickNameLabel.Size = New System.Drawing.Size(57, 24)
        NickNameLabel.TabIndex = 8
        NickNameLabel.Text = "ชื่อเล่น"
        '
        'TelephoneNumberLabel
        '
        TelephoneNumberLabel.AutoSize = True
        TelephoneNumberLabel.Location = New System.Drawing.Point(6, 105)
        TelephoneNumberLabel.Name = "TelephoneNumberLabel"
        TelephoneNumberLabel.Size = New System.Drawing.Size(105, 24)
        TelephoneNumberLabel.TabIndex = 10
        TelephoneNumberLabel.Text = "เบอร์โทรศัพท์"
        '
        'SMSNumberLabel
        '
        SMSNumberLabel.AutoSize = True
        SMSNumberLabel.Location = New System.Drawing.Point(235, 105)
        SMSNumberLabel.Name = "SMSNumberLabel"
        SMSNumberLabel.Size = New System.Drawing.Size(82, 24)
        SMSNumberLabel.TabIndex = 12
        SMSNumberLabel.Text = "เบอร์มือถือ"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Location = New System.Drawing.Point(429, 105)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(44, 24)
        EmailLabel.TabIndex = 16
        EmailLabel.Text = "อีเมล์"
        '
        'IDCardLabel
        '
        IDCardLabel.AutoSize = True
        IDCardLabel.Location = New System.Drawing.Point(235, 71)
        IDCardLabel.Name = "IDCardLabel"
        IDCardLabel.Size = New System.Drawing.Size(169, 24)
        IDCardLabel.TabIndex = 20
        IDCardLabel.Text = "เลขประจำตัวประชาชน"
        '
        'AddressNumberLabel
        '
        AddressNumberLabel.AutoSize = True
        AddressNumberLabel.Location = New System.Drawing.Point(6, 139)
        AddressNumberLabel.Name = "AddressNumberLabel"
        AddressNumberLabel.Size = New System.Drawing.Size(46, 24)
        AddressNumberLabel.TabIndex = 22
        AddressNumberLabel.Text = "เลขที่"
        '
        'AddressRoadLabel
        '
        AddressRoadLabel.AutoSize = True
        AddressRoadLabel.Location = New System.Drawing.Point(197, 139)
        AddressRoadLabel.Name = "AddressRoadLabel"
        AddressRoadLabel.Size = New System.Drawing.Size(82, 24)
        AddressRoadLabel.TabIndex = 24
        AddressRoadLabel.Text = "ซอย/ถนน"
        '
        'AddressTownLabel
        '
        AddressTownLabel.AutoSize = True
        AddressTownLabel.Location = New System.Drawing.Point(6, 173)
        AddressTownLabel.Name = "AddressTownLabel"
        AddressTownLabel.Size = New System.Drawing.Size(49, 24)
        AddressTownLabel.TabIndex = 26
        AddressTownLabel.Text = "ตำบล"
        '
        'AddressCityLabel
        '
        AddressCityLabel.AutoSize = True
        AddressCityLabel.Location = New System.Drawing.Point(351, 173)
        AddressCityLabel.Name = "AddressCityLabel"
        AddressCityLabel.Size = New System.Drawing.Size(52, 24)
        AddressCityLabel.TabIndex = 28
        AddressCityLabel.Text = "อำเภอ"
        '
        'ProvinceLabel
        '
        ProvinceLabel.AutoSize = True
        ProvinceLabel.Location = New System.Drawing.Point(6, 207)
        ProvinceLabel.Name = "ProvinceLabel"
        ProvinceLabel.Size = New System.Drawing.Size(57, 24)
        ProvinceLabel.TabIndex = 30
        ProvinceLabel.Text = "จังหวัด"
        '
        'PostalCodeLabel
        '
        PostalCodeLabel.AutoSize = True
        PostalCodeLabel.Location = New System.Drawing.Point(351, 207)
        PostalCodeLabel.Name = "PostalCodeLabel"
        PostalCodeLabel.Size = New System.Drawing.Size(104, 24)
        PostalCodeLabel.TabIndex = 32
        PostalCodeLabel.Text = "รหัสไปรษณีย์"
        '
        'BrthdayLabel
        '
        BrthdayLabel.AutoSize = True
        BrthdayLabel.Location = New System.Drawing.Point(6, 241)
        BrthdayLabel.Name = "BrthdayLabel"
        BrthdayLabel.Size = New System.Drawing.Size(106, 24)
        BrthdayLabel.TabIndex = 34
        BrthdayLabel.Text = "วันเดือนปีเกิด"
        '
        'StartDateLabel
        '
        StartDateLabel.AutoSize = True
        StartDateLabel.Location = New System.Drawing.Point(6, 275)
        StartDateLabel.Name = "StartDateLabel"
        StartDateLabel.Size = New System.Drawing.Size(83, 24)
        StartDateLabel.TabIndex = 36
        StartDateLabel.Text = "วันเริ่มงาน"
        '
        'EndDateLabel
        '
        EndDateLabel.AutoSize = True
        EndDateLabel.Location = New System.Drawing.Point(292, 276)
        EndDateLabel.Name = "EndDateLabel"
        EndDateLabel.Size = New System.Drawing.Size(147, 24)
        EndDateLabel.TabIndex = 38
        EndDateLabel.Text = "วันสิ้นสุดการทำงาน"
        '
        'UserNameLabel
        '
        UserNameLabel.AutoSize = True
        UserNameLabel.Location = New System.Drawing.Point(6, 308)
        UserNameLabel.Name = "UserNameLabel"
        UserNameLabel.Size = New System.Drawing.Size(88, 24)
        UserNameLabel.TabIndex = 40
        UserNameLabel.Text = "ชื่อผู้ใช้งาน"
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Location = New System.Drawing.Point(6, 342)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(71, 24)
        PasswordLabel.TabIndex = 42
        PasswordLabel.Text = "รหัสผ่าน"
        '
        'CreateWhenLabel
        '
        CreateWhenLabel.AutoSize = True
        CreateWhenLabel.Location = New System.Drawing.Point(372, 311)
        CreateWhenLabel.Name = "CreateWhenLabel"
        CreateWhenLabel.Size = New System.Drawing.Size(67, 24)
        CreateWhenLabel.TabIndex = 46
        CreateWhenLabel.Text = "สร้างเมื่อ"
        '
        'UpdateWhenLabel
        '
        UpdateWhenLabel.AutoSize = True
        UpdateWhenLabel.Location = New System.Drawing.Point(328, 345)
        UpdateWhenLabel.Name = "UpdateWhenLabel"
        UpdateWhenLabel.Size = New System.Drawing.Size(111, 24)
        UpdateWhenLabel.TabIndex = 50
        UpdateWhenLabel.Text = "แก้ไขล่าสุดเมื่อ"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(EmployeeCodeLabel)
        Me.GroupBox1.Controls.Add(Me.EmployeeCodeTextBox)
        Me.GroupBox1.Controls.Add(EmployeeNameLabel)
        Me.GroupBox1.Controls.Add(Me.EmployeeNameTextBox)
        Me.GroupBox1.Controls.Add(FullNameThaiLabel)
        Me.GroupBox1.Controls.Add(Me.FullNameThaiTextBox)
        Me.GroupBox1.Controls.Add(NickNameLabel)
        Me.GroupBox1.Controls.Add(Me.NickNameTextBox)
        Me.GroupBox1.Controls.Add(TelephoneNumberLabel)
        Me.GroupBox1.Controls.Add(Me.TelephoneNumberTextBox)
        Me.GroupBox1.Controls.Add(SMSNumberLabel)
        Me.GroupBox1.Controls.Add(Me.SMSNumberTextBox)
        Me.GroupBox1.Controls.Add(EmailLabel)
        Me.GroupBox1.Controls.Add(Me.EmailTextBox)
        Me.GroupBox1.Controls.Add(IDCardLabel)
        Me.GroupBox1.Controls.Add(Me.IDCardTextBox)
        Me.GroupBox1.Controls.Add(AddressNumberLabel)
        Me.GroupBox1.Controls.Add(Me.AddressNumberTextBox)
        Me.GroupBox1.Controls.Add(AddressRoadLabel)
        Me.GroupBox1.Controls.Add(Me.AddressRoadTextBox)
        Me.GroupBox1.Controls.Add(AddressTownLabel)
        Me.GroupBox1.Controls.Add(Me.AddressTownTextBox)
        Me.GroupBox1.Controls.Add(AddressCityLabel)
        Me.GroupBox1.Controls.Add(Me.AddressCityTextBox)
        Me.GroupBox1.Controls.Add(ProvinceLabel)
        Me.GroupBox1.Controls.Add(Me.ProvinceTextBox)
        Me.GroupBox1.Controls.Add(PostalCodeLabel)
        Me.GroupBox1.Controls.Add(Me.PostalCodeTextBox)
        Me.GroupBox1.Controls.Add(BrthdayLabel)
        Me.GroupBox1.Controls.Add(Me.BrthdayMaskedTextBox)
        Me.GroupBox1.Controls.Add(StartDateLabel)
        Me.GroupBox1.Controls.Add(Me.StartDateMaskedTextBox)
        Me.GroupBox1.Controls.Add(EndDateLabel)
        Me.GroupBox1.Controls.Add(Me.EndDateMaskedTextBox)
        Me.GroupBox1.Controls.Add(UserNameLabel)
        Me.GroupBox1.Controls.Add(Me.UserNameTextBox)
        Me.GroupBox1.Controls.Add(PasswordLabel)
        Me.GroupBox1.Controls.Add(Me.PasswordTextBox)
        Me.GroupBox1.Controls.Add(Me.CreateUserTextBox)
        Me.GroupBox1.Controls.Add(CreateWhenLabel)
        Me.GroupBox1.Controls.Add(Me.CreateWhenTextBox)
        Me.GroupBox1.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox1.Controls.Add(UpdateWhenLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(749, 387)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ข้อมูลพื้นฐาน"
        '
        'EmployeeCodeTextBox
        '
        Me.EmployeeCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EmployeeCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "EmployeeCode", True))
        Me.EmployeeCodeTextBox.Location = New System.Drawing.Point(129, 34)
        Me.EmployeeCodeTextBox.Name = "EmployeeCodeTextBox"
        Me.EmployeeCodeTextBox.Size = New System.Drawing.Size(79, 28)
        Me.EmployeeCodeTextBox.TabIndex = 3
        '
        'EmployeeMasterBindingSource
        '
        Me.EmployeeMasterBindingSource.DataMember = "EmployeeMaster"
        Me.EmployeeMasterBindingSource.DataSource = Me.RealTimeDataSet
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EmployeeNameTextBox
        '
        Me.EmployeeNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EmployeeNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "EmployeeName", True))
        Me.EmployeeNameTextBox.Location = New System.Drawing.Point(129, 68)
        Me.EmployeeNameTextBox.Name = "EmployeeNameTextBox"
        Me.EmployeeNameTextBox.Size = New System.Drawing.Size(100, 28)
        Me.EmployeeNameTextBox.TabIndex = 5
        '
        'FullNameThaiTextBox
        '
        Me.FullNameThaiTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FullNameThaiTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "FullNameThai", True))
        Me.FullNameThaiTextBox.Location = New System.Drawing.Point(320, 33)
        Me.FullNameThaiTextBox.Name = "FullNameThaiTextBox"
        Me.FullNameThaiTextBox.Size = New System.Drawing.Size(198, 28)
        Me.FullNameThaiTextBox.TabIndex = 7
        '
        'NickNameTextBox
        '
        Me.NickNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.NickNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "NickName", True))
        Me.NickNameTextBox.Location = New System.Drawing.Point(591, 32)
        Me.NickNameTextBox.Name = "NickNameTextBox"
        Me.NickNameTextBox.Size = New System.Drawing.Size(100, 28)
        Me.NickNameTextBox.TabIndex = 9
        '
        'TelephoneNumberTextBox
        '
        Me.TelephoneNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TelephoneNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "TelephoneNumber", True))
        Me.TelephoneNumberTextBox.Location = New System.Drawing.Point(129, 102)
        Me.TelephoneNumberTextBox.Name = "TelephoneNumberTextBox"
        Me.TelephoneNumberTextBox.Size = New System.Drawing.Size(100, 28)
        Me.TelephoneNumberTextBox.TabIndex = 11
        '
        'SMSNumberTextBox
        '
        Me.SMSNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SMSNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "SMSNumber", True))
        Me.SMSNumberTextBox.Location = New System.Drawing.Point(323, 102)
        Me.SMSNumberTextBox.Name = "SMSNumberTextBox"
        Me.SMSNumberTextBox.Size = New System.Drawing.Size(100, 28)
        Me.SMSNumberTextBox.TabIndex = 13
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "Email", True))
        Me.EmailTextBox.Location = New System.Drawing.Point(479, 102)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(167, 28)
        Me.EmailTextBox.TabIndex = 17
        '
        'IDCardTextBox
        '
        Me.IDCardTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.IDCardTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "IDCard", True))
        Me.IDCardTextBox.Location = New System.Drawing.Point(410, 68)
        Me.IDCardTextBox.Name = "IDCardTextBox"
        Me.IDCardTextBox.Size = New System.Drawing.Size(214, 28)
        Me.IDCardTextBox.TabIndex = 21
        '
        'AddressNumberTextBox
        '
        Me.AddressNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "AddressNumber", True))
        Me.AddressNumberTextBox.Location = New System.Drawing.Point(129, 136)
        Me.AddressNumberTextBox.Name = "AddressNumberTextBox"
        Me.AddressNumberTextBox.Size = New System.Drawing.Size(62, 28)
        Me.AddressNumberTextBox.TabIndex = 23
        '
        'AddressRoadTextBox
        '
        Me.AddressRoadTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressRoadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "AddressRoad", True))
        Me.AddressRoadTextBox.Location = New System.Drawing.Point(280, 136)
        Me.AddressRoadTextBox.Name = "AddressRoadTextBox"
        Me.AddressRoadTextBox.Size = New System.Drawing.Size(100, 28)
        Me.AddressRoadTextBox.TabIndex = 25
        '
        'AddressTownTextBox
        '
        Me.AddressTownTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressTownTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "AddressTown", True))
        Me.AddressTownTextBox.Location = New System.Drawing.Point(129, 170)
        Me.AddressTownTextBox.Name = "AddressTownTextBox"
        Me.AddressTownTextBox.Size = New System.Drawing.Size(216, 28)
        Me.AddressTownTextBox.TabIndex = 27
        '
        'AddressCityTextBox
        '
        Me.AddressCityTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "AddressCity", True))
        Me.AddressCityTextBox.Location = New System.Drawing.Point(408, 170)
        Me.AddressCityTextBox.Name = "AddressCityTextBox"
        Me.AddressCityTextBox.Size = New System.Drawing.Size(216, 28)
        Me.AddressCityTextBox.TabIndex = 29
        '
        'ProvinceTextBox
        '
        Me.ProvinceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProvinceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "Province", True))
        Me.ProvinceTextBox.Location = New System.Drawing.Point(129, 204)
        Me.ProvinceTextBox.Name = "ProvinceTextBox"
        Me.ProvinceTextBox.Size = New System.Drawing.Size(216, 28)
        Me.ProvinceTextBox.TabIndex = 31
        '
        'PostalCodeTextBox
        '
        Me.PostalCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PostalCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "PostalCode", True))
        Me.PostalCodeTextBox.Location = New System.Drawing.Point(461, 204)
        Me.PostalCodeTextBox.Name = "PostalCodeTextBox"
        Me.PostalCodeTextBox.Size = New System.Drawing.Size(100, 28)
        Me.PostalCodeTextBox.TabIndex = 33
        '
        'BrthdayMaskedTextBox
        '
        Me.BrthdayMaskedTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BrthdayMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "Brthday", True))
        Me.BrthdayMaskedTextBox.Location = New System.Drawing.Point(129, 238)
        Me.BrthdayMaskedTextBox.Mask = "00/00/0000"
        Me.BrthdayMaskedTextBox.Name = "BrthdayMaskedTextBox"
        Me.BrthdayMaskedTextBox.Size = New System.Drawing.Size(100, 28)
        Me.BrthdayMaskedTextBox.TabIndex = 35
        Me.BrthdayMaskedTextBox.ValidatingType = GetType(Date)
        '
        'StartDateMaskedTextBox
        '
        Me.StartDateMaskedTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.StartDateMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "StartDate", True))
        Me.StartDateMaskedTextBox.Location = New System.Drawing.Point(129, 271)
        Me.StartDateMaskedTextBox.Mask = "00/00/0000"
        Me.StartDateMaskedTextBox.Name = "StartDateMaskedTextBox"
        Me.StartDateMaskedTextBox.Size = New System.Drawing.Size(100, 28)
        Me.StartDateMaskedTextBox.TabIndex = 37
        Me.StartDateMaskedTextBox.ValidatingType = GetType(Date)
        '
        'EndDateMaskedTextBox
        '
        Me.EndDateMaskedTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EndDateMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "EndDate", True))
        Me.EndDateMaskedTextBox.Location = New System.Drawing.Point(445, 272)
        Me.EndDateMaskedTextBox.Mask = "00/00/0000"
        Me.EndDateMaskedTextBox.Name = "EndDateMaskedTextBox"
        Me.EndDateMaskedTextBox.Size = New System.Drawing.Size(100, 28)
        Me.EndDateMaskedTextBox.TabIndex = 39
        Me.EndDateMaskedTextBox.ValidatingType = GetType(Date)
        '
        'UserNameTextBox
        '
        Me.UserNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UserNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "UserName", True))
        Me.UserNameTextBox.Location = New System.Drawing.Point(129, 305)
        Me.UserNameTextBox.Name = "UserNameTextBox"
        Me.UserNameTextBox.Size = New System.Drawing.Size(189, 28)
        Me.UserNameTextBox.TabIndex = 41
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "Password", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(129, 339)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(189, 28)
        Me.PasswordTextBox.TabIndex = 43
        '
        'CreateUserTextBox
        '
        Me.CreateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "CreateUser", True))
        Me.CreateUserTextBox.Location = New System.Drawing.Point(445, 308)
        Me.CreateUserTextBox.Name = "CreateUserTextBox"
        Me.CreateUserTextBox.Size = New System.Drawing.Size(100, 28)
        Me.CreateUserTextBox.TabIndex = 45
        '
        'CreateWhenTextBox
        '
        Me.CreateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "CreateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm"))
        Me.CreateWhenTextBox.Location = New System.Drawing.Point(551, 308)
        Me.CreateWhenTextBox.Name = "CreateWhenTextBox"
        Me.CreateWhenTextBox.Size = New System.Drawing.Size(195, 28)
        Me.CreateWhenTextBox.TabIndex = 47
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(445, 342)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(100, 28)
        Me.UpdateUserTextBox.TabIndex = 49
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmployeeMasterBindingSource, "UpdateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm"))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(551, 342)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(194, 28)
        Me.UpdateWhenTextBox.TabIndex = 51
        '
        'EmployeeMasterTableAdapter
        '
        Me.EmployeeMasterTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CustomerTableAdapter = Nothing
        Me.TableAdapterManager.EmployeeMasterTableAdapter = Me.EmployeeMasterTableAdapter
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'EmployeeMasterBindingNavigator
        '
        Me.EmployeeMasterBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.EmployeeMasterBindingNavigator.BindingSource = Me.EmployeeMasterBindingSource
        Me.EmployeeMasterBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.EmployeeMasterBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.EmployeeMasterBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.EmployeeMasterBindingNavigator.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmployeeMasterBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.EmployeeMasterBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.ToolStripButton1, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.EmployeeMasterBindingNavigatorSaveItem})
        Me.EmployeeMasterBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.EmployeeMasterBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.EmployeeMasterBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.EmployeeMasterBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.EmployeeMasterBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.EmployeeMasterBindingNavigator.Name = "EmployeeMasterBindingNavigator"
        Me.EmployeeMasterBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.EmployeeMasterBindingNavigator.Size = New System.Drawing.Size(1002, 27)
        Me.EmployeeMasterBindingNavigator.TabIndex = 1
        Me.EmployeeMasterBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(72, 24)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'EmployeeMasterBindingNavigatorSaveItem
        '
        Me.EmployeeMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.EmployeeMasterBindingNavigatorSaveItem.Image = CType(resources.GetObject("EmployeeMasterBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.EmployeeMasterBindingNavigatorSaveItem.Name = "EmployeeMasterBindingNavigatorSaveItem"
        Me.EmployeeMasterBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.EmployeeMasterBindingNavigatorSaveItem.Text = "Save Data"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(24, 24)
        Me.ToolStripButton1.Text = "ดึงข้อมูลใหม่"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.PictureBox1)
        Me.GroupBox2.Location = New System.Drawing.Point(758, 26)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(214, 246)
        Me.GroupBox2.TabIndex = 40
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "รูปประจำตัว"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(6, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(200, 200)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(133, 24)
        Me.ToolStripLabel1.Text = "ทะเบียนพนักงาน"
        '
        'EmployeeMasterPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.EmployeeMasterBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "EmployeeMasterPanel"
        Me.Size = New System.Drawing.Size(1002, 455)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.EmployeeMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployeeMasterBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EmployeeMasterBindingNavigator.ResumeLayout(False)
        Me.EmployeeMasterBindingNavigator.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents EmployeeMasterBindingSource As BindingSource
    Friend WithEvents EmployeeMasterTableAdapter As RealTimeDataSetTableAdapters.EmployeeMasterTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents EmployeeMasterBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents EmployeeMasterBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents EmployeeCodeTextBox As TextBox
    Friend WithEvents EmployeeNameTextBox As TextBox
    Friend WithEvents FullNameThaiTextBox As TextBox
    Friend WithEvents NickNameTextBox As TextBox
    Friend WithEvents TelephoneNumberTextBox As TextBox
    Friend WithEvents SMSNumberTextBox As TextBox
    Friend WithEvents EmailTextBox As TextBox
    Friend WithEvents IDCardTextBox As TextBox
    Friend WithEvents AddressNumberTextBox As TextBox
    Friend WithEvents AddressRoadTextBox As TextBox
    Friend WithEvents AddressTownTextBox As TextBox
    Friend WithEvents AddressCityTextBox As TextBox
    Friend WithEvents ProvinceTextBox As TextBox
    Friend WithEvents PostalCodeTextBox As TextBox
    Friend WithEvents BrthdayMaskedTextBox As MaskedTextBox
    Friend WithEvents StartDateMaskedTextBox As MaskedTextBox
    Friend WithEvents EndDateMaskedTextBox As MaskedTextBox
    Friend WithEvents UserNameTextBox As TextBox
    Friend WithEvents PasswordTextBox As TextBox
    Friend WithEvents CreateUserTextBox As TextBox
    Friend WithEvents CreateWhenTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
End Class
