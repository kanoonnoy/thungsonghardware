﻿Public Class EmployeeMasterPanel

    Private _RootEmployeePath = String.Empty
    Private _ImageProfileFullPath = String.Empty

    Public Sub Reload()
        Try

            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.EmployeeMasterTableAdapter.Fill(RealTimeDataSet.EmployeeMaster)
            Me.EmployeeMasterBindingSource.Sort = "EmployeeCode"
            Me.EmployeeMasterBindingSource.MoveLast()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try
    End Sub


    Private Sub Relink()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub UpdateAll()
        Try

            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.Validate()
            Me.EmployeeMasterBindingSource.EndEdit()

            Dim dtUpdate = Me.RealTimeDataSet.EmployeeMaster.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductID = row("EmployeeID").ToString()
                    Dim position = Me.EmployeeMasterBindingSource.Find("EmployeeID", ProductID)
                    If position >= 0 Then
                        Dim rowView = Me.EmployeeMasterBindingSource(position)
                        If row.RowState = DataRowState.Added Then
                            rowView("CreateWhen") = DateTime.Now
                            rowView("CreateUser") = GlobalV.CurrentUsername
                        End If
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If

                    End If

                Next
                Dim Update = Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub EmployeeMasterBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles EmployeeMasterBindingNavigatorSaveItem.Click
        UpdateAll()

    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Reload()

    End Sub

    Private Sub EmployeeMasterBindingSource_PositionChanged(sender As Object, e As EventArgs) Handles EmployeeMasterBindingSource.PositionChanged

    End Sub
End Class
