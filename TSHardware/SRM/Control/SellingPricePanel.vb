﻿Imports System.IO

Public Class SellingPricePanel

#Region "Variable"
    Private table As TableEntity = New TableEntity()
    Private _ProductCode = String.Empty
    Private _ProductName = String.Empty
    Private _ProductGroupCode = String.Empty
    Private _SupplierCode = String.Empty
    Private _IsStartup = True
    Private _dtSourcePrice As DataTable = New DataTable()
    Private _dtProductGroupMaster As DataTable = New DataTable()
    Private _dtSupllierMaster As DataTable = New DataTable()
    Private _PRODUCT_PATH As String = ""
#End Region

#Region "Reload"
    Private Sub CreateMenu()

        Try
            Me.TreeView1.Nodes.Clear()

            Dim str = "SELECT DISTINCT ProductGroupCode FROM ProductMaster ORDER BY ProductGroupCode"
            If table.SQLQuery(str) Then
                If table.Records.Rows.Count > 0 Then
                    For Each row As DataRow In table.Records.Rows
                        Dim groupName = ""
                        Dim group = row("ProductGroupCode").ToString()
                        Dim rowGroupName = Me._dtProductGroupMaster.Select(String.Format("ProductGroupCode='{0}'", group))
                        If Not rowGroupName.Length = 0 Then
                            groupName = String.Format("{0}", rowGroupName(0)("ProductGroupName"))
                        End If
                        If String.IsNullOrEmpty(groupName) Then
                            groupName = group
                        End If
                        Dim nodes As TreeNode = New TreeNode(groupName)
                        Dim productCodeAll As DataRow() = Me.RealTimeDataSet.ProductMaster.Select(String.Format("ProductGroupCode='{0}'", group))
                        If Not productCodeAll.Length = 0 Then
                            For Each rowAdd As DataRow In productCodeAll
                                Dim title = rowAdd("ProductCode").ToString()
                                Dim node As TreeNode = New TreeNode()
                                node.Text = title
                                node.Tag = title

                                nodes.Nodes.Add(node)
                            Next

                        End If

                        Me.TreeView1.Nodes.Add(nodes)
                    Next
                End If
            End If

            Me.TreeView1.ExpandAll()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
    Public Sub Reload()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            If _IsStartup Then
                _IsStartup = False
                _dtSourcePrice.Columns.Add("Display", GetType(String))
                _dtSourcePrice.Columns.Add("PR1", GetType(Double))
                _dtSourcePrice.Columns.Add("PR2", GetType(Double))
                _dtSourcePrice.Columns.Add("PR3", GetType(Double))
                _dtSourcePrice.Columns.Add("PR4", GetType(Double))

                _dtSourcePrice.Rows.Add("มาตรฐาน", 33, 20, 10, 5)
                _dtSourcePrice.Rows.Add("สำหรับขายส่ง", 20, 15, 5, 3)
                _dtSourcePrice.Rows.Add("สำหรับขายโครงการ", 10, 9, 4, 2.5)

                Me.PriceSourceComboBox1.ValueMember = "Display"
                Me.PriceSourceComboBox1.DisplayMember = "Display"
                Me.PriceSourceComboBox1.DataSource = _dtSourcePrice

                Dim strProductGroup = "SELECT * FROM ProductGroupMaster"
                If table.SQLQuery(strProductGroup) Then
                    _dtProductGroupMaster = table.Records
                End If

                Dim strSupllierMaster = "SELECT * FROM SupllierMaster"
                If table.SQLQuery(strSupllierMaster) Then
                    _dtSupllierMaster = table.Records
                End If
            End If
            Me.ProductMasterTableAdapter.Fill(RealTimeDataSet.ProductMaster)
            Me.ProductMasterBindingSource.MoveFirst()

            ' Create Menu
            ' Load SubCode

            CreateMenu()


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub SellingPricePanel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Reload()

    End Sub
    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()

    End Sub
#End Region

#Region "Relink"

    Private Sub Relink()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Dim rowview As DataRowView = Me.ProductMasterBindingSource.Current
            _ProductCode = rowview("ProductCode").ToString()
            _ProductName = rowview("ProductName").ToString()


            Me.Label2.Text = String.Format("{0:N0}/{1:N0}", Me.ProductMasterBindingSource.Position + 1, Me.ProductMasterBindingSource.Count)

            Me.StockCodeTextBox1.Text = _ProductCode
            Me.StockNameTextBox2.Text = _ProductName

            ' ProductImage
            Dim FileFullPath = String.Format("{0}\{1}.jpg", _PRODUCT_PATH, _ProductCode)

            If File.Exists(FileFullPath) Then
                Me.PictureBox1.ImageLocation = FileFullPath
            Else
                Me.PictureBox1.ImageLocation = Nothing
                Me.PictureBox1.Image = Nothing
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub ProductMasterBindingSource_PositionChanged(sender As Object, e As EventArgs) Handles ProductMasterBindingSource.PositionChanged
        Relink()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.ProductMasterBindingSource.MoveFirst()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.ProductMasterBindingSource.MovePrevious()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.ProductMasterBindingSource.MoveNext()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.ProductMasterBindingSource.MoveLast()
    End Sub

#End Region

#Region "Add New product"
    Private Sub btAddProduct_Click(sender As Object, e As EventArgs) Handles btAddProduct.Click
        Dim dlg As AddNewProductDialog = New AddNewProductDialog()

        If dlg.ShowDialog() = DialogResult.Yes Then
            AddNewProduct(dlg.ProductCode, dlg.SupplierCode, dlg.ProductGroupCode)
            Console.WriteLine("{0},{1},{2}", dlg.ProductCode, dlg.ProductGroupCode, dlg.SupplierCode)
        End If
    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(sender As Object, e As EventArgs)

    End Sub
    Private Function AddNewProduct(ByVal productCode As String, ByVal SupllierCode As String, ByVal ProductGroupCdoe As String)
        Try
            Dim dataTableView As DataView = ProductMasterBindingSource.List
            Dim rowView = dataTableView.AddNew()

            rowView("ProductCode") = productCode
            rowView("SupllierCode") = SupllierCode
            rowView("ProductGroupCode") = ProductGroupCdoe

            rowView("CreateUser") = GlobalV.CurrentUsername
            rowView("CreateWhen") = DateTime.Now

            rowView("UpdateUser") = GlobalV.CurrentUsername
            rowView("UpdateWhen") = DateTime.Now

            Me.ProductMasterBindingSource.MoveLast()
            Me.ProductMasterBindingSource.EndEdit()

            _ProductCode = ""
            _ProductGroupCode = ""
            _SupplierCode = ""
            Console.WriteLine("Add Success.")

            UpdateAll()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        Return Nothing
    End Function

#End Region

#Region "Update all"
    Private Sub UpdateAll()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.Validate()
            Me.ProductMasterBindingSource.EndEdit()

            Dim dtUpdate = Me.RealTimeDataSet.ProductMaster.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductID = row("ProductID").ToString()
                    Dim position = Me.ProductMasterBindingSource.Find("ProductID", ProductID)
                    If position >= 0 Then
                        Dim rowView = Me.ProductMasterBindingSource(position)
                        If row.RowState = DataRowState.Added Then
                            rowView("CreateWhen") = DateTime.Now
                            rowView("CreateUser") = GlobalV.CurrentUsername
                        End If
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If

                    End If

                Next
                Dim Update = Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
                Console.WriteLine("Update {0} rows", Update)
            End If


        Catch ex As Exception
            Console.WriteLine(ex.Message)

        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub ProductMasterBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        UpdateAll()
    End Sub

    Private Sub btUpdate_Click(sender As Object, e As EventArgs) Handles btUpdate.Click
        UpdateAll()
    End Sub
#End Region

#Region "Profit"

    Private Sub PriceSourceComboBox1_SelectedValueChanged(sender As Object, e As EventArgs) Handles PriceSourceComboBox1.SelectedValueChanged
        Try
            If Not PriceSourceComboBox1.SelectedValue Is Nothing Then
                Dim result = _dtSourcePrice.Select(String.Format("Display='{0}'", PriceSourceComboBox1.SelectedValue))
                If result.Length > 0 Then
                    Me.ProfitPR1TextBox.Text = String.Format("{0:N2}", result(0)("PR1"))
                    Me.ProfitPR2TextBox.Text = String.Format("{0:N2}", result(0)("PR2"))
                    Me.ProfitPR3TextBox.Text = String.Format("{0:N2}", result(0)("PR3"))
                    Me.ProfitPR4TextBox.Text = String.Format("{0:N2}", result(0)("PR4"))
                End If
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

    End Sub

    Private Sub ProfitPR1TextBox_TextChanged(sender As Object, e As EventArgs) Handles ProfitPR1TextBox.TextChanged
        Try
            Dim SellPrice As Double = 0
            If Not Me.ProfitPR1TextBox.TextLength = 0 Then
                Dim Cost = Convert.ToDouble(Me.CostTextBox.Text)
                Dim ProfitPercent = Convert.ToDouble(Me.ProfitPR1TextBox.Text)
                SellPrice = ((ProfitPercent / 100) * Cost) + Cost

            Else
                SellPrice = 0
            End If

            Me.SellPrice1TextBox.Text = String.Format("{0:N2}", SellPrice)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub ProfitPR2TextBox_TextChanged(sender As Object, e As EventArgs) Handles ProfitPR2TextBox.TextChanged
        Try
            Dim SellPrice As Double = 0
            If Not Me.ProfitPR2TextBox.TextLength = 0 Then
                Dim Cost = Convert.ToDouble(Me.CostTextBox.Text)
                Dim ProfitPercent = Convert.ToDouble(Me.ProfitPR2TextBox.Text)
                SellPrice = ((ProfitPercent / 100) * Cost) + Cost

            Else
                SellPrice = 0
            End If

            Me.SellPrice2TextBox.Text = String.Format("{0:N2}", SellPrice)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub ProfitPR3TextBox_TextChanged(sender As Object, e As EventArgs) Handles ProfitPR3TextBox.TextChanged
        Try
            Dim SellPrice As Double = 0
            If Not Me.ProfitPR3TextBox.TextLength = 0 Then
                Dim Cost = Convert.ToDouble(Me.CostTextBox.Text)
                Dim ProfitPercent = Convert.ToDouble(Me.ProfitPR3TextBox.Text)
                SellPrice = ((ProfitPercent / 100) * Cost) + Cost

            Else
                SellPrice = 0
            End If

            Me.SellPrice3TextBox.Text = String.Format("{0:N2}", SellPrice)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub ProfitPR4TextBox_TextChanged(sender As Object, e As EventArgs) Handles ProfitPR4TextBox.TextChanged
        Try
            Dim SellPrice As Double = 0
            If Not Me.ProfitPR4TextBox.TextLength = 0 Then
                Dim Cost = Convert.ToDouble(Me.CostTextBox.Text)
                Dim ProfitPercent = Convert.ToDouble(Me.ProfitPR4TextBox.Text)
                SellPrice = ((ProfitPercent / 100) * Cost) + Cost

            Else
                SellPrice = 0
            End If

            Me.SellPrice4TextBox.Text = String.Format("{0:N2}", SellPrice)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_NodeMouseDoubleClick(sender As Object, e As TreeNodeMouseClickEventArgs)


    End Sub

    Private Sub TreeView1_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            Dim result = e.Node.Tag
            If Not result = vbNullChar Then
                ' fing 
                Dim position = Me.ProductMasterBindingSource.Find("ProductCode", result)
                If Not position = -1 Then
                    Me.ProductMasterBindingSource.Position = position
                Else
                    Console.WriteLine("{0}={1} หาไม่พบ", result, position)
                End If

            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_NodeMouseHover(sender As Object, e As TreeNodeMouseHoverEventArgs) Handles TreeView1.NodeMouseHover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub TreeView1_MouseLeave(sender As Object, e As EventArgs) Handles TreeView1.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub
#End Region

End Class
