﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuantityPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim QuantityCodeLabel As System.Windows.Forms.Label
        Dim QuantityNameLabel As System.Windows.Forms.Label
        Dim DescriptionLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(QuantityPanel))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.QuantityMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.QuantityMasterTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.QuantityMasterTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.QuantityMasterBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.QuantityMasterBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.btReload = New System.Windows.Forms.ToolStripButton()
        Me.QuantityMasterDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityCodeTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityNameTextBox = New System.Windows.Forms.TextBox()
        Me.DescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        QuantityCodeLabel = New System.Windows.Forms.Label()
        QuantityNameLabel = New System.Windows.Forms.Label()
        DescriptionLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QuantityMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QuantityMasterBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.QuantityMasterBindingNavigator.SuspendLayout()
        CType(Me.QuantityMasterDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'QuantityCodeLabel
        '
        QuantityCodeLabel.AutoSize = True
        QuantityCodeLabel.Location = New System.Drawing.Point(6, 39)
        QuantityCodeLabel.Name = "QuantityCodeLabel"
        QuantityCodeLabel.Size = New System.Drawing.Size(82, 24)
        QuantityCodeLabel.TabIndex = 4
        QuantityCodeLabel.Text = "รหัสหน่วย"
        '
        'QuantityNameLabel
        '
        QuantityNameLabel.AutoSize = True
        QuantityNameLabel.Location = New System.Drawing.Point(308, 38)
        QuantityNameLabel.Name = "QuantityNameLabel"
        QuantityNameLabel.Size = New System.Drawing.Size(73, 24)
        QuantityNameLabel.TabIndex = 6
        QuantityNameLabel.Text = "ชื่อหน่วย"
        '
        'DescriptionLabel
        '
        DescriptionLabel.AutoSize = True
        DescriptionLabel.Location = New System.Drawing.Point(6, 72)
        DescriptionLabel.Name = "DescriptionLabel"
        DescriptionLabel.Size = New System.Drawing.Size(89, 24)
        DescriptionLabel.TabIndex = 8
        DescriptionLabel.Text = "รายละเอียด"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(308, 73)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(111, 24)
        UpdateUserLabel.TabIndex = 10
        UpdateUserLabel.Text = "แก้ไขล่าสุดเมื่อ"
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'QuantityMasterBindingSource
        '
        Me.QuantityMasterBindingSource.DataMember = "QuantityMaster"
        Me.QuantityMasterBindingSource.DataSource = Me.RealTimeDataSet
        '
        'QuantityMasterTableAdapter
        '
        Me.QuantityMasterTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Me.QuantityMasterTableAdapter
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'QuantityMasterBindingNavigator
        '
        Me.QuantityMasterBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.QuantityMasterBindingNavigator.BindingSource = Me.QuantityMasterBindingSource
        Me.QuantityMasterBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.QuantityMasterBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.QuantityMasterBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.QuantityMasterBindingNavigator.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuantityMasterBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.QuantityMasterBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btReload, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.QuantityMasterBindingNavigatorSaveItem})
        Me.QuantityMasterBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.QuantityMasterBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.QuantityMasterBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.QuantityMasterBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.QuantityMasterBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.QuantityMasterBindingNavigator.Name = "QuantityMasterBindingNavigator"
        Me.QuantityMasterBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.QuantityMasterBindingNavigator.Size = New System.Drawing.Size(776, 27)
        Me.QuantityMasterBindingNavigator.TabIndex = 0
        Me.QuantityMasterBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(90, 24)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(137, 24)
        Me.ToolStripLabel1.Text = "ทะเบียนหน่วยนับ"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'QuantityMasterBindingNavigatorSaveItem
        '
        Me.QuantityMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.QuantityMasterBindingNavigatorSaveItem.Image = CType(resources.GetObject("QuantityMasterBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.QuantityMasterBindingNavigatorSaveItem.Name = "QuantityMasterBindingNavigatorSaveItem"
        Me.QuantityMasterBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.QuantityMasterBindingNavigatorSaveItem.Text = "Save Data"
        '
        'btReload
        '
        Me.btReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(24, 24)
        Me.btReload.Text = "โหลดข้อมูลใหม่"
        '
        'QuantityMasterDataGridView
        '
        Me.QuantityMasterDataGridView.AllowUserToAddRows = False
        Me.QuantityMasterDataGridView.AllowUserToDeleteRows = False
        Me.QuantityMasterDataGridView.AllowUserToResizeColumns = False
        Me.QuantityMasterDataGridView.AutoGenerateColumns = False
        Me.QuantityMasterDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.QuantityMasterDataGridView.ColumnHeadersHeight = 30
        Me.QuantityMasterDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6})
        Me.QuantityMasterDataGridView.DataSource = Me.QuantityMasterBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.QuantityMasterDataGridView.DefaultCellStyle = DataGridViewCellStyle6
        Me.QuantityMasterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.QuantityMasterDataGridView.Location = New System.Drawing.Point(3, 24)
        Me.QuantityMasterDataGridView.Name = "QuantityMasterDataGridView"
        Me.QuantityMasterDataGridView.ReadOnly = True
        Me.QuantityMasterDataGridView.RowTemplate.Height = 24
        Me.QuantityMasterDataGridView.Size = New System.Drawing.Size(770, 386)
        Me.QuantityMasterDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "QuantityCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "รหัสหน่วยนับ"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 110
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "QuantityName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อหน่วยนับ"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 110
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn4.HeaderText = "รายละเอียด"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "UpdateUser"
        Me.DataGridViewTextBoxColumn5.HeaderText = "แก้ไขล่าสุดโดย"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "UpdateWhen"
        DataGridViewCellStyle5.Format = "dd/MM/yyyy HH:mm"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn6.HeaderText = "แก้ไขล่าสุดเมื่อ"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 120
        '
        'QuantityCodeTextBox
        '
        Me.QuantityCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.QuantityCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.QuantityMasterBindingSource, "QuantityCode", True))
        Me.QuantityCodeTextBox.Location = New System.Drawing.Point(94, 35)
        Me.QuantityCodeTextBox.Name = "QuantityCodeTextBox"
        Me.QuantityCodeTextBox.Size = New System.Drawing.Size(200, 28)
        Me.QuantityCodeTextBox.TabIndex = 0
        '
        'QuantityNameTextBox
        '
        Me.QuantityNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.QuantityNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.QuantityMasterBindingSource, "QuantityName", True))
        Me.QuantityNameTextBox.Location = New System.Drawing.Point(387, 35)
        Me.QuantityNameTextBox.Name = "QuantityNameTextBox"
        Me.QuantityNameTextBox.Size = New System.Drawing.Size(276, 28)
        Me.QuantityNameTextBox.TabIndex = 1
        '
        'DescriptionTextBox
        '
        Me.DescriptionTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.QuantityMasterBindingSource, "Description", True))
        Me.DescriptionTextBox.Location = New System.Drawing.Point(94, 69)
        Me.DescriptionTextBox.Name = "DescriptionTextBox"
        Me.DescriptionTextBox.Size = New System.Drawing.Size(200, 28)
        Me.DescriptionTextBox.TabIndex = 2
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.QuantityMasterBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(425, 70)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(69, 28)
        Me.UpdateUserTextBox.TabIndex = 11
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.QuantityMasterBindingSource, "UpdateWhen", True))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(500, 70)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(163, 28)
        Me.UpdateWhenTextBox.TabIndex = 13
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(QuantityCodeLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox1.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox1.Controls.Add(UpdateUserLabel)
        Me.GroupBox1.Controls.Add(Me.DescriptionTextBox)
        Me.GroupBox1.Controls.Add(Me.QuantityCodeTextBox)
        Me.GroupBox1.Controls.Add(DescriptionLabel)
        Me.GroupBox1.Controls.Add(QuantityNameLabel)
        Me.GroupBox1.Controls.Add(Me.QuantityNameTextBox)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(776, 125)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ข้อมูลหน่วยนับ"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.QuantityMasterDataGridView)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 152)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(776, 413)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "รายการหน่วยนับ"
        '
        'QuantityPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.QuantityMasterBindingNavigator)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "QuantityPanel"
        Me.Size = New System.Drawing.Size(776, 565)
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QuantityMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QuantityMasterBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.QuantityMasterBindingNavigator.ResumeLayout(False)
        Me.QuantityMasterBindingNavigator.PerformLayout()
        CType(Me.QuantityMasterDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents QuantityMasterBindingSource As BindingSource
    Friend WithEvents QuantityMasterTableAdapter As RealTimeDataSetTableAdapters.QuantityMasterTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents QuantityMasterBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents QuantityMasterBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents QuantityMasterDataGridView As DataGridView
    Friend WithEvents QuantityCodeTextBox As TextBox
    Friend WithEvents QuantityNameTextBox As TextBox
    Friend WithEvents DescriptionTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents btReload As ToolStripButton
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
End Class
