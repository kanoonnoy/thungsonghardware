﻿Public Class SupplierPanel
    Public Sub Reload()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.SupllierMasterTableAdapter.Fill(RealTimeDataSet.SupllierMaster)
            Me.SupllierMasterBindingSource.Sort = "SupllierCode"
            Me.SupllierMasterBindingSource.MoveLast()



        Catch ex As Exception
            Console.WriteLine(ex.Message)

        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Public Sub UpdateAll()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.Validate()
            Me.SupllierMasterBindingSource.EndEdit()
            Dim dtUpdate = Me.RealTimeDataSet.SupllierMaster.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductGroupID = row("SupllierID").ToString()
                    Dim position = Me.SupllierMasterBindingSource.Find("SupllierID", ProductGroupID)
                    If position >= 0 Then
                        Dim rowView = Me.SupllierMasterBindingSource(position)
                        If row.RowState = DataRowState.Added Then
                            rowView("CreateWhen") = DateTime.Now
                            rowView("CreateUser") = GlobalV.CurrentUsername
                        End If
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If

                    End If

                Next
                Dim Update = Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
                Console.WriteLine("Update {0} rows", Update)
            End If




        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Reload()
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try

    End Sub
    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()
    End Sub

    Private Sub ProductGroupMasterBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles ProductGroupMasterBindingNavigatorSaveItem.Click
        UpdateAll()

    End Sub
End Class
