﻿Public Class ProductGroupPanel
    Private Sub ProductGroupMasterBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles ProductGroupMasterBindingNavigatorSaveItem.Click
        UpdateAll()
    End Sub

    Public Sub Reload()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Me.ProductGroupMasterTableAdapter.Fill(RealTimeDataSet.ProductGroupMaster)
            Me.ProductGroupMasterBindingSource.MoveLast()


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub UpdateAll()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.AppStarting

            Me.Validate()
            Me.ProductGroupMasterBindingSource.EndEdit()
            Dim dtUpdate = Me.RealTimeDataSet.ProductGroupMaster.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductGroupID = row("ProductGroupID").ToString()
                    Dim position = Me.ProductGroupMasterBindingSource.Find("ProductGroupID", ProductGroupID)
                    If position >= 0 Then
                        Dim rowView = Me.ProductGroupMasterBindingSource(position)
                        If row.RowState = DataRowState.Added Then
                            rowView("CreateWhen") = DateTime.Now
                            rowView("CreateUser") = GlobalV.CurrentUsername
                        End If
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If

                    End If

                Next
                Dim Update = Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
                Console.WriteLine("Update {0} rows", Update)
            End If

        Catch ex As Exception

        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub
    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()
    End Sub
End Class
