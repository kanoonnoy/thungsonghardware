﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductGroupPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ProductGroupCodeLabel As System.Windows.Forms.Label
        Dim ProductGroupNameLabel As System.Windows.Forms.Label
        Dim DescriptionLabel As System.Windows.Forms.Label
        Dim CreateUserLabel As System.Windows.Forms.Label
        Dim CreateWhenLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim UpdateWhenLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductGroupPanel))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.ProductGroupMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductGroupMasterTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.ProductGroupMasterTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.ProductGroupMasterBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btReload = New System.Windows.Forms.ToolStripButton()
        Me.ProductGroupMasterBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ProductGroupMasterDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductGroupCodeTextBox = New System.Windows.Forms.TextBox()
        Me.ProductGroupNameTextBox = New System.Windows.Forms.TextBox()
        Me.DescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.CreateUserTextBox = New System.Windows.Forms.TextBox()
        Me.CreateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        ProductGroupCodeLabel = New System.Windows.Forms.Label()
        ProductGroupNameLabel = New System.Windows.Forms.Label()
        DescriptionLabel = New System.Windows.Forms.Label()
        CreateUserLabel = New System.Windows.Forms.Label()
        CreateWhenLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        UpdateWhenLabel = New System.Windows.Forms.Label()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductGroupMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductGroupMasterBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ProductGroupMasterBindingNavigator.SuspendLayout()
        CType(Me.ProductGroupMasterDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProductGroupCodeLabel
        '
        ProductGroupCodeLabel.AutoSize = True
        ProductGroupCodeLabel.Location = New System.Drawing.Point(6, 36)
        ProductGroupCodeLabel.Name = "ProductGroupCodeLabel"
        ProductGroupCodeLabel.Size = New System.Drawing.Size(122, 24)
        ProductGroupCodeLabel.TabIndex = 4
        ProductGroupCodeLabel.Text = "รหัสหมวดสินค้า"
        '
        'ProductGroupNameLabel
        '
        ProductGroupNameLabel.AutoSize = True
        ProductGroupNameLabel.Location = New System.Drawing.Point(241, 36)
        ProductGroupNameLabel.Name = "ProductGroupNameLabel"
        ProductGroupNameLabel.Size = New System.Drawing.Size(113, 24)
        ProductGroupNameLabel.TabIndex = 6
        ProductGroupNameLabel.Text = "ชื่อหมวดสินค้า"
        '
        'DescriptionLabel
        '
        DescriptionLabel.AutoSize = True
        DescriptionLabel.Location = New System.Drawing.Point(6, 70)
        DescriptionLabel.Name = "DescriptionLabel"
        DescriptionLabel.Size = New System.Drawing.Size(89, 24)
        DescriptionLabel.TabIndex = 8
        DescriptionLabel.Text = "รายละเอียด"
        '
        'CreateUserLabel
        '
        CreateUserLabel.AutoSize = True
        CreateUserLabel.Location = New System.Drawing.Point(58, 172)
        CreateUserLabel.Name = "CreateUserLabel"
        CreateUserLabel.Size = New System.Drawing.Size(70, 24)
        CreateUserLabel.TabIndex = 10
        CreateUserLabel.Text = "สร้างโดย"
        '
        'CreateWhenLabel
        '
        CreateWhenLabel.AutoSize = True
        CreateWhenLabel.Location = New System.Drawing.Point(58, 206)
        CreateWhenLabel.Name = "CreateWhenLabel"
        CreateWhenLabel.Size = New System.Drawing.Size(67, 24)
        CreateWhenLabel.TabIndex = 12
        CreateWhenLabel.Text = "สร้างเมื่อ"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(240, 172)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(114, 24)
        UpdateUserLabel.TabIndex = 14
        UpdateUserLabel.Text = "แก้ไขล่าสุดโดย"
        '
        'UpdateWhenLabel
        '
        UpdateWhenLabel.AutoSize = True
        UpdateWhenLabel.Location = New System.Drawing.Point(240, 206)
        UpdateWhenLabel.Name = "UpdateWhenLabel"
        UpdateWhenLabel.Size = New System.Drawing.Size(111, 24)
        UpdateWhenLabel.TabIndex = 16
        UpdateWhenLabel.Text = "แก้ไขล่าสุดเมื่อ"
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductGroupMasterBindingSource
        '
        Me.ProductGroupMasterBindingSource.DataMember = "ProductGroupMaster"
        Me.ProductGroupMasterBindingSource.DataSource = Me.RealTimeDataSet
        '
        'ProductGroupMasterTableAdapter
        '
        Me.ProductGroupMasterTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Me.ProductGroupMasterTableAdapter
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ProductGroupMasterBindingNavigator
        '
        Me.ProductGroupMasterBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.ProductGroupMasterBindingNavigator.BindingSource = Me.ProductGroupMasterBindingSource
        Me.ProductGroupMasterBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ProductGroupMasterBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.ProductGroupMasterBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ProductGroupMasterBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ProductGroupMasterBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btReload, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.ProductGroupMasterBindingNavigatorSaveItem})
        Me.ProductGroupMasterBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ProductGroupMasterBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ProductGroupMasterBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ProductGroupMasterBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ProductGroupMasterBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ProductGroupMasterBindingNavigator.Name = "ProductGroupMasterBindingNavigator"
        Me.ProductGroupMasterBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ProductGroupMasterBindingNavigator.Size = New System.Drawing.Size(702, 27)
        Me.ProductGroupMasterBindingNavigator.TabIndex = 0
        Me.ProductGroupMasterBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "เพิ่มใหม่"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(72, 24)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorDeleteItem.Text = "ลบ"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(93, 24)
        Me.ToolStripLabel1.Text = "หมวดสินค้า"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'btReload
        '
        Me.btReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(24, 24)
        Me.btReload.Text = "ToolStripButton1"
        Me.btReload.ToolTipText = "ดึงข้อมูลล่าสุด"
        '
        'ProductGroupMasterBindingNavigatorSaveItem
        '
        Me.ProductGroupMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ProductGroupMasterBindingNavigatorSaveItem.Image = CType(resources.GetObject("ProductGroupMasterBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ProductGroupMasterBindingNavigatorSaveItem.Name = "ProductGroupMasterBindingNavigatorSaveItem"
        Me.ProductGroupMasterBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.ProductGroupMasterBindingNavigatorSaveItem.Text = "บันทึก"
        '
        'ProductGroupMasterDataGridView
        '
        Me.ProductGroupMasterDataGridView.AllowUserToAddRows = False
        Me.ProductGroupMasterDataGridView.AllowUserToDeleteRows = False
        Me.ProductGroupMasterDataGridView.AutoGenerateColumns = False
        Me.ProductGroupMasterDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.ProductGroupMasterDataGridView.ColumnHeadersHeight = 30
        Me.ProductGroupMasterDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.ProductGroupMasterDataGridView.DataSource = Me.ProductGroupMasterBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ProductGroupMasterDataGridView.DefaultCellStyle = DataGridViewCellStyle6
        Me.ProductGroupMasterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ProductGroupMasterDataGridView.Location = New System.Drawing.Point(3, 24)
        Me.ProductGroupMasterDataGridView.Name = "ProductGroupMasterDataGridView"
        Me.ProductGroupMasterDataGridView.ReadOnly = True
        Me.ProductGroupMasterDataGridView.RowHeadersWidth = 21
        Me.ProductGroupMasterDataGridView.RowTemplate.Height = 24
        Me.ProductGroupMasterDataGridView.Size = New System.Drawing.Size(696, 273)
        Me.ProductGroupMasterDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "ProductGroupCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "รหัสหมวดสินค้า"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "ProductGroupName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อหมวดสินค้า"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Description"
        Me.DataGridViewTextBoxColumn4.HeaderText = "รายละเอียด"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "CreateUser"
        Me.DataGridViewTextBoxColumn5.HeaderText = "สร้างโดย"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "CreateWhen"
        DataGridViewCellStyle4.Format = "dd/MM/yyyy HH:mm"
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn6.HeaderText = "สร้างเมื่อ"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 120
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "UpdateUser"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ล่าสุดโดย"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "UpdateWhen"
        DataGridViewCellStyle5.Format = "dd/MM/yyyy HH:mm"
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn8.HeaderText = "ล่าสุดเมื่อ"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 120
        '
        'ProductGroupCodeTextBox
        '
        Me.ProductGroupCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProductGroupCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "ProductGroupCode", True))
        Me.ProductGroupCodeTextBox.Location = New System.Drawing.Point(134, 33)
        Me.ProductGroupCodeTextBox.MaxLength = 255
        Me.ProductGroupCodeTextBox.Name = "ProductGroupCodeTextBox"
        Me.ProductGroupCodeTextBox.Size = New System.Drawing.Size(100, 28)
        Me.ProductGroupCodeTextBox.TabIndex = 5
        '
        'ProductGroupNameTextBox
        '
        Me.ProductGroupNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProductGroupNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "ProductGroupName", True))
        Me.ProductGroupNameTextBox.Location = New System.Drawing.Point(360, 33)
        Me.ProductGroupNameTextBox.MaxLength = 255
        Me.ProductGroupNameTextBox.Name = "ProductGroupNameTextBox"
        Me.ProductGroupNameTextBox.Size = New System.Drawing.Size(302, 28)
        Me.ProductGroupNameTextBox.TabIndex = 7
        '
        'DescriptionTextBox
        '
        Me.DescriptionTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "Description", True))
        Me.DescriptionTextBox.Location = New System.Drawing.Point(134, 67)
        Me.DescriptionTextBox.MaxLength = 255
        Me.DescriptionTextBox.Multiline = True
        Me.DescriptionTextBox.Name = "DescriptionTextBox"
        Me.DescriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.DescriptionTextBox.Size = New System.Drawing.Size(528, 96)
        Me.DescriptionTextBox.TabIndex = 9
        '
        'CreateUserTextBox
        '
        Me.CreateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "CreateUser", True))
        Me.CreateUserTextBox.Location = New System.Drawing.Point(134, 169)
        Me.CreateUserTextBox.Name = "CreateUserTextBox"
        Me.CreateUserTextBox.ReadOnly = True
        Me.CreateUserTextBox.Size = New System.Drawing.Size(100, 28)
        Me.CreateUserTextBox.TabIndex = 11
        '
        'CreateWhenTextBox
        '
        Me.CreateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "CreateWhen", True))
        Me.CreateWhenTextBox.Location = New System.Drawing.Point(134, 203)
        Me.CreateWhenTextBox.Name = "CreateWhenTextBox"
        Me.CreateWhenTextBox.ReadOnly = True
        Me.CreateWhenTextBox.Size = New System.Drawing.Size(100, 28)
        Me.CreateWhenTextBox.TabIndex = 13
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "UpdateUser", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm:ss"))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(368, 169)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(200, 28)
        Me.UpdateUserTextBox.TabIndex = 15
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductGroupMasterBindingSource, "UpdateWhen", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "dd/MM/yyyy HH:mm:ss"))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(368, 203)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(200, 28)
        Me.UpdateWhenTextBox.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(ProductGroupCodeLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox1.Controls.Add(Me.ProductGroupCodeTextBox)
        Me.GroupBox1.Controls.Add(UpdateWhenLabel)
        Me.GroupBox1.Controls.Add(ProductGroupNameLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox1.Controls.Add(Me.ProductGroupNameTextBox)
        Me.GroupBox1.Controls.Add(UpdateUserLabel)
        Me.GroupBox1.Controls.Add(DescriptionLabel)
        Me.GroupBox1.Controls.Add(Me.CreateWhenTextBox)
        Me.GroupBox1.Controls.Add(Me.DescriptionTextBox)
        Me.GroupBox1.Controls.Add(CreateWhenLabel)
        Me.GroupBox1.Controls.Add(CreateUserLabel)
        Me.GroupBox1.Controls.Add(Me.CreateUserTextBox)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(702, 240)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ข้อมูลหมวดสินค้า"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ProductGroupMasterDataGridView)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 267)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(702, 300)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "หมวดสินค้าทั้งหมด"
        '
        'ProductGroupPanel
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProductGroupMasterBindingNavigator)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "ProductGroupPanel"
        Me.Size = New System.Drawing.Size(702, 567)
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductGroupMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductGroupMasterBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ProductGroupMasterBindingNavigator.ResumeLayout(False)
        Me.ProductGroupMasterBindingNavigator.PerformLayout()
        CType(Me.ProductGroupMasterDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents ProductGroupMasterBindingSource As BindingSource
    Friend WithEvents ProductGroupMasterTableAdapter As RealTimeDataSetTableAdapters.ProductGroupMasterTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ProductGroupMasterBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents ProductGroupMasterBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents ProductGroupMasterDataGridView As DataGridView
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ProductGroupCodeTextBox As TextBox
    Friend WithEvents ProductGroupNameTextBox As TextBox
    Friend WithEvents DescriptionTextBox As TextBox
    Friend WithEvents CreateUserTextBox As TextBox
    Friend WithEvents CreateWhenTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btReload As ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
End Class
