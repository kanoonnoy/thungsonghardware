﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SupplierPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SupllierCodeLabel As System.Windows.Forms.Label
        Dim SupllierNameLabel As System.Windows.Forms.Label
        Dim TelephoneNumberLabel As System.Windows.Forms.Label
        Dim SMSNumberLabel As System.Windows.Forms.Label
        Dim FaxNumberLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim AddressNumberLabel As System.Windows.Forms.Label
        Dim AddressRoadLabel As System.Windows.Forms.Label
        Dim AddressTownLabel As System.Windows.Forms.Label
        Dim AddressCityLabel As System.Windows.Forms.Label
        Dim ProvinceLabel As System.Windows.Forms.Label
        Dim PostalCodeLabel As System.Windows.Forms.Label
        Dim CreateUserLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SupplierPanel))
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.SupllierMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SupllierMasterTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.SupllierMasterTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.SupllierMasterDataGridView = New System.Windows.Forms.DataGridView()
        Me.SupllierCodeTextBox = New System.Windows.Forms.TextBox()
        Me.SupllierNameTextBox = New System.Windows.Forms.TextBox()
        Me.TelephoneNumberTextBox = New System.Windows.Forms.TextBox()
        Me.SMSNumberTextBox = New System.Windows.Forms.TextBox()
        Me.FaxNumberTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.AddressNumberTextBox = New System.Windows.Forms.TextBox()
        Me.AddressRoadTextBox = New System.Windows.Forms.TextBox()
        Me.AddressTownTextBox = New System.Windows.Forms.TextBox()
        Me.AddressCityTextBox = New System.Windows.Forms.TextBox()
        Me.ProvinceTextBox = New System.Windows.Forms.TextBox()
        Me.PostalCodeTextBox = New System.Windows.Forms.TextBox()
        Me.CreateUserTextBox = New System.Windows.Forms.TextBox()
        Me.CreateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ProductGroupMasterBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.btReload = New System.Windows.Forms.ToolStripButton()
        Me.ProductGroupMasterBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        SupllierCodeLabel = New System.Windows.Forms.Label()
        SupllierNameLabel = New System.Windows.Forms.Label()
        TelephoneNumberLabel = New System.Windows.Forms.Label()
        SMSNumberLabel = New System.Windows.Forms.Label()
        FaxNumberLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        AddressNumberLabel = New System.Windows.Forms.Label()
        AddressRoadLabel = New System.Windows.Forms.Label()
        AddressTownLabel = New System.Windows.Forms.Label()
        AddressCityLabel = New System.Windows.Forms.Label()
        ProvinceLabel = New System.Windows.Forms.Label()
        PostalCodeLabel = New System.Windows.Forms.Label()
        CreateUserLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SupllierMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SupllierMasterDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ProductGroupMasterBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ProductGroupMasterBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'SupllierCodeLabel
        '
        SupllierCodeLabel.AutoSize = True
        SupllierCodeLabel.Location = New System.Drawing.Point(6, 30)
        SupllierCodeLabel.Name = "SupllierCodeLabel"
        SupllierCodeLabel.Size = New System.Drawing.Size(110, 24)
        SupllierCodeLabel.TabIndex = 4
        SupllierCodeLabel.Text = "รหัสผู้จำหน่าย"
        '
        'SupllierNameLabel
        '
        SupllierNameLabel.AutoSize = True
        SupllierNameLabel.Location = New System.Drawing.Point(301, 30)
        SupllierNameLabel.Name = "SupllierNameLabel"
        SupllierNameLabel.Size = New System.Drawing.Size(101, 24)
        SupllierNameLabel.TabIndex = 6
        SupllierNameLabel.Text = "ชื่อผู้จำหน่าย"
        '
        'TelephoneNumberLabel
        '
        TelephoneNumberLabel.AutoSize = True
        TelephoneNumberLabel.Location = New System.Drawing.Point(31, 132)
        TelephoneNumberLabel.Name = "TelephoneNumberLabel"
        TelephoneNumberLabel.Size = New System.Drawing.Size(85, 24)
        TelephoneNumberLabel.TabIndex = 8
        TelephoneNumberLabel.Text = "เบอรติดต่อ"
        '
        'SMSNumberLabel
        '
        SMSNumberLabel.AutoSize = True
        SMSNumberLabel.Location = New System.Drawing.Point(255, 132)
        SMSNumberLabel.Name = "SMSNumberLabel"
        SMSNumberLabel.Size = New System.Drawing.Size(82, 24)
        SMSNumberLabel.TabIndex = 10
        SMSNumberLabel.Text = "เบอรมือถือ"
        '
        'FaxNumberLabel
        '
        FaxNumberLabel.AutoSize = True
        FaxNumberLabel.Location = New System.Drawing.Point(54, 167)
        FaxNumberLabel.Name = "FaxNumberLabel"
        FaxNumberLabel.Size = New System.Drawing.Size(62, 24)
        FaxNumberLabel.TabIndex = 12
        FaxNumberLabel.Text = "โทรสาร"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Location = New System.Drawing.Point(63, 199)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(53, 24)
        EmailLabel.TabIndex = 14
        EmailLabel.Text = "อีเมลล์"
        '
        'AddressNumberLabel
        '
        AddressNumberLabel.AutoSize = True
        AddressNumberLabel.Location = New System.Drawing.Point(75, 64)
        AddressNumberLabel.Name = "AddressNumberLabel"
        AddressNumberLabel.Size = New System.Drawing.Size(41, 24)
        AddressNumberLabel.TabIndex = 20
        AddressNumberLabel.Text = "ที่อยู่"
        '
        'AddressRoadLabel
        '
        AddressRoadLabel.AutoSize = True
        AddressRoadLabel.Location = New System.Drawing.Point(173, 64)
        AddressRoadLabel.Name = "AddressRoadLabel"
        AddressRoadLabel.Size = New System.Drawing.Size(46, 24)
        AddressRoadLabel.TabIndex = 22
        AddressRoadLabel.Text = "ถนน"
        '
        'AddressTownLabel
        '
        AddressTownLabel.AutoSize = True
        AddressTownLabel.Location = New System.Drawing.Point(394, 64)
        AddressTownLabel.Name = "AddressTownLabel"
        AddressTownLabel.Size = New System.Drawing.Size(49, 24)
        AddressTownLabel.TabIndex = 24
        AddressTownLabel.Text = "ตำบล"
        '
        'AddressCityLabel
        '
        AddressCityLabel.AutoSize = True
        AddressCityLabel.Location = New System.Drawing.Point(555, 64)
        AddressCityLabel.Name = "AddressCityLabel"
        AddressCityLabel.Size = New System.Drawing.Size(52, 24)
        AddressCityLabel.TabIndex = 26
        AddressCityLabel.Text = "อำเภอ"
        '
        'ProvinceLabel
        '
        ProvinceLabel.AutoSize = True
        ProvinceLabel.Location = New System.Drawing.Point(59, 98)
        ProvinceLabel.Name = "ProvinceLabel"
        ProvinceLabel.Size = New System.Drawing.Size(57, 24)
        ProvinceLabel.TabIndex = 28
        ProvinceLabel.Text = "จังหวัด"
        '
        'PostalCodeLabel
        '
        PostalCodeLabel.AutoSize = True
        PostalCodeLabel.Location = New System.Drawing.Point(394, 98)
        PostalCodeLabel.Name = "PostalCodeLabel"
        PostalCodeLabel.Size = New System.Drawing.Size(104, 24)
        PostalCodeLabel.TabIndex = 30
        PostalCodeLabel.Text = "รหัสไปรษณีย์"
        '
        'CreateUserLabel
        '
        CreateUserLabel.AutoSize = True
        CreateUserLabel.Location = New System.Drawing.Point(438, 166)
        CreateUserLabel.Name = "CreateUserLabel"
        CreateUserLabel.Size = New System.Drawing.Size(70, 24)
        CreateUserLabel.TabIndex = 32
        CreateUserLabel.Text = "สร้างโดย"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(394, 205)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(114, 24)
        UpdateUserLabel.TabIndex = 36
        UpdateUserLabel.Text = "แก้ไขล่าสุดโดย"
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SupllierMasterBindingSource
        '
        Me.SupllierMasterBindingSource.DataMember = "SupllierMaster"
        Me.SupllierMasterBindingSource.DataSource = Me.RealTimeDataSet
        '
        'SupllierMasterTableAdapter
        '
        Me.SupllierMasterTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Nothing
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Me.SupllierMasterTableAdapter
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SupllierMasterDataGridView
        '
        Me.SupllierMasterDataGridView.AllowUserToAddRows = False
        Me.SupllierMasterDataGridView.AllowUserToDeleteRows = False
        Me.SupllierMasterDataGridView.AutoGenerateColumns = False
        Me.SupllierMasterDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.SupllierMasterDataGridView.ColumnHeadersHeight = 30
        Me.SupllierMasterDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19})
        Me.SupllierMasterDataGridView.DataSource = Me.SupllierMasterBindingSource
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SupllierMasterDataGridView.DefaultCellStyle = DataGridViewCellStyle12
        Me.SupllierMasterDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SupllierMasterDataGridView.Location = New System.Drawing.Point(3, 24)
        Me.SupllierMasterDataGridView.Name = "SupllierMasterDataGridView"
        Me.SupllierMasterDataGridView.ReadOnly = True
        Me.SupllierMasterDataGridView.RowHeadersWidth = 20
        Me.SupllierMasterDataGridView.RowTemplate.Height = 24
        Me.SupllierMasterDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SupllierMasterDataGridView.Size = New System.Drawing.Size(846, 641)
        Me.SupllierMasterDataGridView.TabIndex = 1
        '
        'SupllierCodeTextBox
        '
        Me.SupllierCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SupllierCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "SupllierCode", True))
        Me.SupllierCodeTextBox.Location = New System.Drawing.Point(122, 27)
        Me.SupllierCodeTextBox.MaxLength = 60
        Me.SupllierCodeTextBox.Name = "SupllierCodeTextBox"
        Me.SupllierCodeTextBox.Size = New System.Drawing.Size(173, 28)
        Me.SupllierCodeTextBox.TabIndex = 5
        '
        'SupllierNameTextBox
        '
        Me.SupllierNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SupllierNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "SupllierName", True))
        Me.SupllierNameTextBox.Location = New System.Drawing.Point(408, 27)
        Me.SupllierNameTextBox.MaxLength = 100
        Me.SupllierNameTextBox.Name = "SupllierNameTextBox"
        Me.SupllierNameTextBox.Size = New System.Drawing.Size(357, 28)
        Me.SupllierNameTextBox.TabIndex = 7
        '
        'TelephoneNumberTextBox
        '
        Me.TelephoneNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TelephoneNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "TelephoneNumber", True))
        Me.TelephoneNumberTextBox.Location = New System.Drawing.Point(122, 129)
        Me.TelephoneNumberTextBox.MaxLength = 10
        Me.TelephoneNumberTextBox.Name = "TelephoneNumberTextBox"
        Me.TelephoneNumberTextBox.Size = New System.Drawing.Size(127, 28)
        Me.TelephoneNumberTextBox.TabIndex = 9
        '
        'SMSNumberTextBox
        '
        Me.SMSNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SMSNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "SMSNumber", True))
        Me.SMSNumberTextBox.Location = New System.Drawing.Point(343, 129)
        Me.SMSNumberTextBox.MaxLength = 10
        Me.SMSNumberTextBox.Name = "SMSNumberTextBox"
        Me.SMSNumberTextBox.Size = New System.Drawing.Size(127, 28)
        Me.SMSNumberTextBox.TabIndex = 11
        '
        'FaxNumberTextBox
        '
        Me.FaxNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FaxNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "FaxNumber", True))
        Me.FaxNumberTextBox.Location = New System.Drawing.Point(122, 163)
        Me.FaxNumberTextBox.MaxLength = 10
        Me.FaxNumberTextBox.Name = "FaxNumberTextBox"
        Me.FaxNumberTextBox.Size = New System.Drawing.Size(127, 28)
        Me.FaxNumberTextBox.TabIndex = 13
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "Email", True))
        Me.EmailTextBox.Location = New System.Drawing.Point(122, 196)
        Me.EmailTextBox.MaxLength = 255
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(264, 28)
        Me.EmailTextBox.TabIndex = 15
        '
        'AddressNumberTextBox
        '
        Me.AddressNumberTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressNumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "AddressNumber", True))
        Me.AddressNumberTextBox.Location = New System.Drawing.Point(122, 61)
        Me.AddressNumberTextBox.MaxLength = 255
        Me.AddressNumberTextBox.Name = "AddressNumberTextBox"
        Me.AddressNumberTextBox.Size = New System.Drawing.Size(45, 28)
        Me.AddressNumberTextBox.TabIndex = 21
        '
        'AddressRoadTextBox
        '
        Me.AddressRoadTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressRoadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "AddressRoad", True))
        Me.AddressRoadTextBox.Location = New System.Drawing.Point(225, 61)
        Me.AddressRoadTextBox.MaxLength = 255
        Me.AddressRoadTextBox.Name = "AddressRoadTextBox"
        Me.AddressRoadTextBox.Size = New System.Drawing.Size(161, 28)
        Me.AddressRoadTextBox.TabIndex = 23
        '
        'AddressTownTextBox
        '
        Me.AddressTownTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressTownTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "AddressTown", True))
        Me.AddressTownTextBox.Location = New System.Drawing.Point(449, 61)
        Me.AddressTownTextBox.MaxLength = 255
        Me.AddressTownTextBox.Name = "AddressTownTextBox"
        Me.AddressTownTextBox.Size = New System.Drawing.Size(100, 28)
        Me.AddressTownTextBox.TabIndex = 25
        '
        'AddressCityTextBox
        '
        Me.AddressCityTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddressCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "AddressCity", True))
        Me.AddressCityTextBox.Location = New System.Drawing.Point(613, 61)
        Me.AddressCityTextBox.MaxLength = 255
        Me.AddressCityTextBox.Name = "AddressCityTextBox"
        Me.AddressCityTextBox.Size = New System.Drawing.Size(152, 28)
        Me.AddressCityTextBox.TabIndex = 27
        '
        'ProvinceTextBox
        '
        Me.ProvinceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProvinceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "Province", True))
        Me.ProvinceTextBox.Location = New System.Drawing.Point(122, 95)
        Me.ProvinceTextBox.Name = "ProvinceTextBox"
        Me.ProvinceTextBox.Size = New System.Drawing.Size(264, 28)
        Me.ProvinceTextBox.TabIndex = 29
        '
        'PostalCodeTextBox
        '
        Me.PostalCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PostalCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "PostalCode", True))
        Me.PostalCodeTextBox.Location = New System.Drawing.Point(499, 95)
        Me.PostalCodeTextBox.MaxLength = 5
        Me.PostalCodeTextBox.Name = "PostalCodeTextBox"
        Me.PostalCodeTextBox.Size = New System.Drawing.Size(100, 28)
        Me.PostalCodeTextBox.TabIndex = 31
        '
        'CreateUserTextBox
        '
        Me.CreateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "CreateUser", True))
        Me.CreateUserTextBox.Location = New System.Drawing.Point(514, 163)
        Me.CreateUserTextBox.Name = "CreateUserTextBox"
        Me.CreateUserTextBox.ReadOnly = True
        Me.CreateUserTextBox.Size = New System.Drawing.Size(100, 28)
        Me.CreateUserTextBox.TabIndex = 33
        '
        'CreateWhenTextBox
        '
        Me.CreateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "CreateWhen", True))
        Me.CreateWhenTextBox.Location = New System.Drawing.Point(620, 163)
        Me.CreateWhenTextBox.Name = "CreateWhenTextBox"
        Me.CreateWhenTextBox.ReadOnly = True
        Me.CreateWhenTextBox.Size = New System.Drawing.Size(145, 28)
        Me.CreateWhenTextBox.TabIndex = 35
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(514, 196)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(100, 28)
        Me.UpdateUserTextBox.TabIndex = 37
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SupllierMasterBindingSource, "UpdateWhen", True))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(620, 196)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(145, 28)
        Me.UpdateWhenTextBox.TabIndex = 39
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(SupllierCodeLabel)
        Me.GroupBox1.Controls.Add(CreateUserLabel)
        Me.GroupBox1.Controls.Add(AddressNumberLabel)
        Me.GroupBox1.Controls.Add(Me.CreateUserTextBox)
        Me.GroupBox1.Controls.Add(TelephoneNumberLabel)
        Me.GroupBox1.Controls.Add(Me.CreateWhenTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressNumberTextBox)
        Me.GroupBox1.Controls.Add(UpdateUserLabel)
        Me.GroupBox1.Controls.Add(AddressRoadLabel)
        Me.GroupBox1.Controls.Add(Me.UpdateUserTextBox)
        Me.GroupBox1.Controls.Add(Me.SupllierNameTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressRoadTextBox)
        Me.GroupBox1.Controls.Add(Me.UpdateWhenTextBox)
        Me.GroupBox1.Controls.Add(Me.TelephoneNumberTextBox)
        Me.GroupBox1.Controls.Add(AddressTownLabel)
        Me.GroupBox1.Controls.Add(SMSNumberLabel)
        Me.GroupBox1.Controls.Add(Me.AddressTownTextBox)
        Me.GroupBox1.Controls.Add(Me.SupllierCodeTextBox)
        Me.GroupBox1.Controls.Add(AddressCityLabel)
        Me.GroupBox1.Controls.Add(Me.SMSNumberTextBox)
        Me.GroupBox1.Controls.Add(Me.AddressCityTextBox)
        Me.GroupBox1.Controls.Add(SupllierNameLabel)
        Me.GroupBox1.Controls.Add(ProvinceLabel)
        Me.GroupBox1.Controls.Add(FaxNumberLabel)
        Me.GroupBox1.Controls.Add(Me.ProvinceTextBox)
        Me.GroupBox1.Controls.Add(Me.FaxNumberTextBox)
        Me.GroupBox1.Controls.Add(PostalCodeLabel)
        Me.GroupBox1.Controls.Add(EmailLabel)
        Me.GroupBox1.Controls.Add(Me.PostalCodeTextBox)
        Me.GroupBox1.Controls.Add(Me.EmailTextBox)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(852, 244)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ข้อมูลผู้จำหน่าย"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SupllierMasterDataGridView)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 271)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(852, 668)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "รายการผู้จำหน่าย"
        '
        'ProductGroupMasterBindingNavigator
        '
        Me.ProductGroupMasterBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.ProductGroupMasterBindingNavigator.BindingSource = Me.SupllierMasterBindingSource
        Me.ProductGroupMasterBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ProductGroupMasterBindingNavigator.CountItemFormat = "ทั้งหมด {0}"
        Me.ProductGroupMasterBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ProductGroupMasterBindingNavigator.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ProductGroupMasterBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.btReload, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.ProductGroupMasterBindingNavigatorSaveItem})
        Me.ProductGroupMasterBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ProductGroupMasterBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ProductGroupMasterBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ProductGroupMasterBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ProductGroupMasterBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ProductGroupMasterBindingNavigator.Name = "ProductGroupMasterBindingNavigator"
        Me.ProductGroupMasterBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ProductGroupMasterBindingNavigator.Size = New System.Drawing.Size(852, 27)
        Me.ProductGroupMasterBindingNavigator.TabIndex = 2
        Me.ProductGroupMasterBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(72, 24)
        Me.BindingNavigatorCountItem.Text = "ทั้งหมด {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(81, 24)
        Me.ToolStripLabel1.Text = "ผู้จำหน่าย"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 27)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 27)
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 27)
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "SupllierCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "รหัสผู้จำหน่าย"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "SupllierName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อผู้จำหน่าย"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "TelephoneNumber"
        Me.DataGridViewTextBoxColumn4.HeaderText = "เบอร์โทรติดต่อ"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Email"
        Me.DataGridViewTextBoxColumn7.HeaderText = "อีเมลล์"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 120
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "CreateUser"
        Me.DataGridViewTextBoxColumn16.HeaderText = "สร้างโดย"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 120
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "CreateWhen"
        DataGridViewCellStyle10.Format = "dd/MM/yyyy HH:mm"
        Me.DataGridViewTextBoxColumn17.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn17.HeaderText = "สร้างเมื่อ"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 120
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "UpdateUser"
        Me.DataGridViewTextBoxColumn18.HeaderText = "แก้ไขล่าสุดโดย"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 120
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "UpdateWhen"
        DataGridViewCellStyle11.Format = "dd/MM/yyyy HH:mm"
        Me.DataGridViewTextBoxColumn19.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewTextBoxColumn19.HeaderText = "แก้ไขล่าสุดเมื่อ"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Width = 120
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorAddNewItem.Text = "เพิ่มใหม่"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorDeleteItem.Text = "ลบ"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(24, 24)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'btReload
        '
        Me.btReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(24, 24)
        Me.btReload.Text = "ToolStripButton1"
        Me.btReload.ToolTipText = "ดึงข้อมูลล่าสุด"
        '
        'ProductGroupMasterBindingNavigatorSaveItem
        '
        Me.ProductGroupMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ProductGroupMasterBindingNavigatorSaveItem.Image = CType(resources.GetObject("ProductGroupMasterBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ProductGroupMasterBindingNavigatorSaveItem.Name = "ProductGroupMasterBindingNavigatorSaveItem"
        Me.ProductGroupMasterBindingNavigatorSaveItem.Size = New System.Drawing.Size(24, 24)
        Me.ProductGroupMasterBindingNavigatorSaveItem.Text = "บันทึก"
        '
        'SupplierPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProductGroupMasterBindingNavigator)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "SupplierPanel"
        Me.Size = New System.Drawing.Size(852, 939)
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SupllierMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SupllierMasterDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ProductGroupMasterBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ProductGroupMasterBindingNavigator.ResumeLayout(False)
        Me.ProductGroupMasterBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents SupllierMasterBindingSource As BindingSource
    Friend WithEvents SupllierMasterTableAdapter As RealTimeDataSetTableAdapters.SupllierMasterTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SupllierMasterDataGridView As DataGridView
    Friend WithEvents SupllierCodeTextBox As TextBox
    Friend WithEvents SupllierNameTextBox As TextBox
    Friend WithEvents TelephoneNumberTextBox As TextBox
    Friend WithEvents SMSNumberTextBox As TextBox
    Friend WithEvents FaxNumberTextBox As TextBox
    Friend WithEvents EmailTextBox As TextBox
    Friend WithEvents AddressNumberTextBox As TextBox
    Friend WithEvents AddressRoadTextBox As TextBox
    Friend WithEvents AddressTownTextBox As TextBox
    Friend WithEvents AddressCityTextBox As TextBox
    Friend WithEvents ProvinceTextBox As TextBox
    Friend WithEvents PostalCodeTextBox As TextBox
    Friend WithEvents CreateUserTextBox As TextBox
    Friend WithEvents CreateWhenTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ProductGroupMasterBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents btReload As ToolStripButton
    Friend WithEvents ProductGroupMasterBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
End Class
