﻿Public Class QuantityPanel

    Public Sub Reload()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.QuantityMasterTableAdapter.Fill(Me.RealTimeDataSet.QuantityMaster)
            Me.QuantityMasterBindingSource.MoveLast()


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try

    End Sub
    Private Sub UpdateAll()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor

            Me.Validate()
            Me.QuantityMasterBindingSource.EndEdit()

            Dim dtUpdate = Me.RealTimeDataSet.QuantityMaster.GetChanges()
            If (Not IsNothing(dtUpdate)) Then
                For Each row As DataRow In dtUpdate.Rows

                    Dim ProductGroupID = row("QuantityID").ToString()
                    Dim position = Me.QuantityMasterBindingSource.Find("QuantityID", ProductGroupID)
                    If position >= 0 Then
                        Dim rowView = Me.QuantityMasterBindingSource(position)
                        If row.RowState <> DataRowState.Deleted Then
                            rowView("UpdateWhen") = DateTime.Now
                            rowView("UpdateUser") = GlobalV.CurrentUsername
                        End If
                    End If
                Next
                Me.TableAdapterManager.UpdateAll(Me.RealTimeDataSet)
            End If


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub QuantityMasterBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles QuantityMasterBindingNavigatorSaveItem.Click
        UpdateAll()
    End Sub

    Private Sub btReload_Click(sender As Object, e As EventArgs) Handles btReload.Click
        Reload()

    End Sub
End Class
