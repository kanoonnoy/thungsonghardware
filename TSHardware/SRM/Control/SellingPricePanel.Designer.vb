﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SellingPricePanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ProductNameLabel As System.Windows.Forms.Label
        Dim CostLabel As System.Windows.Forms.Label
        Dim SellPrice1Label As System.Windows.Forms.Label
        Dim SellPrice2Label As System.Windows.Forms.Label
        Dim SellPrice3Label As System.Windows.Forms.Label
        Dim SellPrice4Label As System.Windows.Forms.Label
        Dim RetailPriceLabel As System.Windows.Forms.Label
        Dim QtyLabel As System.Windows.Forms.Label
        Dim QuantityCodeLabel As System.Windows.Forms.Label
        Dim Properties01Label As System.Windows.Forms.Label
        Dim WarrantyLabel As System.Windows.Forms.Label
        Dim RemarkLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim CreateUserLabel As System.Windows.Forms.Label
        Dim UpdateUserLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.StockCodeTextBox1 = New System.Windows.Forms.TextBox()
        Me.StockNameTextBox2 = New System.Windows.Forms.TextBox()
        Me.RealTimeDataSet = New TSHardware.RealTimeDataSet()
        Me.ProductMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductMasterTableAdapter = New TSHardware.RealTimeDataSetTableAdapters.ProductMasterTableAdapter()
        Me.TableAdapterManager = New TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager()
        Me.ProductNameTextBox = New System.Windows.Forms.TextBox()
        Me.CostTextBox = New System.Windows.Forms.TextBox()
        Me.SellPrice1TextBox = New System.Windows.Forms.TextBox()
        Me.SellPrice2TextBox = New System.Windows.Forms.TextBox()
        Me.SellPrice3TextBox = New System.Windows.Forms.TextBox()
        Me.SellPrice4TextBox = New System.Windows.Forms.TextBox()
        Me.RetailPriceTextBox = New System.Windows.Forms.TextBox()
        Me.QtyTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityCodeTextBox = New System.Windows.Forms.TextBox()
        Me.Properties01TextBox = New System.Windows.Forms.TextBox()
        Me.Properties02TextBox = New System.Windows.Forms.TextBox()
        Me.Properties03TextBox = New System.Windows.Forms.TextBox()
        Me.Properties04TextBox = New System.Windows.Forms.TextBox()
        Me.Properties05TextBox = New System.Windows.Forms.TextBox()
        Me.Properties06TextBox = New System.Windows.Forms.TextBox()
        Me.Properties07TextBox = New System.Windows.Forms.TextBox()
        Me.Properties08TextBox = New System.Windows.Forms.TextBox()
        Me.Properties09TextBox = New System.Windows.Forms.TextBox()
        Me.Properties10TextBox = New System.Windows.Forms.TextBox()
        Me.WarrantyTextBox = New System.Windows.Forms.TextBox()
        Me.RemarkTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.CreateUserTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateUserTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btAddProduct = New System.Windows.Forms.Button()
        Me.ProfitPR1TextBox = New System.Windows.Forms.TextBox()
        Me.ProfitPR2TextBox = New System.Windows.Forms.TextBox()
        Me.ProfitPR3TextBox = New System.Windows.Forms.TextBox()
        Me.ProfitPR4TextBox = New System.Windows.Forms.TextBox()
        Me.CreateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.UpdateWhenTextBox = New System.Windows.Forms.TextBox()
        Me.btUpdate = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.btReload = New System.Windows.Forms.Button()
        Me.btSearch = New System.Windows.Forms.Button()
        Me.PriceSourceComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        ProductNameLabel = New System.Windows.Forms.Label()
        CostLabel = New System.Windows.Forms.Label()
        SellPrice1Label = New System.Windows.Forms.Label()
        SellPrice2Label = New System.Windows.Forms.Label()
        SellPrice3Label = New System.Windows.Forms.Label()
        SellPrice4Label = New System.Windows.Forms.Label()
        RetailPriceLabel = New System.Windows.Forms.Label()
        QtyLabel = New System.Windows.Forms.Label()
        QuantityCodeLabel = New System.Windows.Forms.Label()
        Properties01Label = New System.Windows.Forms.Label()
        WarrantyLabel = New System.Windows.Forms.Label()
        RemarkLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        CreateUserLabel = New System.Windows.Forms.Label()
        UpdateUserLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProductNameLabel
        '
        ProductNameLabel.AutoSize = True
        ProductNameLabel.Location = New System.Drawing.Point(166, 51)
        ProductNameLabel.Name = "ProductNameLabel"
        ProductNameLabel.Size = New System.Drawing.Size(71, 24)
        ProductNameLabel.TabIndex = 14
        ProductNameLabel.Text = "ชื่อสินค้า"
        '
        'CostLabel
        '
        CostLabel.AutoSize = True
        CostLabel.Location = New System.Drawing.Point(167, 247)
        CostLabel.Name = "CostLabel"
        CostLabel.Size = New System.Drawing.Size(65, 24)
        CostLabel.TabIndex = 20
        CostLabel.Text = "ทุนฐาน"
        '
        'SellPrice1Label
        '
        SellPrice1Label.AutoSize = True
        SellPrice1Label.Location = New System.Drawing.Point(167, 133)
        SellPrice1Label.Name = "SellPrice1Label"
        SellPrice1Label.Size = New System.Drawing.Size(72, 24)
        SellPrice1Label.TabIndex = 22
        SellPrice1Label.Text = "ราคา P1"
        '
        'SellPrice2Label
        '
        SellPrice2Label.AutoSize = True
        SellPrice2Label.Location = New System.Drawing.Point(167, 160)
        SellPrice2Label.Name = "SellPrice2Label"
        SellPrice2Label.Size = New System.Drawing.Size(72, 24)
        SellPrice2Label.TabIndex = 24
        SellPrice2Label.Text = "ราคา P2"
        '
        'SellPrice3Label
        '
        SellPrice3Label.AutoSize = True
        SellPrice3Label.Location = New System.Drawing.Point(167, 188)
        SellPrice3Label.Name = "SellPrice3Label"
        SellPrice3Label.Size = New System.Drawing.Size(72, 24)
        SellPrice3Label.TabIndex = 26
        SellPrice3Label.Text = "ราคา P3"
        '
        'SellPrice4Label
        '
        SellPrice4Label.AutoSize = True
        SellPrice4Label.Location = New System.Drawing.Point(167, 214)
        SellPrice4Label.Name = "SellPrice4Label"
        SellPrice4Label.Size = New System.Drawing.Size(72, 24)
        SellPrice4Label.TabIndex = 28
        SellPrice4Label.Text = "ราคา P4"
        '
        'RetailPriceLabel
        '
        RetailPriceLabel.AutoSize = True
        RetailPriceLabel.Location = New System.Drawing.Point(167, 100)
        RetailPriceLabel.Name = "RetailPriceLabel"
        RetailPriceLabel.Size = New System.Drawing.Size(75, 24)
        RetailPriceLabel.TabIndex = 30
        RetailPriceLabel.Text = "ราคาปลีก"
        '
        'QtyLabel
        '
        QtyLabel.AutoSize = True
        QtyLabel.Location = New System.Drawing.Point(225, 318)
        QtyLabel.Name = "QtyLabel"
        QtyLabel.Size = New System.Drawing.Size(63, 24)
        QtyLabel.TabIndex = 32
        QtyLabel.Text = "คงเหลือ"
        '
        'QuantityCodeLabel
        '
        QuantityCodeLabel.AutoSize = True
        QuantityCodeLabel.Location = New System.Drawing.Point(369, 244)
        QuantityCodeLabel.Name = "QuantityCodeLabel"
        QuantityCodeLabel.Size = New System.Drawing.Size(30, 24)
        QuantityCodeLabel.TabIndex = 34
        QuantityCodeLabel.Text = "ต่อ"
        '
        'Properties01Label
        '
        Properties01Label.AutoSize = True
        Properties01Label.Location = New System.Drawing.Point(500, 51)
        Properties01Label.Name = "Properties01Label"
        Properties01Label.Size = New System.Drawing.Size(89, 24)
        Properties01Label.TabIndex = 36
        Properties01Label.Text = "รายละเอียด"
        '
        'WarrantyLabel
        '
        WarrantyLabel.AutoSize = True
        WarrantyLabel.Location = New System.Drawing.Point(166, 380)
        WarrantyLabel.Name = "WarrantyLabel"
        WarrantyLabel.Size = New System.Drawing.Size(106, 24)
        WarrantyLabel.TabIndex = 56
        WarrantyLabel.Text = "การรับประกัน"
        '
        'RemarkLabel
        '
        RemarkLabel.AutoSize = True
        RemarkLabel.Location = New System.Drawing.Point(166, 413)
        RemarkLabel.Name = "RemarkLabel"
        RemarkLabel.Size = New System.Drawing.Size(79, 24)
        RemarkLabel.TabIndex = 58
        RemarkLabel.Text = "หมายเหตุ"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(531, 322)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(59, 24)
        StatusLabel.TabIndex = 60
        StatusLabel.Text = "สถานะ"
        '
        'CreateUserLabel
        '
        CreateUserLabel.AutoSize = True
        CreateUserLabel.Location = New System.Drawing.Point(551, 380)
        CreateUserLabel.Name = "CreateUserLabel"
        CreateUserLabel.Size = New System.Drawing.Size(70, 24)
        CreateUserLabel.TabIndex = 62
        CreateUserLabel.Text = "สร้างโดย"
        '
        'UpdateUserLabel
        '
        UpdateUserLabel.AutoSize = True
        UpdateUserLabel.Location = New System.Drawing.Point(551, 413)
        UpdateUserLabel.Name = "UpdateUserLabel"
        UpdateUserLabel.Size = New System.Drawing.Size(86, 24)
        UpdateUserLabel.TabIndex = 66
        UpdateUserLabel.Text = "แก้ไขล่าสุด"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Label1.Location = New System.Drawing.Point(472, 130)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(92, 91)
        Label1.TabIndex = 70
        Label1.Text = "V"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(322, 73)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(45, 24)
        Label4.TabIndex = 78
        Label4.Text = "ราคา"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(381, 96)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(60, 24)
        Label5.TabIndex = 79
        Label5.Text = "กำไร%"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(229, 277)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(140, 24)
        Label3.TabIndex = 86
        Label3.Text = "รูปแบบการตั้งราคา"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(412, 318)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(30, 24)
        Label6.TabIndex = 88
        Label6.Text = "ต่อ"
        '
        'TreeView1
        '
        Me.TreeView1.HideSelection = False
        Me.TreeView1.Location = New System.Drawing.Point(6, 104)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(157, 350)
        Me.TreeView1.TabIndex = 0
        '
        'StockCodeTextBox1
        '
        Me.StockCodeTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.StockCodeTextBox1.Location = New System.Drawing.Point(6, 26)
        Me.StockCodeTextBox1.Name = "StockCodeTextBox1"
        Me.StockCodeTextBox1.Size = New System.Drawing.Size(157, 28)
        Me.StockCodeTextBox1.TabIndex = 2
        '
        'StockNameTextBox2
        '
        Me.StockNameTextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.StockNameTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.StockNameTextBox2.Location = New System.Drawing.Point(6, 54)
        Me.StockNameTextBox2.Name = "StockNameTextBox2"
        Me.StockNameTextBox2.Size = New System.Drawing.Size(157, 22)
        Me.StockNameTextBox2.TabIndex = 3
        '
        'RealTimeDataSet
        '
        Me.RealTimeDataSet.DataSetName = "RealTimeDataSet"
        Me.RealTimeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductMasterBindingSource
        '
        Me.ProductMasterBindingSource.DataMember = "ProductMaster"
        Me.ProductMasterBindingSource.DataSource = Me.RealTimeDataSet
        '
        'ProductMasterTableAdapter
        '
        Me.ProductMasterTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ProductGroupMasterTableAdapter = Nothing
        Me.TableAdapterManager.ProductMasterTableAdapter = Me.ProductMasterTableAdapter
        Me.TableAdapterManager.QuantityMasterTableAdapter = Nothing
        Me.TableAdapterManager.SupllierMasterTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = TSHardware.RealTimeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ProductNameTextBox
        '
        Me.ProductNameTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ProductNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "ProductName", True))
        Me.ProductNameTextBox.Location = New System.Drawing.Point(237, 46)
        Me.ProductNameTextBox.Name = "ProductNameTextBox"
        Me.ProductNameTextBox.Size = New System.Drawing.Size(257, 28)
        Me.ProductNameTextBox.TabIndex = 0
        '
        'CostTextBox
        '
        Me.CostTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.CostTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Cost", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.CostTextBox.Location = New System.Drawing.Point(249, 240)
        Me.CostTextBox.Name = "CostTextBox"
        Me.CostTextBox.Size = New System.Drawing.Size(116, 28)
        Me.CostTextBox.TabIndex = 6
        Me.CostTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SellPrice1TextBox
        '
        Me.SellPrice1TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.SellPrice1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "SellPrice1", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.SellPrice1TextBox.Location = New System.Drawing.Point(249, 126)
        Me.SellPrice1TextBox.Name = "SellPrice1TextBox"
        Me.SellPrice1TextBox.Size = New System.Drawing.Size(116, 28)
        Me.SellPrice1TextBox.TabIndex = 2
        Me.SellPrice1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SellPrice2TextBox
        '
        Me.SellPrice2TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.SellPrice2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "SellPrice2", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.SellPrice2TextBox.Location = New System.Drawing.Point(249, 153)
        Me.SellPrice2TextBox.Name = "SellPrice2TextBox"
        Me.SellPrice2TextBox.Size = New System.Drawing.Size(116, 28)
        Me.SellPrice2TextBox.TabIndex = 3
        Me.SellPrice2TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SellPrice3TextBox
        '
        Me.SellPrice3TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.SellPrice3TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "SellPrice3", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.SellPrice3TextBox.Location = New System.Drawing.Point(249, 180)
        Me.SellPrice3TextBox.Name = "SellPrice3TextBox"
        Me.SellPrice3TextBox.Size = New System.Drawing.Size(116, 28)
        Me.SellPrice3TextBox.TabIndex = 4
        Me.SellPrice3TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SellPrice4TextBox
        '
        Me.SellPrice4TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.SellPrice4TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "SellPrice4", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.SellPrice4TextBox.Location = New System.Drawing.Point(249, 207)
        Me.SellPrice4TextBox.Name = "SellPrice4TextBox"
        Me.SellPrice4TextBox.Size = New System.Drawing.Size(116, 28)
        Me.SellPrice4TextBox.TabIndex = 5
        Me.SellPrice4TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RetailPriceTextBox
        '
        Me.RetailPriceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.RetailPriceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "RetailPrice", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N2"))
        Me.RetailPriceTextBox.Location = New System.Drawing.Point(249, 93)
        Me.RetailPriceTextBox.Name = "RetailPriceTextBox"
        Me.RetailPriceTextBox.Size = New System.Drawing.Size(116, 28)
        Me.RetailPriceTextBox.TabIndex = 1
        Me.RetailPriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'QtyTextBox
        '
        Me.QtyTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.QtyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Qty", True))
        Me.QtyTextBox.Location = New System.Drawing.Point(292, 315)
        Me.QtyTextBox.Name = "QtyTextBox"
        Me.QtyTextBox.Size = New System.Drawing.Size(116, 28)
        Me.QtyTextBox.TabIndex = 8
        '
        'QuantityCodeTextBox
        '
        Me.QuantityCodeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.QuantityCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "QuantityCode", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.QuantityCodeTextBox.Location = New System.Drawing.Point(403, 241)
        Me.QuantityCodeTextBox.Name = "QuantityCodeTextBox"
        Me.QuantityCodeTextBox.Size = New System.Drawing.Size(72, 28)
        Me.QuantityCodeTextBox.TabIndex = 7
        Me.QuantityCodeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Properties01TextBox
        '
        Me.Properties01TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties01TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties01", True))
        Me.Properties01TextBox.Location = New System.Drawing.Point(591, 43)
        Me.Properties01TextBox.MaxLength = 100
        Me.Properties01TextBox.Name = "Properties01TextBox"
        Me.Properties01TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties01TextBox.TabIndex = 11
        '
        'Properties02TextBox
        '
        Me.Properties02TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties02TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties02", True))
        Me.Properties02TextBox.Location = New System.Drawing.Point(591, 70)
        Me.Properties02TextBox.MaxLength = 100
        Me.Properties02TextBox.Name = "Properties02TextBox"
        Me.Properties02TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties02TextBox.TabIndex = 12
        '
        'Properties03TextBox
        '
        Me.Properties03TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties03TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties03", True))
        Me.Properties03TextBox.Location = New System.Drawing.Point(591, 97)
        Me.Properties03TextBox.MaxLength = 100
        Me.Properties03TextBox.Name = "Properties03TextBox"
        Me.Properties03TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties03TextBox.TabIndex = 13
        '
        'Properties04TextBox
        '
        Me.Properties04TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties04TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties04", True))
        Me.Properties04TextBox.Location = New System.Drawing.Point(591, 124)
        Me.Properties04TextBox.MaxLength = 100
        Me.Properties04TextBox.Name = "Properties04TextBox"
        Me.Properties04TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties04TextBox.TabIndex = 14
        '
        'Properties05TextBox
        '
        Me.Properties05TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties05TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties05", True))
        Me.Properties05TextBox.Location = New System.Drawing.Point(591, 151)
        Me.Properties05TextBox.MaxLength = 100
        Me.Properties05TextBox.Name = "Properties05TextBox"
        Me.Properties05TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties05TextBox.TabIndex = 15
        '
        'Properties06TextBox
        '
        Me.Properties06TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties06TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties06", True))
        Me.Properties06TextBox.Location = New System.Drawing.Point(591, 178)
        Me.Properties06TextBox.MaxLength = 100
        Me.Properties06TextBox.Name = "Properties06TextBox"
        Me.Properties06TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties06TextBox.TabIndex = 16
        '
        'Properties07TextBox
        '
        Me.Properties07TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties07TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties07", True))
        Me.Properties07TextBox.Location = New System.Drawing.Point(591, 205)
        Me.Properties07TextBox.MaxLength = 100
        Me.Properties07TextBox.Name = "Properties07TextBox"
        Me.Properties07TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties07TextBox.TabIndex = 17
        '
        'Properties08TextBox
        '
        Me.Properties08TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties08TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties08", True))
        Me.Properties08TextBox.Location = New System.Drawing.Point(591, 232)
        Me.Properties08TextBox.MaxLength = 100
        Me.Properties08TextBox.Name = "Properties08TextBox"
        Me.Properties08TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties08TextBox.TabIndex = 18
        '
        'Properties09TextBox
        '
        Me.Properties09TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties09TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties09", True))
        Me.Properties09TextBox.Location = New System.Drawing.Point(591, 259)
        Me.Properties09TextBox.MaxLength = 100
        Me.Properties09TextBox.Name = "Properties09TextBox"
        Me.Properties09TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties09TextBox.TabIndex = 19
        '
        'Properties10TextBox
        '
        Me.Properties10TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Properties10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Properties10", True))
        Me.Properties10TextBox.Location = New System.Drawing.Point(591, 286)
        Me.Properties10TextBox.MaxLength = 100
        Me.Properties10TextBox.Name = "Properties10TextBox"
        Me.Properties10TextBox.Size = New System.Drawing.Size(387, 28)
        Me.Properties10TextBox.TabIndex = 20
        '
        'WarrantyTextBox
        '
        Me.WarrantyTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.WarrantyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Warranty", True))
        Me.WarrantyTextBox.Location = New System.Drawing.Point(271, 377)
        Me.WarrantyTextBox.Name = "WarrantyTextBox"
        Me.WarrantyTextBox.Size = New System.Drawing.Size(228, 28)
        Me.WarrantyTextBox.TabIndex = 9
        '
        'RemarkTextBox
        '
        Me.RemarkTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.RemarkTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Remark", True))
        Me.RemarkTextBox.Location = New System.Drawing.Point(271, 410)
        Me.RemarkTextBox.Name = "RemarkTextBox"
        Me.RemarkTextBox.Size = New System.Drawing.Size(228, 28)
        Me.RemarkTextBox.TabIndex = 10
        '
        'StatusTextBox
        '
        Me.StatusTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(591, 319)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(62, 28)
        Me.StatusTextBox.TabIndex = 21
        '
        'CreateUserTextBox
        '
        Me.CreateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "CreateUser", True))
        Me.CreateUserTextBox.Location = New System.Drawing.Point(641, 377)
        Me.CreateUserTextBox.Name = "CreateUserTextBox"
        Me.CreateUserTextBox.ReadOnly = True
        Me.CreateUserTextBox.Size = New System.Drawing.Size(104, 28)
        Me.CreateUserTextBox.TabIndex = 63
        '
        'UpdateUserTextBox
        '
        Me.UpdateUserTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateUserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "UpdateUser", True))
        Me.UpdateUserTextBox.Location = New System.Drawing.Point(641, 410)
        Me.UpdateUserTextBox.Name = "UpdateUserTextBox"
        Me.UpdateUserTextBox.ReadOnly = True
        Me.UpdateUserTextBox.Size = New System.Drawing.Size(104, 28)
        Me.UpdateUserTextBox.TabIndex = 67
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 18)
        Me.Label2.TabIndex = 71
        Me.Label2.Tag = "{0:N0}/{1:N0}"
        Me.Label2.Text = "0/1"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btAddProduct
        '
        Me.btAddProduct.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0006
        Me.btAddProduct.Location = New System.Drawing.Point(223, 3)
        Me.btAddProduct.Name = "btAddProduct"
        Me.btAddProduct.Size = New System.Drawing.Size(30, 30)
        Me.btAddProduct.TabIndex = 72
        Me.btAddProduct.UseVisualStyleBackColor = True
        '
        'ProfitPR1TextBox
        '
        Me.ProfitPR1TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ProfitPR1TextBox.Location = New System.Drawing.Point(371, 127)
        Me.ProfitPR1TextBox.Name = "ProfitPR1TextBox"
        Me.ProfitPR1TextBox.Size = New System.Drawing.Size(68, 28)
        Me.ProfitPR1TextBox.TabIndex = 74
        '
        'ProfitPR2TextBox
        '
        Me.ProfitPR2TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ProfitPR2TextBox.Location = New System.Drawing.Point(371, 154)
        Me.ProfitPR2TextBox.Name = "ProfitPR2TextBox"
        Me.ProfitPR2TextBox.Size = New System.Drawing.Size(68, 28)
        Me.ProfitPR2TextBox.TabIndex = 75
        '
        'ProfitPR3TextBox
        '
        Me.ProfitPR3TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ProfitPR3TextBox.Location = New System.Drawing.Point(371, 181)
        Me.ProfitPR3TextBox.Name = "ProfitPR3TextBox"
        Me.ProfitPR3TextBox.Size = New System.Drawing.Size(68, 28)
        Me.ProfitPR3TextBox.TabIndex = 76
        '
        'ProfitPR4TextBox
        '
        Me.ProfitPR4TextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ProfitPR4TextBox.Location = New System.Drawing.Point(371, 208)
        Me.ProfitPR4TextBox.Name = "ProfitPR4TextBox"
        Me.ProfitPR4TextBox.Size = New System.Drawing.Size(68, 28)
        Me.ProfitPR4TextBox.TabIndex = 77
        '
        'CreateWhenTextBox
        '
        Me.CreateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "CreateWhen", True))
        Me.CreateWhenTextBox.Location = New System.Drawing.Point(751, 377)
        Me.CreateWhenTextBox.Name = "CreateWhenTextBox"
        Me.CreateWhenTextBox.ReadOnly = True
        Me.CreateWhenTextBox.Size = New System.Drawing.Size(162, 28)
        Me.CreateWhenTextBox.TabIndex = 80
        '
        'UpdateWhenTextBox
        '
        Me.UpdateWhenTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.UpdateWhenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "UpdateWhen", True))
        Me.UpdateWhenTextBox.Location = New System.Drawing.Point(752, 410)
        Me.UpdateWhenTextBox.Name = "UpdateWhenTextBox"
        Me.UpdateWhenTextBox.ReadOnly = True
        Me.UpdateWhenTextBox.Size = New System.Drawing.Size(162, 28)
        Me.UpdateWhenTextBox.TabIndex = 81
        '
        'btUpdate
        '
        Me.btUpdate.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0008
        Me.btUpdate.Location = New System.Drawing.Point(249, 3)
        Me.btUpdate.Name = "btUpdate"
        Me.btUpdate.Size = New System.Drawing.Size(30, 30)
        Me.btUpdate.TabIndex = 82
        Me.btUpdate.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.StockCodeTextBox1)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.StockNameTextBox2)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(165, 598)
        Me.GroupBox1.TabIndex = 84
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ค้นหาสินค้า"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(6, 460)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(157, 133)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Button2
        '
        Me.Button2.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0002
        Me.Button2.Location = New System.Drawing.Point(6, 78)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(24, 24)
        Me.Button2.TabIndex = 5
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0003
        Me.Button3.Location = New System.Drawing.Point(30, 78)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(24, 24)
        Me.Button3.TabIndex = 6
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0004
        Me.Button4.Location = New System.Drawing.Point(114, 78)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(24, 24)
        Me.Button4.TabIndex = 7
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0005
        Me.Button5.Location = New System.Drawing.Point(136, 78)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(24, 24)
        Me.Button5.TabIndex = 8
        Me.Button5.UseVisualStyleBackColor = True
        '
        'btReload
        '
        Me.btReload.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0001
        Me.btReload.Location = New System.Drawing.Point(197, 3)
        Me.btReload.Name = "btReload"
        Me.btReload.Size = New System.Drawing.Size(30, 30)
        Me.btReload.TabIndex = 83
        Me.btReload.UseVisualStyleBackColor = True
        '
        'btSearch
        '
        Me.btSearch.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0007
        Me.btSearch.Location = New System.Drawing.Point(171, 3)
        Me.btSearch.Name = "btSearch"
        Me.btSearch.Size = New System.Drawing.Size(30, 30)
        Me.btSearch.TabIndex = 4
        Me.btSearch.UseVisualStyleBackColor = True
        '
        'PriceSourceComboBox1
        '
        Me.PriceSourceComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.PriceSourceComboBox1.FormattingEnabled = True
        Me.PriceSourceComboBox1.Location = New System.Drawing.Point(371, 274)
        Me.PriceSourceComboBox1.Name = "PriceSourceComboBox1"
        Me.PriceSourceComboBox1.Size = New System.Drawing.Size(166, 30)
        Me.PriceSourceComboBox1.TabIndex = 85
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProductMasterBindingSource, "QuantityCode", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.TextBox5.Location = New System.Drawing.Point(446, 315)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(72, 28)
        Me.TextBox5.TabIndex = 87
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'SellingPricePanel
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Label6)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.PriceSourceComboBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btReload)
        Me.Controls.Add(Me.btUpdate)
        Me.Controls.Add(Me.UpdateWhenTextBox)
        Me.Controls.Add(Me.CreateWhenTextBox)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.ProfitPR4TextBox)
        Me.Controls.Add(Me.ProfitPR3TextBox)
        Me.Controls.Add(Me.ProfitPR2TextBox)
        Me.Controls.Add(Me.ProfitPR1TextBox)
        Me.Controls.Add(Me.btAddProduct)
        Me.Controls.Add(Label1)
        Me.Controls.Add(ProductNameLabel)
        Me.Controls.Add(Me.ProductNameTextBox)
        Me.Controls.Add(CostLabel)
        Me.Controls.Add(Me.CostTextBox)
        Me.Controls.Add(SellPrice1Label)
        Me.Controls.Add(Me.SellPrice1TextBox)
        Me.Controls.Add(SellPrice2Label)
        Me.Controls.Add(Me.SellPrice2TextBox)
        Me.Controls.Add(SellPrice3Label)
        Me.Controls.Add(Me.SellPrice3TextBox)
        Me.Controls.Add(SellPrice4Label)
        Me.Controls.Add(Me.SellPrice4TextBox)
        Me.Controls.Add(RetailPriceLabel)
        Me.Controls.Add(Me.RetailPriceTextBox)
        Me.Controls.Add(QtyLabel)
        Me.Controls.Add(Me.QtyTextBox)
        Me.Controls.Add(QuantityCodeLabel)
        Me.Controls.Add(Me.QuantityCodeTextBox)
        Me.Controls.Add(Properties01Label)
        Me.Controls.Add(Me.Properties01TextBox)
        Me.Controls.Add(Me.Properties02TextBox)
        Me.Controls.Add(Me.Properties03TextBox)
        Me.Controls.Add(Me.Properties04TextBox)
        Me.Controls.Add(Me.Properties05TextBox)
        Me.Controls.Add(Me.Properties06TextBox)
        Me.Controls.Add(Me.Properties07TextBox)
        Me.Controls.Add(Me.Properties08TextBox)
        Me.Controls.Add(Me.Properties09TextBox)
        Me.Controls.Add(Me.Properties10TextBox)
        Me.Controls.Add(WarrantyLabel)
        Me.Controls.Add(Me.WarrantyTextBox)
        Me.Controls.Add(RemarkLabel)
        Me.Controls.Add(Me.RemarkTextBox)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(CreateUserLabel)
        Me.Controls.Add(Me.CreateUserTextBox)
        Me.Controls.Add(UpdateUserLabel)
        Me.Controls.Add(Me.UpdateUserTextBox)
        Me.Controls.Add(Me.btSearch)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "SellingPricePanel"
        Me.Size = New System.Drawing.Size(987, 598)
        CType(Me.RealTimeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents StockCodeTextBox1 As TextBox
    Friend WithEvents StockNameTextBox2 As TextBox
    Friend WithEvents btSearch As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents RealTimeDataSet As RealTimeDataSet
    Friend WithEvents ProductMasterBindingSource As BindingSource
    Friend WithEvents ProductMasterTableAdapter As RealTimeDataSetTableAdapters.ProductMasterTableAdapter
    Friend WithEvents TableAdapterManager As RealTimeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ProductNameTextBox As TextBox
    Friend WithEvents CostTextBox As TextBox
    Friend WithEvents SellPrice1TextBox As TextBox
    Friend WithEvents SellPrice2TextBox As TextBox
    Friend WithEvents SellPrice3TextBox As TextBox
    Friend WithEvents SellPrice4TextBox As TextBox
    Friend WithEvents RetailPriceTextBox As TextBox
    Friend WithEvents QtyTextBox As TextBox
    Friend WithEvents QuantityCodeTextBox As TextBox
    Friend WithEvents Properties01TextBox As TextBox
    Friend WithEvents Properties02TextBox As TextBox
    Friend WithEvents Properties03TextBox As TextBox
    Friend WithEvents Properties04TextBox As TextBox
    Friend WithEvents Properties05TextBox As TextBox
    Friend WithEvents Properties06TextBox As TextBox
    Friend WithEvents Properties07TextBox As TextBox
    Friend WithEvents Properties08TextBox As TextBox
    Friend WithEvents Properties09TextBox As TextBox
    Friend WithEvents Properties10TextBox As TextBox
    Friend WithEvents WarrantyTextBox As TextBox
    Friend WithEvents RemarkTextBox As TextBox
    Friend WithEvents StatusTextBox As TextBox
    Friend WithEvents CreateUserTextBox As TextBox
    Friend WithEvents UpdateUserTextBox As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btAddProduct As Button
    Friend WithEvents ProfitPR1TextBox As TextBox
    Friend WithEvents ProfitPR2TextBox As TextBox
    Friend WithEvents ProfitPR3TextBox As TextBox
    Friend WithEvents ProfitPR4TextBox As TextBox
    Friend WithEvents CreateWhenTextBox As TextBox
    Friend WithEvents UpdateWhenTextBox As TextBox
    Friend WithEvents btUpdate As Button
    Friend WithEvents btReload As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents PriceSourceComboBox1 As ComboBox
    Friend WithEvents TextBox5 As TextBox
End Class
