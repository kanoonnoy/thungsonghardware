﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SupplierForm
    Inherits ChildForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SupplierPanel1 = New TSHardware.SupplierPanel()
        Me.SuspendLayout()
        '
        'SupplierPanel1
        '
        Me.SupplierPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SupplierPanel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.SupplierPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SupplierPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.SupplierPanel1.Name = "SupplierPanel1"
        Me.SupplierPanel1.Size = New System.Drawing.Size(894, 663)
        Me.SupplierPanel1.TabIndex = 0
        '
        'SupplierForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 663)
        Me.Controls.Add(Me.SupplierPanel1)
        Me.Name = "SupplierForm"
        Me.Text = "SupplierForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SupplierPanel1 As SupplierPanel
End Class
