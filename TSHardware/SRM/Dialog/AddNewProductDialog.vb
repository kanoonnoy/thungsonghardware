﻿Public Class AddNewProductDialog

    Private table As TableEntity = New TableEntity()
    Private _ProductGroupCode As String
    Public Property ProductGroupCode() As String
        Get
            Return _ProductGroupCode
        End Get
        Set(ByVal value As String)
            _ProductGroupCode = value
        End Set
    End Property
    Private _SupllierCode As String
    Public Property SupplierCode() As String
        Get
            Return _SupllierCode
        End Get
        Set(ByVal value As String)
            _SupllierCode = value
        End Set
    End Property
    Private _ProductCodeAddNew As String = String.Empty
    Public Property ProductCode() As String
        Get
            Return _ProductCodeAddNew
        End Get
        Set(ByVal value As String)
            _ProductCodeAddNew = value
        End Set
    End Property

    Private Sub Reload()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            Me.Label1.Text = ""
            Dim str As String = "SELECT * FROM ProductGroupMaster"
            If table.SQLQuery(str) Then
                'Console.WriteLine(table.Records.Rows.Count)
                Dim dataProductGroup = table.Records
                Me.ComboBox1.DataSource = dataProductGroup
                Me.ComboBox1.DisplayMember = "ProductGroupName"
                Me.ComboBox1.ValueMember = "ProductGroupCode"
            End If

            Dim str2 = "SELECT * FROM SupllierMaster"
            If table.SQLQuery(str2) Then
                Dim dataSupplierMaster = table.Records
                Me.ComboBox2.DataSource = dataSupplierMaster
                Me.ComboBox2.DisplayMember = "SupllierCode"
                Me.ComboBox2.ValueMember = "SupllierName"
            End If


        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try
    End Sub
    Private Sub AddNewProductDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Reload()

    End Sub
    Private Sub Submit()
        Try
            Me.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            Me.Label1.Text = ""
            Dim ProductCode = Me.TextBox2.Text
            If ProductCode.Length = 0 Then
                Return
            End If
            Dim isAdd = False
            Dim strcheck = String.Format("SELECT * FROM ProductMaster WHERE ProductCode = '{0}'", ProductCode)
            If table.SQLQuery(strcheck) Then
                If table.Records.Rows.Count = 0 Then
                    isAdd = True
                End If
            End If

            If isAdd Then
                ' Add ProductCode
                Dim dlg = MessageBox.Show(String.Format("คุณต้องการจะเพิ่มรหัสสินค้า '{0}' นี้หรือไม่?", ProductCode), "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                If dlg = DialogResult.Yes Then
                    _ProductCodeAddNew = ProductCode
                    _ProductGroupCode = Me.ComboBox1.SelectedValue.ToString()
                    _SupllierCode = Me.ComboBox2.SelectedValue.ToString()
                    Me.DialogResult = DialogResult.Yes
                    Me.Close()
                End If
            Else
                Label1.Text = "ขออภัยรหัสนี้มีอยู่แล้ว"

            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            Me.Enabled = True
            Me.Cursor = Cursors.Default

        End Try

    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Submit()

    End Sub


End Class