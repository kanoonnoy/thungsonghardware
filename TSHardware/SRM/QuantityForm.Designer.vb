﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuantityForm
    Inherits ChildForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.QuantityPanel1 = New TSHardware.QuantityPanel()
        Me.SuspendLayout()
        '
        'QuantityPanel1
        '
        Me.QuantityPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.QuantityPanel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.QuantityPanel1.Location = New System.Drawing.Point(0, 0)
        Me.QuantityPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.QuantityPanel1.Name = "QuantityPanel1"
        Me.QuantityPanel1.Size = New System.Drawing.Size(762, 530)
        Me.QuantityPanel1.TabIndex = 0
        '
        'QuantityForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(762, 530)
        Me.Controls.Add(Me.QuantityPanel1)
        Me.Name = "QuantityForm"
        Me.Text = "QuantityForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents QuantityPanel1 As QuantityPanel
End Class
