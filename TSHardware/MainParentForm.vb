﻿Imports System.Configuration
Imports System.Reflection
Imports System.Windows.Forms
Imports System.Configuration.ConfigurationManager
Imports System.IO

Public Class MainParentForm

    Private _IsStartup = True
    Private _ConnectionSrting As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=""{DatabasePath}\RealTime.mdb"""



    Private Sub Startup()
        Try
            Dim C As Control

            For Each C In Me.Controls
                If TypeOf C Is MdiClient Then
                    C.BackColor = Color.White
                    Exit For
                End If
            Next

            C = Nothing

            Dim FileFullPathLogo = String.Format("{0}\LOGO\logo.jpg", GlobalV.ERPPath)
            If File.Exists(FileFullPathLogo) Then
                Me.BackgroundImage = GlobalV.SafeImageFromFile(FileFullPathLogo)
                Me.BackgroundImageLayout = ImageLayout.Zoom

                Me.BackColor = Color.White
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub Initialization()
        Try
            _ConnectionSrting = _ConnectionSrting.Replace("{DatabasePath}", GlobalV.ERPPath)
            '' OS 64 bit Microsoft.Jet.OLEDB.4.0 to Microsoft.ACE.OLEDB.12.0
            GlobalV.DataBaseConnectionString = String.Format("{0}\RealTime.mdb", GlobalV.ERPPath)
            If Environment.Is64BitOperatingSystem Then
                Dim connection = _ConnectionSrting.Replace("Microsoft.Jet.OLEDB.4.0", "Microsoft.ACE.OLEDB.12.0")
                My.Settings.Item("RealTimeConnectionString") = connection

            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub SwitchUser()
        Try
            Me.LabelCompanyName.Text = "บริษัท ทุ่งสงกสรู จำกัด สำนักงานใหญ่"
            Me.LabelUserName.Text = GlobalV.CurrentUsername
            Me.ToolStripStatusLabel.Text = String.Format("เข้าใช้งานเมื่อเวลา: {0:dd/MM/yyyy HH:mm:ss} โดยเครื่อง: {1}", DateTime.Now, Environment.MachineName)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally

        End Try
    End Sub
    Private Sub Reload()
        Try

            If _IsStartup Then
                _IsStartup = False
                Startup()
                Initialization()
            End If

            SwitchUser()

            Me.NaviBar1.Settings.VisibleButtons = 0



            Me.TreeView1.ExpandAll()
            Me.TreeView2.ExpandAll()
            Me.TreeView3.ExpandAll()
            Me.TreeView4.ExpandAll()

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub MainParentForm_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Reload()
    End Sub


    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
        ' Create a new instance of the child form.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Window " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: Add code here to open the file.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: Add code here to save the current contents of the form to a file.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click

    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub TreeView2_MouseDoubleClick(sender As Object, e As MouseEventArgs)
        Console.WriteLine()
    End Sub

    Private _ChilForm As Form
    Private Sub TreeView2_NodeMouseDoubleClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles TreeView2.NodeMouseDoubleClick, TreeView4.NodeMouseDoubleClick, TreeView3.NodeMouseDoubleClick, TreeView1.NodeMouseDoubleClick
        Try
            Dim formName = [Assembly].GetEntryAssembly.GetName.Name & "." & e.Node.Tag.ToString()
            Dim formCreated As Type = Type.[GetType](formName)
            Dim showForm As Form = TryCast(Activator.CreateInstance(formCreated), Form)

            Dim isStart = True
            For Each ChildForm As Form In Me.MdiChildren
                If ChildForm.Text = e.Node.Text.ToString() Then
                    isStart = False
                    ChildForm.Activate()
                End If
            Next

            If isStart Then
                showForm.MdiParent = Me
                showForm.Show()
                showForm.ShowIcon = False
                showForm.Text = e.Node.Text.ToString()
                showForm.WindowState = FormWindowState.Maximized
                showForm.BringToFront()
            End If

        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub
End Class
