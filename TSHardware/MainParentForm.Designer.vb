﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainParentForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("จุดขายสินค้า", 54, 54)
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ขายหน้าร้าน", 125, 125, New System.Windows.Forms.TreeNode() {TreeNode11})
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนลูกค้า", 25, 25)
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ลูกค้า", 142, 142, New System.Windows.Forms.TreeNode() {TreeNode13})
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("เอกสารใบรับประกัน", 214, 214)
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("เอกสารอิเล็กทรอนิกส์", 192, 192, New System.Windows.Forms.TreeNode() {TreeNode15})
        Dim TreeNode30 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("รายงานที่เกี่ยวข้อง", 80, 80)
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainParentForm))
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนพนักงาน", 72, 72)
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("พนักงาน", 208, 208, New System.Windows.Forms.TreeNode() {TreeNode17})
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("คำนวณเงินเดือน", 92, 92)
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("สวัสดิการ", 116, 116, New System.Windows.Forms.TreeNode() {TreeNode19})
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("บัตรพนักงาน")
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("เอกสารที่เกี่ยวข้อง", 211, 211, New System.Windows.Forms.TreeNode() {TreeNode21})
        Dim TreeNode23 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนสินค้า", 216, 216)
        Dim TreeNode24 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนหน่วยนับ", 147, 147)
        Dim TreeNode25 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนหมวดสินค้า", 47, 47)
        Dim TreeNode26 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("สินค้า", 90, 90, New System.Windows.Forms.TreeNode() {TreeNode23, TreeNode24, TreeNode25})
        Dim TreeNode27 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ทะเบียนผู้จำหน่าย", 211, 211)
        Dim TreeNode28 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ผู้จำหน่าย", 163, 163, New System.Windows.Forms.TreeNode() {TreeNode27})
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("สิทธิ์การใช้งานระบบ")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("ระบบ", 195, 195, New System.Windows.Forms.TreeNode() {TreeNode1})
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("แก้ไขข้อมูลบัญชีผู้ใช้", 61, 61)
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("บัญชีผู้ใช้", 25, 25, New System.Windows.Forms.TreeNode() {TreeNode3})
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CascadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileVerticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrangeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.NaviBar1 = New Guifreaks.NavigationBar.NaviBar(Me.components)
        Me.NaviBandSaleForce = New Guifreaks.NavigationBar.NaviBand(Me.components)
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.NaviBandHR = New Guifreaks.NavigationBar.NaviBand(Me.components)
        Me.TreeView3 = New System.Windows.Forms.TreeView()
        Me.NaviBandSRM = New Guifreaks.NavigationBar.NaviBand(Me.components)
        Me.TreeView2 = New System.Windows.Forms.TreeView()
        Me.NaviBandSetting = New Guifreaks.NavigationBar.NaviBand(Me.components)
        Me.TreeView4 = New System.Windows.Forms.TreeView()
        Me.LabelUserName = New System.Windows.Forms.Label()
        Me.btLogout = New System.Windows.Forms.Button()
        Me.LabelCompanyName = New System.Windows.Forms.Label()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        CType(Me.NaviBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NaviBar1.SuspendLayout()
        Me.NaviBandSaleForce.ClientArea.SuspendLayout()
        Me.NaviBandSaleForce.SuspendLayout()
        Me.NaviBandHR.ClientArea.SuspendLayout()
        Me.NaviBandHR.SuspendLayout()
        Me.NaviBandSRM.ClientArea.SuspendLayout()
        Me.NaviBandSRM.SuspendLayout()
        Me.NaviBandSetting.ClientArea.SuspendLayout()
        Me.NaviBandSetting.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewMenu, Me.WindowsMenu})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.WindowsMenu
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(10, 3, 0, 3)
        Me.MenuStrip.Size = New System.Drawing.Size(782, 30)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolBarToolStripMenuItem, Me.StatusBarToolStripMenuItem})
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(59, 24)
        Me.ViewMenu.Text = "&มุมมอง"
        '
        'ToolBarToolStripMenuItem
        '
        Me.ToolBarToolStripMenuItem.Checked = True
        Me.ToolBarToolStripMenuItem.CheckOnClick = True
        Me.ToolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToolBarToolStripMenuItem.Name = "ToolBarToolStripMenuItem"
        Me.ToolBarToolStripMenuItem.Size = New System.Drawing.Size(150, 26)
        Me.ToolBarToolStripMenuItem.Text = "&Toolbar"
        '
        'StatusBarToolStripMenuItem
        '
        Me.StatusBarToolStripMenuItem.Checked = True
        Me.StatusBarToolStripMenuItem.CheckOnClick = True
        Me.StatusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StatusBarToolStripMenuItem.Name = "StatusBarToolStripMenuItem"
        Me.StatusBarToolStripMenuItem.Size = New System.Drawing.Size(150, 26)
        Me.StatusBarToolStripMenuItem.Text = "&Status Bar"
        '
        'WindowsMenu
        '
        Me.WindowsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CascadeToolStripMenuItem, Me.TileVerticalToolStripMenuItem, Me.TileHorizontalToolStripMenuItem, Me.CloseAllToolStripMenuItem, Me.ArrangeIconsToolStripMenuItem})
        Me.WindowsMenu.Name = "WindowsMenu"
        Me.WindowsMenu.Size = New System.Drawing.Size(68, 24)
        Me.WindowsMenu.Text = "&หน้าต่าง"
        '
        'CascadeToolStripMenuItem
        '
        Me.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem"
        Me.CascadeToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.CascadeToolStripMenuItem.Text = "&Cascade"
        '
        'TileVerticalToolStripMenuItem
        '
        Me.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem"
        Me.TileVerticalToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.TileVerticalToolStripMenuItem.Text = "Tile &Vertical"
        '
        'TileHorizontalToolStripMenuItem
        '
        Me.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem"
        Me.TileHorizontalToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.TileHorizontalToolStripMenuItem.Text = "Tile &Horizontal"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.CloseAllToolStripMenuItem.Text = "C&lose All"
        '
        'ArrangeIconsToolStripMenuItem
        '
        Me.ArrangeIconsToolStripMenuItem.Name = "ArrangeIconsToolStripMenuItem"
        Me.ArrangeIconsToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.ArrangeIconsToolStripMenuItem.Text = "&Arrange Icons"
        '
        'StatusStrip
        '
        Me.StatusStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 528)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 24, 0)
        Me.StatusStrip.Size = New System.Drawing.Size(782, 25)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(49, 20)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'NaviBar1
        '
        Me.NaviBar1.ActiveBand = Me.NaviBandSaleForce
        Me.NaviBar1.Controls.Add(Me.NaviBandSaleForce)
        Me.NaviBar1.Controls.Add(Me.NaviBandHR)
        Me.NaviBar1.Controls.Add(Me.NaviBandSRM)
        Me.NaviBar1.Controls.Add(Me.NaviBandSetting)
        Me.NaviBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NaviBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.NaviBar1.Location = New System.Drawing.Point(0, 30)
        Me.NaviBar1.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBar1.Name = "NaviBar1"
        Me.NaviBar1.ShowMoreOptionsButton = False
        Me.NaviBar1.Size = New System.Drawing.Size(235, 498)
        Me.NaviBar1.TabIndex = 9
        Me.NaviBar1.Text = "NaviBar1"
        Me.NaviBar1.VisibleLargeButtons = 4
        '
        'NaviBandSaleForce
        '
        '
        'NaviBandSaleForce.ClientArea
        '
        Me.NaviBandSaleForce.ClientArea.Controls.Add(Me.TreeView1)
        Me.NaviBandSaleForce.ClientArea.Location = New System.Drawing.Point(0, 0)
        Me.NaviBandSaleForce.ClientArea.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSaleForce.ClientArea.Name = "ClientArea"
        Me.NaviBandSaleForce.ClientArea.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSaleForce.ClientArea.TabIndex = 0
        Me.NaviBandSaleForce.LargeImage = Global.TSHardware.My.Resources.Resources.ALL_16_0012
        Me.NaviBandSaleForce.Location = New System.Drawing.Point(1, 27)
        Me.NaviBandSaleForce.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSaleForce.Name = "NaviBandSaleForce"
        Me.NaviBandSaleForce.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSaleForce.SmallImage = Global.TSHardware.My.Resources.Resources.ALL_16_0012
        Me.NaviBandSaleForce.TabIndex = 3
        Me.NaviBandSaleForce.Text = "ขายหน้าร้าน"
        '
        'TreeView1
        '
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.HideSelection = False
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.ImageList1
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4)
        Me.TreeView1.Name = "TreeView1"
        TreeNode11.ImageIndex = 54
        TreeNode11.Name = "Node3"
        TreeNode11.SelectedImageIndex = 54
        TreeNode11.Tag = "SellForceForm"
        TreeNode11.Text = "จุดขายสินค้า"
        TreeNode12.ImageIndex = 125
        TreeNode12.Name = "Node0"
        TreeNode12.SelectedImageIndex = 125
        TreeNode12.Text = "ขายหน้าร้าน"
        TreeNode13.ImageIndex = 25
        TreeNode13.Name = "Node1"
        TreeNode13.SelectedImageIndex = 25
        TreeNode13.Tag = "CustomerForm"
        TreeNode13.Text = "ทะเบียนลูกค้า"
        TreeNode14.ImageIndex = 142
        TreeNode14.Name = "Node0"
        TreeNode14.SelectedImageIndex = 142
        TreeNode14.Text = "ลูกค้า"
        TreeNode15.ImageIndex = 214
        TreeNode15.Name = "Node4"
        TreeNode15.SelectedImageIndex = 214
        TreeNode15.Text = "เอกสารใบรับประกัน"
        TreeNode16.ImageIndex = 192
        TreeNode16.Name = "Node1"
        TreeNode16.SelectedImageIndex = 192
        TreeNode16.Text = "เอกสารอิเล็กทรอนิกส์"
        TreeNode30.ImageIndex = 80
        TreeNode30.Name = "Node2"
        TreeNode30.SelectedImageIndex = 80
        TreeNode30.Text = "รายงานที่เกี่ยวข้อง"
        Me.TreeView1.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode12, TreeNode14, TreeNode16, TreeNode30})
        Me.TreeView1.SelectedImageIndex = 0
        Me.TreeView1.Size = New System.Drawing.Size(233, 303)
        Me.TreeView1.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "3d bar chart.png")
        Me.ImageList1.Images.SetKeyName(1, "Abort.png")
        Me.ImageList1.Images.SetKeyName(2, "About.png")
        Me.ImageList1.Images.SetKeyName(3, "Accounting.png")
        Me.ImageList1.Images.SetKeyName(4, "Address book.png")
        Me.ImageList1.Images.SetKeyName(5, "Alarm clock.png")
        Me.ImageList1.Images.SetKeyName(6, "Alarm.png")
        Me.ImageList1.Images.SetKeyName(7, "Alert.png")
        Me.ImageList1.Images.SetKeyName(8, "Alien.png")
        Me.ImageList1.Images.SetKeyName(9, "Anchor.png")
        Me.ImageList1.Images.SetKeyName(10, "Application.png")
        Me.ImageList1.Images.SetKeyName(11, "Apply.png")
        Me.ImageList1.Images.SetKeyName(12, "Back.png")
        Me.ImageList1.Images.SetKeyName(13, "Bad mark.png")
        Me.ImageList1.Images.SetKeyName(14, "Bee.png")
        Me.ImageList1.Images.SetKeyName(15, "Black bookmark.png")
        Me.ImageList1.Images.SetKeyName(16, "Black pin.png")
        Me.ImageList1.Images.SetKeyName(17, "Black tag.png")
        Me.ImageList1.Images.SetKeyName(18, "Blog.png")
        Me.ImageList1.Images.SetKeyName(19, "Blue bookmark.png")
        Me.ImageList1.Images.SetKeyName(20, "Blue key.png")
        Me.ImageList1.Images.SetKeyName(21, "Blue pin.png")
        Me.ImageList1.Images.SetKeyName(22, "Blue tag.png")
        Me.ImageList1.Images.SetKeyName(23, "Bomb.png")
        Me.ImageList1.Images.SetKeyName(24, "Bookmark.png")
        Me.ImageList1.Images.SetKeyName(25, "Boss.png")
        Me.ImageList1.Images.SetKeyName(26, "Bottom.png")
        Me.ImageList1.Images.SetKeyName(27, "Briefcase.png")
        Me.ImageList1.Images.SetKeyName(28, "Brush.png")
        Me.ImageList1.Images.SetKeyName(29, "Bubble.png")
        Me.ImageList1.Images.SetKeyName(30, "Buy.png")
        Me.ImageList1.Images.SetKeyName(31, "Calculator.png")
        Me.ImageList1.Images.SetKeyName(32, "Calendar.png")
        Me.ImageList1.Images.SetKeyName(33, "Cancel.png")
        Me.ImageList1.Images.SetKeyName(34, "Car key.png")
        Me.ImageList1.Images.SetKeyName(35, "CD.png")
        Me.ImageList1.Images.SetKeyName(36, "Clipboard.png")
        Me.ImageList1.Images.SetKeyName(37, "Clock.png")
        Me.ImageList1.Images.SetKeyName(38, "Comment.png")
        Me.ImageList1.Images.SetKeyName(39, "Company.png")
        Me.ImageList1.Images.SetKeyName(40, "Compass.png")
        Me.ImageList1.Images.SetKeyName(41, "Component.png")
        Me.ImageList1.Images.SetKeyName(42, "Computer 16x16.png")
        Me.ImageList1.Images.SetKeyName(43, "Copy.png")
        Me.ImageList1.Images.SetKeyName(44, "Create.png")
        Me.ImageList1.Images.SetKeyName(45, "Cut.png")
        Me.ImageList1.Images.SetKeyName(46, "Danger.png")
        Me.ImageList1.Images.SetKeyName(47, "Database.png")
        Me.ImageList1.Images.SetKeyName(48, "Delete.png")
        Me.ImageList1.Images.SetKeyName(49, "Delivery.png")
        Me.ImageList1.Images.SetKeyName(50, "Diagram.png")
        Me.ImageList1.Images.SetKeyName(51, "Dial.png")
        Me.ImageList1.Images.SetKeyName(52, "Disaster.png")
        Me.ImageList1.Images.SetKeyName(53, "Display 16x16.png")
        Me.ImageList1.Images.SetKeyName(54, "Dollar.png")
        Me.ImageList1.Images.SetKeyName(55, "Down.png")
        Me.ImageList1.Images.SetKeyName(56, "Download.png")
        Me.ImageList1.Images.SetKeyName(57, "Downloads folder.png")
        Me.ImageList1.Images.SetKeyName(58, "Earth.png")
        Me.ImageList1.Images.SetKeyName(59, "Eject.png")
        Me.ImageList1.Images.SetKeyName(60, "E-mail.png")
        Me.ImageList1.Images.SetKeyName(61, "Equipment.png")
        Me.ImageList1.Images.SetKeyName(62, "Erase.png")
        Me.ImageList1.Images.SetKeyName(63, "Error.png")
        Me.ImageList1.Images.SetKeyName(64, "Euro.png")
        Me.ImageList1.Images.SetKeyName(65, "Exit.png")
        Me.ImageList1.Images.SetKeyName(66, "Expand.png")
        Me.ImageList1.Images.SetKeyName(67, "Eye.png")
        Me.ImageList1.Images.SetKeyName(68, "Fall.png")
        Me.ImageList1.Images.SetKeyName(69, "Fast-forward.png")
        Me.ImageList1.Images.SetKeyName(70, "Favourites.png")
        Me.ImageList1.Images.SetKeyName(71, "Female symbol.png")
        Me.ImageList1.Images.SetKeyName(72, "Female.png")
        Me.ImageList1.Images.SetKeyName(73, "Film.png")
        Me.ImageList1.Images.SetKeyName(74, "Filter.png")
        Me.ImageList1.Images.SetKeyName(75, "Find.png")
        Me.ImageList1.Images.SetKeyName(76, "First record.png")
        Me.ImageList1.Images.SetKeyName(77, "First.png")
        Me.ImageList1.Images.SetKeyName(78, "Flag.png")
        Me.ImageList1.Images.SetKeyName(79, "Flash drive.png")
        Me.ImageList1.Images.SetKeyName(80, "Folder.png")
        Me.ImageList1.Images.SetKeyName(81, "Forbidden.png")
        Me.ImageList1.Images.SetKeyName(82, "Forward.png")
        Me.ImageList1.Images.SetKeyName(83, "Free bsd.png")
        Me.ImageList1.Images.SetKeyName(84, "Gift.png")
        Me.ImageList1.Images.SetKeyName(85, "Globe.png")
        Me.ImageList1.Images.SetKeyName(86, "Go back.png")
        Me.ImageList1.Images.SetKeyName(87, "Go forward.png")
        Me.ImageList1.Images.SetKeyName(88, "Go.png")
        Me.ImageList1.Images.SetKeyName(89, "Good mark.png")
        Me.ImageList1.Images.SetKeyName(90, "Green bookmark.png")
        Me.ImageList1.Images.SetKeyName(91, "Green pin.png")
        Me.ImageList1.Images.SetKeyName(92, "Green tag.png")
        Me.ImageList1.Images.SetKeyName(93, "Hard disk.png")
        Me.ImageList1.Images.SetKeyName(94, "Heart.png")
        Me.ImageList1.Images.SetKeyName(95, "Help book 3d.png")
        Me.ImageList1.Images.SetKeyName(96, "Help book.png")
        Me.ImageList1.Images.SetKeyName(97, "Help symbol.png")
        Me.ImageList1.Images.SetKeyName(98, "Help.png")
        Me.ImageList1.Images.SetKeyName(99, "Hint.png")
        Me.ImageList1.Images.SetKeyName(100, "History.png")
        Me.ImageList1.Images.SetKeyName(101, "Home.png")
        Me.ImageList1.Images.SetKeyName(102, "Hourglass.png")
        Me.ImageList1.Images.SetKeyName(103, "How-to.png")
        Me.ImageList1.Images.SetKeyName(104, "Hungup.png")
        Me.ImageList1.Images.SetKeyName(105, "Info.png")
        Me.ImageList1.Images.SetKeyName(106, "In-yang.png")
        Me.ImageList1.Images.SetKeyName(107, "Iphone.png")
        Me.ImageList1.Images.SetKeyName(108, "Key.png")
        Me.ImageList1.Images.SetKeyName(109, "Last recor.png")
        Me.ImageList1.Images.SetKeyName(110, "Last.png")
        Me.ImageList1.Images.SetKeyName(111, "Left-right.png")
        Me.ImageList1.Images.SetKeyName(112, "Lightning.png")
        Me.ImageList1.Images.SetKeyName(113, "Liner.png")
        Me.ImageList1.Images.SetKeyName(114, "Linux.png")
        Me.ImageList1.Images.SetKeyName(115, "List.png")
        Me.ImageList1.Images.SetKeyName(116, "Load.png")
        Me.ImageList1.Images.SetKeyName(117, "Lock.png")
        Me.ImageList1.Images.SetKeyName(118, "Low rating.png")
        Me.ImageList1.Images.SetKeyName(119, "Magic wand.png")
        Me.ImageList1.Images.SetKeyName(120, "Mail.png")
        Me.ImageList1.Images.SetKeyName(121, "Male symbol.png")
        Me.ImageList1.Images.SetKeyName(122, "Male.png")
        Me.ImageList1.Images.SetKeyName(123, "Medium rating.png")
        Me.ImageList1.Images.SetKeyName(124, "Message.png")
        Me.ImageList1.Images.SetKeyName(125, "Mobile-phone.png")
        Me.ImageList1.Images.SetKeyName(126, "Modify.png")
        Me.ImageList1.Images.SetKeyName(127, "Move.png")
        Me.ImageList1.Images.SetKeyName(128, "Movie.png")
        Me.ImageList1.Images.SetKeyName(129, "Music.png")
        Me.ImageList1.Images.SetKeyName(130, "Mute.png")
        Me.ImageList1.Images.SetKeyName(131, "Network connection.png")
        Me.ImageList1.Images.SetKeyName(132, "New document.png")
        Me.ImageList1.Images.SetKeyName(133, "New.png")
        Me.ImageList1.Images.SetKeyName(134, "Next track.png")
        Me.ImageList1.Images.SetKeyName(135, "Next.png")
        Me.ImageList1.Images.SetKeyName(136, "No.png")
        Me.ImageList1.Images.SetKeyName(137, "No-entry.png")
        Me.ImageList1.Images.SetKeyName(138, "Notes.png")
        Me.ImageList1.Images.SetKeyName(139, "OK.png")
        Me.ImageList1.Images.SetKeyName(140, "Paste.png")
        Me.ImageList1.Images.SetKeyName(141, "Pause.png")
        Me.ImageList1.Images.SetKeyName(142, "People.png")
        Me.ImageList1.Images.SetKeyName(143, "Percent.png")
        Me.ImageList1.Images.SetKeyName(144, "Person.png")
        Me.ImageList1.Images.SetKeyName(145, "Phone number.png")
        Me.ImageList1.Images.SetKeyName(146, "Phone.png")
        Me.ImageList1.Images.SetKeyName(147, "Pie chart.png")
        Me.ImageList1.Images.SetKeyName(148, "Pinion.png")
        Me.ImageList1.Images.SetKeyName(149, "Play.png")
        Me.ImageList1.Images.SetKeyName(150, "Playback.png")
        Me.ImageList1.Images.SetKeyName(151, "Play-music.png")
        Me.ImageList1.Images.SetKeyName(152, "Previous record.png")
        Me.ImageList1.Images.SetKeyName(153, "Previous.png")
        Me.ImageList1.Images.SetKeyName(154, "Print.png")
        Me.ImageList1.Images.SetKeyName(155, "Problem.png")
        Me.ImageList1.Images.SetKeyName(156, "Question.png")
        Me.ImageList1.Images.SetKeyName(157, "Radiation.png")
        Me.ImageList1.Images.SetKeyName(158, "Raise.png")
        Me.ImageList1.Images.SetKeyName(159, "Record.png")
        Me.ImageList1.Images.SetKeyName(160, "Red bookmark.png")
        Me.ImageList1.Images.SetKeyName(161, "Red mark.png")
        Me.ImageList1.Images.SetKeyName(162, "Red pin.png")
        Me.ImageList1.Images.SetKeyName(163, "Red star.png")
        Me.ImageList1.Images.SetKeyName(164, "Red tag.png")
        Me.ImageList1.Images.SetKeyName(165, "Redo.png")
        Me.ImageList1.Images.SetKeyName(166, "Refresh.png")
        Me.ImageList1.Images.SetKeyName(167, "Remove.png")
        Me.ImageList1.Images.SetKeyName(168, "Repair.png")
        Me.ImageList1.Images.SetKeyName(169, "Report.png")
        Me.ImageList1.Images.SetKeyName(170, "Retort.png")
        Me.ImageList1.Images.SetKeyName(171, "Rewind.png")
        Me.ImageList1.Images.SetKeyName(172, "Sad.png")
        Me.ImageList1.Images.SetKeyName(173, "Save.png")
        Me.ImageList1.Images.SetKeyName(174, "Schedule.png")
        Me.ImageList1.Images.SetKeyName(175, "Script.png")
        Me.ImageList1.Images.SetKeyName(176, "Search.png")
        Me.ImageList1.Images.SetKeyName(177, "Shield 16x16.png")
        Me.ImageList1.Images.SetKeyName(178, "Shopping cart.png")
        Me.ImageList1.Images.SetKeyName(179, "Silence.png")
        Me.ImageList1.Images.SetKeyName(180, "Smile.png")
        Me.ImageList1.Images.SetKeyName(181, "Sound.png")
        Me.ImageList1.Images.SetKeyName(182, "Stop sign.png")
        Me.ImageList1.Images.SetKeyName(183, "Stop.png")
        Me.ImageList1.Images.SetKeyName(184, "Stopwatch.png")
        Me.ImageList1.Images.SetKeyName(185, "Sum.png")
        Me.ImageList1.Images.SetKeyName(186, "Sync.png")
        Me.ImageList1.Images.SetKeyName(187, "Table.png")
        Me.ImageList1.Images.SetKeyName(188, "Target.png")
        Me.ImageList1.Images.SetKeyName(189, "Taxi.png")
        Me.ImageList1.Images.SetKeyName(190, "Terminate.png")
        Me.ImageList1.Images.SetKeyName(191, "Text preview.png")
        Me.ImageList1.Images.SetKeyName(192, "Text.png")
        Me.ImageList1.Images.SetKeyName(193, "Thumbs down.png")
        Me.ImageList1.Images.SetKeyName(194, "Thumbs up.png")
        Me.ImageList1.Images.SetKeyName(195, "Toolbox.png")
        Me.ImageList1.Images.SetKeyName(196, "Top.png")
        Me.ImageList1.Images.SetKeyName(197, "Trackback.png")
        Me.ImageList1.Images.SetKeyName(198, "Trash.png")
        Me.ImageList1.Images.SetKeyName(199, "Tune.png")
        Me.ImageList1.Images.SetKeyName(200, "Turn off.png")
        Me.ImageList1.Images.SetKeyName(201, "Twitter.png")
        Me.ImageList1.Images.SetKeyName(202, "Undo.png")
        Me.ImageList1.Images.SetKeyName(203, "Unlock.png")
        Me.ImageList1.Images.SetKeyName(204, "Up.png")
        Me.ImageList1.Images.SetKeyName(205, "Update.png")
        Me.ImageList1.Images.SetKeyName(206, "Up-down.png")
        Me.ImageList1.Images.SetKeyName(207, "Upload.png")
        Me.ImageList1.Images.SetKeyName(208, "User group.png")
        Me.ImageList1.Images.SetKeyName(209, "View.png")
        Me.ImageList1.Images.SetKeyName(210, "Volume.png")
        Me.ImageList1.Images.SetKeyName(211, "Wallet.png")
        Me.ImageList1.Images.SetKeyName(212, "Warning.png")
        Me.ImageList1.Images.SetKeyName(213, "Wrench.png")
        Me.ImageList1.Images.SetKeyName(214, "Yellow bookmark.png")
        Me.ImageList1.Images.SetKeyName(215, "Yellow pin.png")
        Me.ImageList1.Images.SetKeyName(216, "Yellow tag.png")
        Me.ImageList1.Images.SetKeyName(217, "Yes.png")
        Me.ImageList1.Images.SetKeyName(218, "Zoom.png")
        '
        'NaviBandHR
        '
        '
        'NaviBandHR.ClientArea
        '
        Me.NaviBandHR.ClientArea.Controls.Add(Me.TreeView3)
        Me.NaviBandHR.ClientArea.Location = New System.Drawing.Point(0, 0)
        Me.NaviBandHR.ClientArea.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandHR.ClientArea.Name = "ClientArea"
        Me.NaviBandHR.ClientArea.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandHR.ClientArea.TabIndex = 0
        Me.NaviBandHR.LargeImage = Global.TSHardware.My.Resources.Resources.ALL_16_0010
        Me.NaviBandHR.Location = New System.Drawing.Point(1, 27)
        Me.NaviBandHR.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandHR.Name = "NaviBandHR"
        Me.NaviBandHR.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandHR.SmallImage = Global.TSHardware.My.Resources.Resources.ALL_16_0010
        Me.NaviBandHR.TabIndex = 7
        Me.NaviBandHR.Text = "ทรัพยากรบุคคล"
        '
        'TreeView3
        '
        Me.TreeView3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView3.ImageIndex = 0
        Me.TreeView3.ImageList = Me.ImageList1
        Me.TreeView3.Location = New System.Drawing.Point(0, 0)
        Me.TreeView3.Name = "TreeView3"
        TreeNode17.ImageIndex = 72
        TreeNode17.Name = "Node2"
        TreeNode17.SelectedImageIndex = 72
        TreeNode17.Tag = "EmployeeMasterForm"
        TreeNode17.Text = "ทะเบียนพนักงาน"
        TreeNode18.ImageIndex = 208
        TreeNode18.Name = "Node0"
        TreeNode18.SelectedImageIndex = 208
        TreeNode18.Text = "พนักงาน"
        TreeNode19.ImageIndex = 92
        TreeNode19.Name = "Node4"
        TreeNode19.SelectedImageIndex = 92
        TreeNode19.Text = "คำนวณเงินเดือน"
        TreeNode20.ImageIndex = 116
        TreeNode20.Name = "Node1"
        TreeNode20.SelectedImageIndex = 116
        TreeNode20.Text = "สวัสดิการ"
        TreeNode21.Name = "Node1"
        TreeNode21.Tag = "DynamicReportForm"
        TreeNode21.Text = "บัตรพนักงาน"
        TreeNode22.ImageIndex = 211
        TreeNode22.Name = "Node0"
        TreeNode22.SelectedImageIndex = 211
        TreeNode22.Text = "เอกสารที่เกี่ยวข้อง"
        Me.TreeView3.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode18, TreeNode20, TreeNode22})
        Me.TreeView3.SelectedImageIndex = 0
        Me.TreeView3.Size = New System.Drawing.Size(233, 303)
        Me.TreeView3.TabIndex = 0
        '
        'NaviBandSRM
        '
        '
        'NaviBandSRM.ClientArea
        '
        Me.NaviBandSRM.ClientArea.Controls.Add(Me.TreeView2)
        Me.NaviBandSRM.ClientArea.Location = New System.Drawing.Point(0, 0)
        Me.NaviBandSRM.ClientArea.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSRM.ClientArea.Name = "ClientArea"
        Me.NaviBandSRM.ClientArea.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSRM.ClientArea.TabIndex = 0
        Me.NaviBandSRM.LargeImage = Global.TSHardware.My.Resources.Resources.ALL_16_0011
        Me.NaviBandSRM.Location = New System.Drawing.Point(1, 27)
        Me.NaviBandSRM.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSRM.Name = "NaviBandSRM"
        Me.NaviBandSRM.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSRM.SmallImage = Global.TSHardware.My.Resources.Resources.ALL_16_0011
        Me.NaviBandSRM.TabIndex = 5
        Me.NaviBandSRM.Text = "บริหารผู้ผลิต"
        '
        'TreeView2
        '
        Me.TreeView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView2.HideSelection = False
        Me.TreeView2.ImageIndex = 0
        Me.TreeView2.ImageList = Me.ImageList1
        Me.TreeView2.Location = New System.Drawing.Point(0, 0)
        Me.TreeView2.Margin = New System.Windows.Forms.Padding(4)
        Me.TreeView2.Name = "TreeView2"
        TreeNode23.ImageIndex = 216
        TreeNode23.Name = "Node4"
        TreeNode23.SelectedImageIndex = 216
        TreeNode23.Tag = "SellingPiceForm"
        TreeNode23.Text = "ทะเบียนสินค้า"
        TreeNode24.ImageIndex = 147
        TreeNode24.Name = "Node1"
        TreeNode24.SelectedImageIndex = 147
        TreeNode24.Tag = "QuantityForm"
        TreeNode24.Text = "ทะเบียนหน่วยนับ"
        TreeNode25.ImageIndex = 47
        TreeNode25.Name = "Node0"
        TreeNode25.SelectedImageIndex = 47
        TreeNode25.Tag = "ProductGroupForm"
        TreeNode25.Text = "ทะเบียนหมวดสินค้า"
        TreeNode26.ImageIndex = 90
        TreeNode26.Name = "Node0"
        TreeNode26.SelectedImageIndex = 90
        TreeNode26.Text = "สินค้า"
        TreeNode27.ImageIndex = 211
        TreeNode27.Name = "Node6"
        TreeNode27.SelectedImageIndex = 211
        TreeNode27.Tag = "SupplierForm"
        TreeNode27.Text = "ทะเบียนผู้จำหน่าย"
        TreeNode28.ImageIndex = 163
        TreeNode28.Name = "Node1"
        TreeNode28.SelectedImageIndex = 163
        TreeNode28.Text = "ผู้จำหน่าย"
        Me.TreeView2.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode26, TreeNode28})
        Me.TreeView2.SelectedImageIndex = 0
        Me.TreeView2.Size = New System.Drawing.Size(233, 303)
        Me.TreeView2.TabIndex = 0
        '
        'NaviBandSetting
        '
        '
        'NaviBandSetting.ClientArea
        '
        Me.NaviBandSetting.ClientArea.Controls.Add(Me.TreeView4)
        Me.NaviBandSetting.ClientArea.Location = New System.Drawing.Point(0, 0)
        Me.NaviBandSetting.ClientArea.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSetting.ClientArea.Name = "ClientArea"
        Me.NaviBandSetting.ClientArea.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSetting.ClientArea.TabIndex = 0
        Me.NaviBandSetting.LargeImage = Global.TSHardware.My.Resources.Resources.ALL_16_0009
        Me.NaviBandSetting.Location = New System.Drawing.Point(1, 27)
        Me.NaviBandSetting.Margin = New System.Windows.Forms.Padding(4)
        Me.NaviBandSetting.Name = "NaviBandSetting"
        Me.NaviBandSetting.Size = New System.Drawing.Size(233, 303)
        Me.NaviBandSetting.SmallImage = Global.TSHardware.My.Resources.Resources.ALL_16_0009
        Me.NaviBandSetting.TabIndex = 9
        Me.NaviBandSetting.Text = "ตั้งค่า"
        '
        'TreeView4
        '
        Me.TreeView4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView4.ImageIndex = 0
        Me.TreeView4.ImageList = Me.ImageList1
        Me.TreeView4.Location = New System.Drawing.Point(0, 0)
        Me.TreeView4.Name = "TreeView4"
        TreeNode1.Name = "Node2"
        TreeNode1.Text = "สิทธิ์การใช้งานระบบ"
        TreeNode2.ImageIndex = 195
        TreeNode2.Name = "Node0"
        TreeNode2.SelectedImageIndex = 195
        TreeNode2.Text = "ระบบ"
        TreeNode3.ImageIndex = 61
        TreeNode3.Name = "Node3"
        TreeNode3.SelectedImageIndex = 61
        TreeNode3.Text = "แก้ไขข้อมูลบัญชีผู้ใช้"
        TreeNode4.ImageIndex = 25
        TreeNode4.Name = "Node1"
        TreeNode4.SelectedImageIndex = 25
        TreeNode4.Text = "บัญชีผู้ใช้"
        Me.TreeView4.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode2, TreeNode4})
        Me.TreeView4.SelectedImageIndex = 0
        Me.TreeView4.Size = New System.Drawing.Size(233, 303)
        Me.TreeView4.TabIndex = 0
        '
        'LabelUserName
        '
        Me.LabelUserName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelUserName.AutoSize = True
        Me.LabelUserName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LabelUserName.Location = New System.Drawing.Point(371, 7)
        Me.LabelUserName.Name = "LabelUserName"
        Me.LabelUserName.Size = New System.Drawing.Size(78, 18)
        Me.LabelUserName.TabIndex = 11
        Me.LabelUserName.Text = "ชื่อผู้ใช้ระบบ"
        '
        'btLogout
        '
        Me.btLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btLogout.FlatAppearance.BorderSize = 0
        Me.btLogout.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btLogout.Image = Global.TSHardware.My.Resources.Resources.ALL_16_0013
        Me.btLogout.Location = New System.Drawing.Point(345, 5)
        Me.btLogout.Name = "btLogout"
        Me.btLogout.Size = New System.Drawing.Size(20, 20)
        Me.btLogout.TabIndex = 12
        Me.btLogout.UseVisualStyleBackColor = True
        '
        'LabelCompanyName
        '
        Me.LabelCompanyName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelCompanyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LabelCompanyName.Location = New System.Drawing.Point(549, 7)
        Me.LabelCompanyName.Name = "LabelCompanyName"
        Me.LabelCompanyName.Size = New System.Drawing.Size(162, 18)
        Me.LabelCompanyName.TabIndex = 13
        Me.LabelCompanyName.Text = "ชื่อผู้ใช้ระบบ"
        Me.LabelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MainParentForm
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(782, 553)
        Me.Controls.Add(Me.LabelCompanyName)
        Me.Controls.Add(Me.btLogout)
        Me.Controls.Add(Me.LabelUserName)
        Me.Controls.Add(Me.NaviBar1)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.Name = "MainParentForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Thungsong Hardware"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.NaviBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NaviBar1.ResumeLayout(False)
        Me.NaviBandSaleForce.ClientArea.ResumeLayout(False)
        Me.NaviBandSaleForce.ResumeLayout(False)
        Me.NaviBandHR.ClientArea.ResumeLayout(False)
        Me.NaviBandHR.ResumeLayout(False)
        Me.NaviBandSRM.ClientArea.ResumeLayout(False)
        Me.NaviBandSRM.ResumeLayout(False)
        Me.NaviBandSetting.ClientArea.ResumeLayout(False)
        Me.NaviBandSetting.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents NaviBar1 As Guifreaks.NavigationBar.NaviBar
    Friend WithEvents NaviBandHR As Guifreaks.NavigationBar.NaviBand
    Friend WithEvents NaviBandSRM As Guifreaks.NavigationBar.NaviBand
    Friend WithEvents NaviBandSaleForce As Guifreaks.NavigationBar.NaviBand
    Friend WithEvents NaviBandSetting As Guifreaks.NavigationBar.NaviBand
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents TreeView2 As TreeView
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents ViewMenu As ToolStripMenuItem
    Friend WithEvents ToolBarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusBarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WindowsMenu As ToolStripMenuItem
    Friend WithEvents CascadeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TileVerticalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TileHorizontalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArrangeIconsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TreeView3 As TreeView
    Friend WithEvents TreeView4 As TreeView
    Friend WithEvents LabelUserName As Label
    Friend WithEvents btLogout As Button
    Friend WithEvents LabelCompanyName As Label
End Class
